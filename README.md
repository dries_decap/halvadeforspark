# Documentation
Documentation can be found [here](https://halvadeforspark.readthedocs.io/en/latest/). A quick overview is given below.

# Halvade for Spark
Halvade for Spark is a re-implementation of Halvade (which was implemented in MapReduce). This implementation differs slightly from the previous as the way genomic regions are determined has change and should now be more balanced. This new implementation takes advantage of some Spark features to increase performance, i.e. intermediate files are avoided as much as possible.

## Requirements
This runs on Apache Spark version 2.1 or newer. The Spark cluster should be installed on your current machine or cluster.
Additionally, Halvade requires the reference files of the genome that is being analysed (here we take the human genome reference hg38). These files are required (can be a different base name) and need to be present in the same folder on every machine:

- Homo_sapiens.GRCh38.dna_sm.primary_assembly.dict
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.ann
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.amb
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.bwt
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.fai
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.pac
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.sa
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.vcf.gz
- Homo_sapiens.GRCh38.dna_sm.primary_assembly.vcf.gz.tbi


A directory of binaries that are used in Halvade is also required (they should be present on all machines at the same location), these are the required binaries:

- bwa
- samtools
- GATK 4.0 or newer jar file

## Spark-submit
Running HoS through `spark-submit` requires setting all the configurations in the submit command. Here is an example of how HoS can be started using the `spark-submit` command on a 2 node cluster with 20 CPU cores and ~150 GBytes RAM. Some Spark configurations that should be set are `the number of executors per node`, `the total number of executors`, `cores per executor`, `executor memory`, but most importantly the `executor memoryOverhead` should be set to at least `6 GBytes` to be able to run the tools in that executor memory.

```bash
spark-submit --class be.ugent.intec.halvade.DnaPipeline \
             --master yarn \
             --deploy-mode client \
             --driver-memory 4G \
             --executor-memory 6G \
             --total-executor-cores 40 \
             --executor-cores 20 \
             --num-executors 19 \
             --conf "spark.yarn.executor.memoryOverhead=8192" \
             --conf "spark.task.cpus=2" \
             --jars "$halvadelib/halvade-assembly-1.5.jar,$halvadelib/hadoop-bam-7.10.0.jar,$halvadelib/htsjdk-2.11.0.jar" \
             spark-genomics-lib-assembly-1.3.jar -i $i -o $o -t $excores -m $m -r $ref -b $b -s $s --overwrite --tmp $tmp
```

where `$i` and `$o` are respectivly the input and output folders. `$excores` are the number of CPU cores available per executor, `$m` is the memory (in GBytes) per executor and should be set to approximately `0.7*executor_memory`, in this case 5.4 GBytes. `$ref`, `$b`, `$s` and `$tmp` are respectively the location of the reference file, the binaries directory and the dbsnp file and the tmp folder. More details are given in the documentation.
