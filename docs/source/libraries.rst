Required libraries
------------------

.. note:: This step is not needed when using the Docker version or running on the cloud.

Halvade works with several libraries, the Halvade jar and these libraries needs to be available for Spark.

.. code-block:: bash
	:linenos:

	HALVADE_VERSION=2.0.4
	wget https://bitbucket.org/dries_decap/halvadeforspark/downloads/halvade-assembly-${HALVADE_VERSION}.jar
	HADOOP_BAM_VERSION=7.10.0
	wget https://repo1.maven.org/maven2/org/seqdoop/hadoop-bam/${HADOOP_BAM_VERSION}/hadoop-bam-${HADOOP_BAM_VERSION}.jar
	HTSJDK_VERSION=2.11.0
	wget https://repo1.maven.org/maven2/com/github/samtools/htsjdk/${HTSJDK_VERSION}/htsjdk-${HTSJDK_VERSION}.jar


	.. note:: For Amazon AWS or Google Cloud Engine please use the libraries provided in `halvade-cloud-bundle.tar.gz<https://bitbucket.org/dries_decap/halvadeforspark/downloads/halvade-cloud-bundle.tar.gz>`_ as a bug fix is required in HadoopBAM to run without errors.

Build from source
^^^^^^^^^^^^^^^^^

The Halvade jar can also be obtained by building from the source files. To do this, clone the git repository and built the package with *sbt* as follows:

.. code-block:: bash
	:linenos:

	git clone https://dries_decap@bitbucket.org/dries_decap/halvadeforspark.git
	cd halvadeforspark
	sbt assembly

This will build the jar file in the *target/scala-2.12/* directory.
