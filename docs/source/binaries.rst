Required binaries
=================

.. note:: This step is not needed when using the Docker version.

The binaries of bwa, samtools and GATK need to be available and executable on all nodes in the same directory which will be given as an argument to the Halvade script. In this tutorial we will use the */halvade/bin/* directory for this.

.. code-block:: bash
	:linenos:

	halvadebin=/halvade/bin
	cd $halvadebin

	# get bwa
	BWA_V=0.7.16a
	wget https://github.com/lh3/bwa/releases/download/v0.7.16/bwa-${BWA_V}.tar.bz2
	tar -vxjf bwa-${BWA_V}.tar.bz2
	cd bwa-${BWA_V}
	make
	cp bwa ../
	cd ../
	rm -r bwa-${BWA_V} bwa-${BWA_V}.tar.bz2

	# this needs to be installed on your system for samtools to be able to build:
	sudo yum install -y make gcc zlib-devel bzip2-devel # fedora/centos/amazon aws/google compute etc
	sudo apt-get install -y make gcc zlib1g-dev libbz2-dev # ubuntu/debian etc

	# get samtools
	SAMTOOLS_V=1.11 # needs to be 1.11 or later, for strelka
	wget https://github.com/samtools/samtools/releases/download/${SAMTOOLS_V}/samtools-${SAMTOOLS_V}.tar.bz2
	tar -vxjf samtools-${SAMTOOLS_V}.tar.bz2
	cd samtools-${SAMTOOLS_V}
	./configure --without-curses --disable-lzma # disable curses and lzma for amazon aws/google cloud
	make
	cp samtools ../
	cd ../
	rm -r samtools-${SAMTOOLS_V} samtools-${SAMTOOLS_V}.tar.bz2

	# get GATK
	GATK_V=4.1.2.0
	wget https://github.com/broadinstitute/gatk/releases/download/${GATK_V}/gatk-${GATK_V}.zip
	unzip -j gatk-${GATK_V}.zip gatk-${GATK_V}/gatk-package-${GATK_V}-local.jar
	rm gatk-${GATK_V}.zip

	# optionally Strelka2
	STRELKA_V=2.9.10
	wget https://github.com/Illumina/strelka/releases/download/v${STRELKA_V}/strelka-${STRELKA_V}.centos6_x86_64.tar.bz2
	tar -xvf strelka-${STRELKA_V}.centos6_x86_64.tar.bz2
	rm strelka-${STRELKA_V}.centos6_x86_64.tar.bz2


Now you should have these three binaries in your halvade binary folder:

- ``bwa``
- ``samtools``
- ``gatk-package-${GATK_V}-local.jar``

And optionally this folder:

- ``strelka-${STRELKA_V}.centos6_x86_64``
