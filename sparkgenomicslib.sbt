name := "halvade"

version := "2.0.6"

scalaVersion := "2.12.12"
val sparkVersion = "3.0.0"

// Apache Spark
libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion % "provided"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "com.github.scopt" %% "scopt" % "4.0.0"
libraryDependencies += "org.seqdoop" % "hadoop-bam" % "7.9.0" % "provided"

test in assembly := {}

scalacOptions := Seq("-unchecked", "-deprecation", "-feature")
fork in Test := true
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
parallelExecution in Test := false
