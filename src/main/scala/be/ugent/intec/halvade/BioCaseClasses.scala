package be.ugent.intec.halvade

import scala.collection.Map // makes sure all maps are the same..

final case class PartitionSplit(partition: Int, split: Int) extends Ordered[PartitionSplit] {
  def compare(that: PartitionSplit): Int = if(this.partition == that.partition) this.split - that.split else this.partition - that.partition
  override def toString() : String = "Partition " + partition + " " + split
}
final case class VariantWithLocation(index: Int, position: Int, ref: String, allele: String) extends Ordered[VariantWithLocation] {
  def compare(that: VariantWithLocation): Int =
    if(this.index == that.index)
      if(this.position == that.position)
        if(this.ref == that.ref) this.allele.compare(that.allele)
        else this.ref.compare(that.ref)
      else this.position - that.position
    else this.index - that.index
}
final case class DictIndexAndPosition(index: Int, position: Int) extends Ordered[DictIndexAndPosition] {
  def this() = this(-1, -1)
  def compare(that: DictIndexAndPosition): Int = if(this.index == that.index) this.position - that.position else this.index - that.index
  override def toString() : String = "IdxAndPos " + index + " " + position
}
final case class RecordWithPosition(var record: String, position: DictIndexAndPosition) extends Ordered[RecordWithPosition] {
  def compare(that: RecordWithPosition): Int = this.position.compare(that.position)
}
final case class RecordWithAllPositions(var record: String, positions: Array[DictIndexAndPosition])

final case class SplitIdAndRecordWithPosition(split: Int, record: String, position: DictIndexAndPosition) extends Ordered[SplitIdAndRecordWithPosition] {
  def this() = this(-1, null, null)
  def compare(that: SplitIdAndRecordWithPosition): Int = if(this.split == that.split) this.position.compare(that.position) else this.split - that.split
  override def toString = record + '\n'
}

object BioCaseClassFunctions {
  def minDistance(a: DictIndexAndPosition, b: (DictIndexAndPosition, DictIndexAndPosition)) : Int = {
    if(a.index != b._1.index) Int.MaxValue
    else {
      return if(a.position >= b._1.position && a.position <= b._2.position) 0 // inside the region!
      else Math.min(Math.abs(a.position - b._1.position), Math.abs(a.position - b._2.position))
    }
  }
  def getDistance(a: DictIndexAndPosition, b: DictIndexAndPosition) : Int = {
    if(a.index != b.index) Int.MaxValue
    else
      Math.abs(a.position - b.position)
  }
  def getDistance(a: DictIndexAndPosition, b: DictIndexAndPosition, dictIndexToSize: Map[Int, Int]) : Int = {
    if(a.index == b.index) {
      Math.abs(a.position - b.position)
    } else if (a.index > b.index){
      var pos = dictIndexToSize(b.index) - b.position
      var idx = b.index + 1
      while(idx < a.index) {
        pos += dictIndexToSize(idx)
        idx+=1
      }
      pos += a.position
      pos
    } else {
      var pos = dictIndexToSize(a.index) - a.position
      var idx = a.index + 1
      while(idx < b.index) {
        pos += dictIndexToSize(idx)
        idx+=1
      }
      pos += b.position
      pos
    }
  }
  // assumes same index!
  def getMiddle(a: (DictIndexAndPosition, DictIndexAndPosition)) : DictIndexAndPosition = {
    DictIndexAndPosition(a._1.index, (a._1.position + a._2.position) / 2)
  }
}
