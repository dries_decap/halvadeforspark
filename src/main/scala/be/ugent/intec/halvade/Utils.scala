package be.ugent.intec.halvade

import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import be.ugent.intec.halvade.spark.partitioner.TrioPartitioner
import be.ugent.intec.halvade.spark.input.UnsplittableTextInputFormat
import be.ugent.intec.halvade.FileUtils._
import java.util.StringTokenizer
import java.util.zip._
import java.net.URI
import java.io._
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.RDD.rddToOrderedRDDFunctions
import org.apache.spark.SparkContext
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.conf.Configuration
import org.apache.log4j.Logger
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import scala.collection.JavaConverters._
import scala.collection.Map // makes sure all maps are the same..

object Utils extends Logging {
  var time: Long = 0
  var totaltime: Long = 0

  def time[A](title: String, f: => A) = {
    val s = System.nanoTime
    val ret = f
    info("[" + title + "] time (s): "+(System.nanoTime-s)/1.0e9)
    ret
  }
  def startTime() = {
    totaltime = System.nanoTime
    time = System.nanoTime
  }
  def measureTotalTime(title: String) :String = {
    "[" + title + "] total time (s): "+(System.nanoTime-totaltime)/1.0e9
  }
  def measureTime(title: String) :String = {
    val res = "[" + title + "] time (s): "+(System.nanoTime-time)/1.0e9
    time = System.nanoTime
    res
  }

  /**
  *                                                             Consumes  Consumes
  Op  BAM Description                                             query  reference
  M   0   alignment match (can be a sequence match or mismatch)   yes   yes
  I   1   insertion to the reference                              yes   no
  D   2   deletion from the reference                             no    yes
  N   3   skipped region from the reference                       no    yes
  S   4   soft clipping (clipped sequences present in SEQ)        yes   no
  H   5   hard clipping (clipped sequences NOT present in SEQ)    no    no
  P   6   padding (silent deletion from padded reference)         no    no
  =   7   sequence match                                          yes   yes
  X   8   sequence mismatch                                       yes   yes
  *
  */
  def getRecordLengthFromCigar(cigar: String) : Int = {
    // ascii numbers 0 to 9 = 48 to 57 (< 58)
    var nr = 0
    var len = 0
    for (f <- cigar){
      if (f < 48 || f > 57){ // digit
        if(f == 'M' || f == '=' || f == 'X' || f == 'D' || f == 'N') /// all that consume reference bases are considered motif length, what is covered on the reference
          len += nr
        nr = 0
      } else {
        nr = nr*10 + (f-48)
      }
    }
    len
  }

  def prepVcfFile(input: RDD[String], chromosomeList: Array[String], trioName: String, dict: Map[String, Int]) : RDD[((String, Int, Int), String)] = {
    input.filter(x => (!x.startsWith("#") && chromosomeList.contains(x.split("\t")(0)) ))
                     .map(x => {
                         val tmp = x.split("\t");
                         ((trioName, dict(tmp(0)), tmp(1).toInt), x)
                         })
  }
  def removeDuplicateLines(input: RDD[(String, String)]) : RDD[(String, String)] = {
    input.mapPartitions(it => {
                         var ll = scala.collection.mutable.MutableList[(String, String)]()
                         var prev = it.next();
                         var cur = it.next();
                         while(it.hasNext) {
                           val prevarr = prev._2.split("\t")
                           val curarr = cur._2.split("\t")
                           var prevp =prevarr(0)
                           var curp = curarr(0)
                           if(prevarr.size >= 2 && curarr.size >=2) {
                             prevp += "\t" + prevarr(1)
                             curp += "\t" + curarr(1)
                           }
                           if(prevp != curp) {
                             ll += prev
                             prev = cur
                           }
                           cur = it.next();
                         }
                         ll += cur
                         ll.iterator
                       })
  }

  def parseMemory(memory: String) : Int = { // returns memory string to memory in megabytes
    val tmp = memory.toLowerCase()
    if(tmp.contains("g")) {
      return tmp.dropRight(1).toInt * 1024
    } else if (tmp.contains("m")) {
      return tmp.dropRight(1).toInt
    } else if (tmp.contains("k")) {
      return tmp.dropRight(1).toInt / 1024
    } else { // in megabytes of not specified
      return tmp.toInt
      // throw new Exception("Invalid memory string: " + memory)
    }
  }

  val humanReadableSuffix = Array(" B", " kB", " MB", " GB", " TB", " PB", " EB")
  def humanReadableMemory(memory: Long) : String = {
    var tmp = memory
    var factor = 0
    while(tmp > 1024){ tmp = tmp / 1024; factor += 1}
    return tmp + humanReadableSuffix(factor)
  }

  // MERGE VCF FUNCTIONS
  // SAVE REGIONS TO CHECK SPLITS
  def saveGatkRegions(sc: SparkContext, gatk_regions: Map[PartitionSplit, Array[String]], output: String, overwrite: Boolean) = {
    val conf_ = new Configuration(sc.hadoopConfiguration)
    val myfs =FileSystem.get(new URI(output), conf_);
    if(overwrite) assert(deleteRecursively(new File(output)))

    val dataStream = myfs.create(new Path(output), true);
    val region_writer = new PrintWriter(if(output.endsWith("gz")) new GZIPOutputStream(dataStream) else dataStream)

    for((partitionsplit, region) <- gatk_regions) {
      val line = partitionsplit + "\t" + region.mkString(" ")
      region_writer.println(line)
    }

    region_writer.close()
    dataStream.close()
  }

  // MERGE VCF FILES INTO A SINGLE FILE
  def mergeVcfFiles(sc: SparkContext, dict: Map[String, DictIndexAndPosition], input: String, headerFile: String) : RDD[String] = {

     val variantsWithHeaders = sc.textFile(input)
     val header = if(headerFile == null) variantsWithHeaders.mapPartitionsWithIndex((idx, iter) => if (idx == 0) iter else Iterator()).filter(x => x.startsWith("#")).collect
      else sc.textFile(headerFile).filter(x => x.startsWith("#")).collect
     deduplicateVariantsAndAddHeader(sc, dict, variantsWithHeaders, header)
  }

  protected val vcfFirstLine = "##fileformat=VCFv4.1"
  protected val somaticColumnHeaders = "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tFIRST\tSECOND"
  protected val germlineColumnHeaders = "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tSAMPLE"
  protected val haplotypecallerAlts = Array("##ALT=<ID=NON_REF,Description=\"Represents any possible alternative allele at this location\">")
  protected val haplotypecallerInfos = Array("##INFO=<ID=BaseQRankSum,Number=1,Type=Float,Description=\"Z-score from Wilcoxon rank sum test of Alt Vs. Ref base qualities\">",
    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Approximate read depth; some reads may have been filtered\">",
    "##INFO=<ID=DS,Number=0,Type=Flag,Description=\"Were any of the samples downsampled?\">",
    "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">",
    "##INFO=<ID=ExcessHet,Number=1,Type=Float,Description=\"Phred-scaled p-value for exact test of excess heterozygosity\">",
    "##INFO=<ID=InbreedingCoeff,Number=1,Type=Float,Description=\"Inbreeding coefficient as estimated from the genotype likelihoods per-sample when compared against the Hardy-Weinberg expectation\">",
    "##INFO=<ID=MLEAC,Number=A,Type=Integer,Description=\"Maximum likelihood expectation (MLE) for the allele counts (not necessarily the same as the AC), for each ALT allele, in the same order as listed\">",
    "##INFO=<ID=MLEAF,Number=A,Type=Float,Description=\"Maximum likelihood expectation (MLE) for the allele frequency (not necessarily the same as the AF), for each ALT allele, in the same order as listed\">",
    "##INFO=<ID=MQRankSum,Number=1,Type=Float,Description=\"Z-score From Wilcoxon rank sum test of Alt vs. Ref read mapping qualities\">",
    "##INFO=<ID=RAW_MQandDP,Number=2,Type=Integer,Description=\"Raw data (sum of squared MQ and total depth) for improved RMS Mapping Quality calculation. Incompatible with deprecated RAW_MQ formulation.\">",
    "##INFO=<ID=ReadPosRankSum,Number=1,Type=Float,Description=\"Z-score from Wilcoxon rank sum test of Alt vs. Ref read position bias\">")
  protected val haplotypecallerFormats = Array("##FORMAT=<ID=AD,Number=R,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">",
    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Approximate read depth (reads with MQ=255 or with bad mates are filtered)\">",
    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">",
    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">",
    "##FORMAT=<ID=MIN_DP,Number=1,Type=Integer,Description=\"Minimum DP observed within the GVCF block\">",
    "##FORMAT=<ID=PGT,Number=1,Type=String,Description=\"Physical phasing haplotype information, describing how the alternate alleles are phased in relation to one another\">",
    "##FORMAT=<ID=PID,Number=1,Type=String,Description=\"Physical phasing ID information, where each unique ID within a given sample (but not across samples) connects records within a phasing group\">",
    "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">",
    "##FORMAT=<ID=PS,Number=1,Type=Integer,Description=\"Phasing set (typically the position of the first variant in the set)\">",
    "##FORMAT=<ID=SB,Number=4,Type=Integer,Description=\"Per-sample component statistics which comprise the Fisher's Exact Test to detect strand bias.\">")
  protected val haplotypecallerFilters = Array("##FILTER=<ID=LowQual,Description=\"Low quality\">")

  protected val mutectFormats = Array("##FORMAT=<ID=AD,Number=R,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">",
    "##FORMAT=<ID=AF,Number=A,Type=Float,Description=\"Allele fractions of alternate alleles in the tumor\">",
    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Approximate read depth (reads with MQ=255 or with bad mates are filtered)\">",
    "##FORMAT=<ID=F1R2,Number=R,Type=Integer,Description=\"Count of reads in F1R2 pair orientation supporting each allele\">",
    "##FORMAT=<ID=F2R1,Number=R,Type=Integer,Description=\"Count of reads in F2R1 pair orientation supporting each allele\">",
    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">",
    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">",
    "##FORMAT=<ID=PGT,Number=1,Type=String,Description=\"Physical phasing haplotype information, describing how the alternate alleles are phased in relation to one another\">",
    "##FORMAT=<ID=PID,Number=1,Type=String,Description=\"Physical phasing ID information, where each unique ID within a given sample (but not across samples) connects records within a phasing group\">",
    "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">",
    "##FORMAT=<ID=PS,Number=1,Type=Integer,Description=\"Phasing set (typically the position of the first variant in the set)\">",
    "##FORMAT=<ID=SB,Number=4,Type=Integer,Description=\"Per-sample component statistics which comprise the Fisher's Exact Test to detect strand bias.\">")
  protected val mutectInfos = Array("##INFO=<ID=CONTQ,Number=1,Type=Float,Description=\"Phred-scaled qualities that alt allele are not due to contamination\">",
    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Approximate read depth; some reads may have been filtered\">",
    "##INFO=<ID=ECNT,Number=1,Type=Integer,Description=\"Number of events in this haplotype\">",
    "##INFO=<ID=GERMQ,Number=1,Type=Integer,Description=\"Phred-scaled quality that alt alleles are not germline variants\">",
    "##INFO=<ID=MBQ,Number=R,Type=Integer,Description=\"median base quality\">",
    "##INFO=<ID=MFRL,Number=R,Type=Integer,Description=\"median fragment length\">",
    "##INFO=<ID=MMQ,Number=R,Type=Integer,Description=\"median mapping quality\">",
    "##INFO=<ID=MPOS,Number=A,Type=Integer,Description=\"median distance from end of read\">",
    "##INFO=<ID=NALOD,Number=A,Type=Float,Description=\"Negative log 10 odds of artifact in normal with same allele fraction as tumor\">",
    "##INFO=<ID=NCount,Number=1,Type=Integer,Description=\"Count of N bases in the pileup\">",
    "##INFO=<ID=NLOD,Number=A,Type=Float,Description=\"Normal log 10 likelihood ratio of diploid het or hom alt genotypes\">",
    "##INFO=<ID=OCM,Number=1,Type=Integer,Description=\"Number of alt reads whose original alignment doesn't match the current contig.\">",
    "##INFO=<ID=PON,Number=0,Type=Flag,Description=\"site found in panel of normals\">",
    "##INFO=<ID=POPAF,Number=A,Type=Float,Description=\"negative log 10 population allele frequencies of alt alleles\">",
    "##INFO=<ID=ROQ,Number=1,Type=Float,Description=\"Phred-scaled qualities that alt allele are not due to read orientation artifact\">",
    "##INFO=<ID=RPA,Number=.,Type=Integer,Description=\"Number of times tandem repeat unit is repeated, for each allele (including reference)\">",
    "##INFO=<ID=RU,Number=1,Type=String,Description=\"Tandem repeat unit (bases)\">",
    "##INFO=<ID=SEQQ,Number=1,Type=Integer,Description=\"Phred-scaled quality that alt alleles are not sequencing errors\">",
    "##INFO=<ID=STR,Number=0,Type=Flag,Description=\"Variant is a short tandem repeat\">",
    "##INFO=<ID=STRANDQ,Number=1,Type=Integer,Description=\"Phred-scaled quality of strand bias artifact\">",
    "##INFO=<ID=STRQ,Number=1,Type=Integer,Description=\"Phred-scaled quality that alt alleles in STRs are not polymerase slippage errors\">",
    "##INFO=<ID=TLOD,Number=A,Type=Float,Description=\"Log 10 likelihood ratio score of variant existing versus not existing\">",
    "##INFO=<ID=UNIQ_ALT_READ_COUNT,Number=1,Type=Integer,Description=\"Number of ALT reads with unique start and mate end positions at a variant site\">")
  protected val mutectVersion = "##MutectVersion=2.2" // which is version? how to know? does it matter?
  protected val mutectFilterWarning = "##filtering_status=Warning: unfiltered Mutect 2 calls.  Please run FilterMutectCalls to remove false positives."
  protected val strelkaIndelInfos = Array("##INFO=<ID=QSI,Number=1,Type=Integer,Description=\"Quality score for any somatic variant, ie. for the ALT haplotype to be present at a significantly different frequency in the tumor and normal\">",
    "##INFO=<ID=TQSI,Number=1,Type=Integer,Description=\"Data tier used to compute QSI\">",
    "##INFO=<ID=NT,Number=1,Type=String,Description=\"Genotype of the normal in all data tiers, as used to classify somatic variants. One of {ref,het,hom,conflict}.\">",
    "##INFO=<ID=QSI_NT,Number=1,Type=Integer,Description=\"Quality score reflecting the joint probability of a somatic variant and NT\">",
    "##INFO=<ID=TQSI_NT,Number=1,Type=Integer,Description=\"Data tier used to compute QSI_NT\">",
    "##INFO=<ID=SGT,Number=1,Type=String,Description=\"Most likely somatic genotype excluding normal noise states\">",
    "##INFO=<ID=RU,Number=1,Type=String,Description=\"Smallest repeating sequence unit in inserted or deleted sequence\">",
    "##INFO=<ID=RC,Number=1,Type=Integer,Description=\"Number of times RU repeats in the reference allele\">",
    "##INFO=<ID=IC,Number=1,Type=Integer,Description=\"Number of times RU repeats in the indel allele\">",
    "##INFO=<ID=IHP,Number=1,Type=Integer,Description=\"Largest reference interrupted homopolymer length intersecting with the indel\">",
    "##INFO=<ID=MQ,Number=1,Type=Float,Description=\"RMS Mapping Quality\">",
    "##INFO=<ID=MQ0,Number=1,Type=Integer,Description=\"Total Mapping Quality Zero Reads\">",
    "##INFO=<ID=SOMATIC,Number=0,Type=Flag,Description=\"Somatic mutation\">",
    "##INFO=<ID=OVERLAP,Number=0,Type=Flag,Description=\"Somatic indel possibly overlaps a second indel.\">",
    "##INFO=<ID=SomaticEVS,Number=1,Type=Float,Description=\"Somatic Empirical Variant Score (EVS) expressing the phred-scaled probability of the call being a false positive observation.\">")
  protected val strelkaIndelFormats = Array("##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read depth for tier1\">",
    "##FORMAT=<ID=DP2,Number=1,Type=Integer,Description=\"Read depth for tier2\">",
    "##FORMAT=<ID=TAR,Number=2,Type=Integer,Description=\"Reads strongly supporting alternate allele for tiers 1,2\">",
    "##FORMAT=<ID=TIR,Number=2,Type=Integer,Description=\"Reads strongly supporting indel allele for tiers 1,2\">",
    "##FORMAT=<ID=TOR,Number=2,Type=Integer,Description=\"Other reads (weak support or insufficient indel breakpoint overlap) for tiers 1,2\">",
    "##FORMAT=<ID=DP50,Number=1,Type=Float,Description=\"Average tier1 read depth within 50 bases\">",
    "##FORMAT=<ID=FDP50,Number=1,Type=Float,Description=\"Average tier1 number of basecalls filtered from original read depth within 50 bases\">",
    "##FORMAT=<ID=SUBDP50,Number=1,Type=Float,Description=\"Average number of reads below tier1 mapping quality threshold aligned across sites within 50 bases\">",
    "##FORMAT=<ID=BCN50,Number=1,Type=Float,Description=\"Fraction of filtered reads within 50 bases of the indel.\">")
  protected val strelkaIndelFilters = Array("##FILTER=<ID=LowEVS,Description=\"Somatic Empirical Variant Score (SomaticEVS) is below threshold\">",
    "##FILTER=<ID=LowDepth,Description=\"Tumor or normal sample read depth at this locus is below 2\">",
    "##FILTER=<ID=HighDepth,Description=\"Locus depth is greater than 3x the mean chromosome depth in the normal sample\">")
  protected val strelkaSnvsInfos = Array("##INFO=<ID=QSS,Number=1,Type=Integer,Description=\"Quality score for any somatic snv, ie. for the ALT allele to be present at a significantly different frequency in the tumor and normal\">",
    "##INFO=<ID=TQSS,Number=1,Type=Integer,Description=\"Data tier used to compute QSS\">",
    "##INFO=<ID=NT,Number=1,Type=String,Description=\"Genotype of the normal in all data tiers, as used to classify somatic variants. One of {ref,het,hom,conflict}.\">",
    "##INFO=<ID=QSS_NT,Number=1,Type=Integer,Description=\"Quality score reflecting the joint probability of a somatic variant and NT\">",
    "##INFO=<ID=TQSS_NT,Number=1,Type=Integer,Description=\"Data tier used to compute QSS_NT\">",
    "##INFO=<ID=SGT,Number=1,Type=String,Description=\"Most likely somatic genotype excluding normal noise states\">",
    "##INFO=<ID=SOMATIC,Number=0,Type=Flag,Description=\"Somatic mutation\">",
    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Combined depth across samples\">",
    "##INFO=<ID=MQ,Number=1,Type=Float,Description=\"RMS Mapping Quality\">",
    "##INFO=<ID=MQ0,Number=1,Type=Integer,Description=\"Total Mapping Quality Zero Reads\">",
    "##INFO=<ID=ReadPosRankSum,Number=1,Type=Float,Description=\"Z-score from Wilcoxon rank sum test of Alt Vs. Ref read-position in the tumor\">",
    "##INFO=<ID=SNVSB,Number=1,Type=Float,Description=\"Somatic SNV site strand bias\">",
    "##INFO=<ID=PNOISE,Number=1,Type=Float,Description=\"Fraction of panel containing non-reference noise at this site\">",
    "##INFO=<ID=PNOISE2,Number=1,Type=Float,Description=\"Fraction of panel containing more than one non-reference noise obs at this site\">",
    "##INFO=<ID=SomaticEVS,Number=1,Type=Float,Description=\"Somatic Empirical Variant Score (EVS) expressing the phred-scaled probability of the call being a false positive observation.\">")
  protected val strelkaSnvsFormats = Array("##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read depth for tier1 (used+filtered)\">",
    "##FORMAT=<ID=FDP,Number=1,Type=Integer,Description=\"Number of basecalls filtered from original read depth for tier1\">",
    "##FORMAT=<ID=SDP,Number=1,Type=Integer,Description=\"Number of reads with deletions spanning this site at tier1\">",
    "##FORMAT=<ID=SUBDP,Number=1,Type=Integer,Description=\"Number of reads below tier1 mapping quality threshold aligned across this site\">",
    "##FORMAT=<ID=AU,Number=2,Type=Integer,Description=\"Number of 'A' alleles used in tiers 1,2\">",
    "##FORMAT=<ID=CU,Number=2,Type=Integer,Description=\"Number of 'C' alleles used in tiers 1,2\">",
    "##FORMAT=<ID=GU,Number=2,Type=Integer,Description=\"Number of 'G' alleles used in tiers 1,2\">",
    "##FORMAT=<ID=TU,Number=2,Type=Integer,Description=\"Number of 'T' alleles used in tiers 1,2\">")
  protected val strelkaSnvsFilters = Array("##FILTER=<ID=LowEVS,Description=\"Somatic Empirical Variant Score (SomaticEVS) is below threshold\">",
    "##FILTER=<ID=LowDepth,Description=\"Tumor or normal sample read depth at this locus is below 2\">")
  protected val strelkaSource = "##source=strelka"
  protected val strelkaContent = Array("##content=strelka somatic snv/indel calls", "##priorSomaticSnvRate=0.0001", "##priorSomaticIndelRate=1e-06")

  def createMutect2Header(dict: Map[String, DictIndexAndPosition], tumor_sm: String, normal_sm: String) : Array[String] = {
    //"##Callable=" + callable.toInt.toFloat
    // the order of tumor/normal is alphabetical in gatk so we need to do this as well:
    val first = if (normal_sm < tumor_sm) normal_sm else tumor_sm;
    val second = if (normal_sm < tumor_sm) tumor_sm else normal_sm;
    return Array(vcfFirstLine) ++ mutectFormats  ++ mutectInfos ++ Array(mutectVersion) ++ dict.toArray.sortBy(_._2.index).map(x => "##contig=<ID=" + x._1 + ",length=" + x._2.position + ">") ++
           Array(mutectFilterWarning, "##normal_sample=".concat(normal_sm), "##tumor_sample=".concat(tumor_sm), somaticColumnHeaders.replace("FIRST", first).replace("SECOND", second))

  }
  def createStrelka2Header(dict: Map[String, DictIndexAndPosition], chromDepth: Map[String, Long], tumor_sm: String, normal_sm: String) : Array[String] = {
    val first = if (normal_sm < tumor_sm) normal_sm else tumor_sm;
    val second = if (normal_sm < tumor_sm) tumor_sm else normal_sm;
    return Array(vcfFirstLine) ++ Array(strelkaSource) ++ dict.toArray.sortBy(_._2.index).map(x => "##contig=<ID=" + x._1 + ",length=" + x._2.position + ">") ++
           strelkaIndelInfos ++ strelkaSnvsInfos ++ strelkaIndelFormats ++ strelkaSnvsFormats ++ strelkaIndelFilters ++ strelkaSnvsFilters ++
           chromDepth.map(x => "##Depth_" + x._1 + "=" + x._2) ++ Array(somaticColumnHeaders.replace("FIRST", first).replace("SECOND", second)) // also alphabetical i assume?
  }
  def createHaplotypeCallerHeader(dict: Map[String, DictIndexAndPosition], samplename: String) : Array[String] = {
    return Array(vcfFirstLine) ++ haplotypecallerAlts ++ haplotypecallerFormats  ++ haplotypecallerInfos ++ dict.toArray.sortBy(_._2.index).map(x => "##contig=<ID=" + x._1 + ",length=" + x._2.position + ">") ++
           Array(germlineColumnHeaders.replace("SAMPLE", samplename))

  }

  def getVariantInfoMaps(a: String, b: String) : (Map[String, String], Map[String, String]) = {
    val aMap = a.split("\t")(7).split(";").flatMap(x => {
      val tmp = x.split("=");
      if(tmp.size == 2)
        Some((tmp(0), tmp(1)))
      else {
        // info("problem in reading INFO field of " + a)
        None
      }
    }).toMap // array index 1 out of bound error?? tmp(1)?
    val bMap = b.split("\t")(7).split(";").flatMap(x => {
      val tmp = x.split("=");
      if(tmp.size == 2)
        Some((tmp(0), tmp(1)))
      else {
        // info("problem in reading INFO field of " + b)
        None
      }
    }).toMap
    (aMap, bMap)
  }
  def compareMutect2Variants(a: String, b: String) : Int = {
    val (aMap, bMap) = getVariantInfoMaps(a, b)
    if (aMap.getOrElse("TLOD", "0.0").split(",").map(_.toFloat).max < bMap.getOrElse("TLOD", "0.0").split(",").map(_.toFloat).max)
      return -1
    else
      return 1
  }
  def compareStrelka2Variants(a: String, b: String) : Int = {
    val (aMap, bMap) = getVariantInfoMaps(a, b)
    if (aMap.getOrElse("SomaticEVS", "0.0").split(",").map(_.toFloat).max < bMap.getOrElse("SomaticEVS", "0.0").split(",").map(_.toFloat).max)
      return -1
    else
      return 1
  }
  def compareVariantsByDepth(a: String, b: String) : Int = {
    val (aMap, bMap) = getVariantInfoMaps(a, b)
    if (aMap.getOrElse("DP", "0").split(",").map(_.toInt).max < bMap.getOrElse("DP", "0").split(",").map(_.toInt).max)
      return -1
    else
      return 1
  }
  def deduplicateVariantsAndAddHeader(sc: SparkContext, dict: Map[String, DictIndexAndPosition], variants: RDD[String], header: Array[String],
        compareVariants: (String, String) => Int = compareVariantsByDepth) : RDD[String] = {
     val finalvcf = variants.map(x => {
       val tmp = x.split("\t");
       (VariantWithLocation(dict(tmp(0)).index, tmp(1).toInt, tmp(3), tmp(4)), x) // make unique by position, only one call by position!
     }).reduceByKey((a, b) => if(compareVariants(a, b) > 0) a else b).sortByKey().map(_._2)
     finalvcf.mapPartitionsWithIndex((idx,it) => { if (idx == 0) header.iterator ++ it else it })
  }

  def saveMergedVcfFiles(sc: SparkContext, input: String, output: String, deleteInput: Boolean, callable: Option[Long] = None) = {
    val conf_ = new Configuration(sc.hadoopConfiguration)
    val outfs = FileSystem.get(new URI(output), conf_);
    val infs = FileSystem.get(new URI(input), conf_);

    if(callable.isDefined) {
      val headerline = "statistic\tvalue"
      val line = "callable\t" + callable.get.toInt.toFloat //-1.47063386E9
      val dataStream = outfs.create(new Path(output + ".stats"), true);
      val stats_writer = new PrintWriter(dataStream)
      stats_writer.println(headerline)
      stats_writer.println(line)
      stats_writer.close()
      dataStream.close()
    }

    val dataStream = outfs.create(new Path(output), true);
    val vcf_writer = new PrintWriter(if(output.endsWith("gz")) new GZIPOutputStream(dataStream) else dataStream)
    val outfiles = infs.listStatus(new Path(input)).filter(!_.isDirectory).map(_.getPath).sortBy(x => x.getName)

    val total = outfiles.length
    var active = 0
    var completed = 0
    showProgressBarNonSparkFunctions("Merging", completed, active, total)
    active = 1
    for(fname<-outfiles) {
      val in: InputStream = infs.open(fname);
      val data = scala.io.Source.fromInputStream(in).getLines.mkString("\n")
      if (data.size > 0) vcf_writer.print(data + '\n')
      completed += 1
      showProgressBarNonSparkFunctions("Merging", completed, active, total)
    }
    vcf_writer.close()
    dataStream.close()

    if (deleteInput) assert(infs.delete(new Path(input), true))
    cleanProgressBarNonSparkFunctions()
  }


// PROGRESS BAR SUPPORT

  private def withSuffix(count: Long): String = {
      if (count < 1000) return "" + count;
      val exp: Int = (Math.log(count) / Math.log(1000)).toInt;
      return "%.1f%c".format(count / Math.pow(1000, exp), "kMGTPE".charAt(exp-1));
  }

  val CR = '\r'
  val TerminalWidth = if (!sys.env.getOrElse("COLUMNS", "").isEmpty) {
    sys.env.get("COLUMNS").get.toInt
  } else {
    80
  }
  var lastProgressBar = ""
  var lastUpdateTime = 0L
  def showProgressBarNonSparkFunctions(Title: String, completedTasks: Long, activeTasks: Long, totalTasks: Long) {

    val now = System.currentTimeMillis()
    val header = s"[${Title}:"
    val tailer : String = if(activeTasks == -1) {
      val completedTasks_ : String = withSuffix(completedTasks)
      val totalTasks_ : String = withSuffix(totalTasks)
      s"${completedTasks_} / $totalTasks_]"
    } else {
      val completedTasks_ : String = withSuffix(completedTasks)
      val activeTasks_ : String = withSuffix(activeTasks)
      val totalTasks_ : String = withSuffix(totalTasks)
      s"(${completedTasks_} + ${activeTasks_}) / $totalTasks_]"
    }
    val w = TerminalWidth - header.length - tailer.length
    val bar1 = if (w > 0) {
      val percent = if (totalTasks == 0) 1 else w * completedTasks / totalTasks
      (0 until w).map { i =>
        if (i < percent) "=" else if (i == percent) ">" else " "
      }.mkString("")
    } else {
      ""
    }
    val bar = header + bar1 + tailer

    // only refresh if it's changed of after 1 minute (or the ssh connection will be closed
    // after idle some time)
    if (bar != lastProgressBar || now - lastUpdateTime > 60 * 1000L) {
      System.err.print(CR + bar)
      lastUpdateTime = now
    }
    lastProgressBar = bar
  }

  def cleanProgressBarNonSparkFunctions() {
    if (!lastProgressBar.isEmpty) {
      System.err.printf(CR + " " * TerminalWidth + CR)
      lastProgressBar = ""
    }
  }


  private def tokenize(command: String): Seq[String] = {
    val buf = new ListBuffer[String]
    val tok = new StringTokenizer(command)
    while(tok.hasMoreElements) {
      buf += tok.nextToken()
    }
    buf
  }

  //GENOTYPE TRIO FUNCTIONS
  // def getCorrectHeaders(header: Array[String], fixRGSM: String) : (Array[((String, Int, Int), String)], Array[((String, Int, Int), String)], Array[((String, Int, Int), String)]) = {
  //   var indexheader = header.clone()
  //   var fatherheader = header.clone()
  //   var motherheader = header.clone()
  //   if(fixRGSM != null) {
  //    val fixRGSMSplit = fixRGSM.split(";")
  //    val tobeupdated= header.zipWithIndex.filter(x => x._1.contains("#CHROM")).head
  //    indexheader.update(tobeupdated._2, tobeupdated._1.replace(tobeupdated._1.split("\\s+").last,fixRGSMSplit(0)))
  //    fatherheader.update(tobeupdated._2, tobeupdated._1.replace(tobeupdated._1.split("\\s+").last,fixRGSMSplit(1)))
  //    motherheader.update(tobeupdated._2, tobeupdated._1.replace(tobeupdated._1.split("\\s+").last,fixRGSMSplit(2)))
  //   }
  //   val tmpindexheader = indexheader.zipWithIndex.map(x => (("index", -1, x._2 - indexheader.size), x._1) )
  //   val tmpfatherheader = fatherheader.zipWithIndex.map(x => (("father", -1, x._2 - fatherheader.size), x._1) )
  //   val tmpmotherheader = motherheader.zipWithIndex.map(x => (("mother", -1, x._2 - motherheader.size), x._1) )
  //   (tmpindexheader, tmpfatherheader, tmpmotherheader)
  // }
  //
  // def connectRegions(indexrdd: RDD[((String, Int, Int), String)], chrsizes: Map[Int,Int], dictarray: Array[(String, Int)]) : (Map[Int,List[(Int, (Int, Int))]], Map[Int,String]) = {
  //
  //    val regionstmp = indexrdd.map(x => ((-1, x._1._2), (x._1._3, x._1._3)) )
  //                            .mapPartitionsWithIndex((idx, it) => { it.map(x => ((idx, x._1._2), x._2)) })
  //                            .reduceByKey((a,b) => (Math.min(a._1,b._1), Math.max(a._2, b._2)) )
  //   val tmpregionlist = regionstmp.map(x => (x._1._2, Array((x._1._1, (x._2) )))).reduceByKey(_++_).collect.toMap
  //   val regionlist = tmpregionlist.keys.map(x => {
  //    val list = tmpregionlist(x).sortWith((a,b) => (a._1 < b._1))
  //    val bounds = list.map(x => x._2._1)
  //    (x, list.indices.map(i => {
  //      val tmp = list(i);
  //      val max = (if(i<list.size-1) bounds(i+1)-1 else chrsizes(x));
  //      val min = (if(i==0) 1 else tmp._2._1)
  //      (tmp._1, (min, max))
  //    }).toList)
  //   }).toMap // connected regions
  //   val regions = regionlist.map(x => x._2.map(y => (y._1, " -L "+dictarray.filter(z => z._2 == x._1).head._1+ ":" +y._2._1+ "-" +y._2._2)))
  //                           .flatten.groupBy(_._1).map(x => (x._1, x._2.reduceLeft((a,b) => (a._1, a._2 + b._2))._2))
  //   (regionlist, regions)
  // }
  //
  //
  // def sortAndPartitionVcfFiles(sc: SparkContext, index: String, father: String, mother: String, partitions: Int, chromosomeList: Array[String], fixRGSM: String) :
  //     (RDD[(String, String)], Map[Int, Array[String]]) = {
  //
  //   var tmp = ""
  //   if(index.contains(",")) {
  //     tmp = index.split(",")(0) // first file of comma separated list
  //   } else {
  //     val d = new File(index)
  //     if (d.exists && d.isDirectory)
  //       tmp = d.listFiles.filter(x => x.isFile && x.getName.startsWith("part-") )(0).getAbsolutePath
  //   }
  //   val header = sc.textFile(tmp).filter(x => x.startsWith("#")).collect()
  //   val dict = header.filter(x => x.startsWith("##contig")).map(x => x.substring(9).dropRight(1).split(",")(0).split("=")(1)).zipWithIndex.toMap
  //   val indexrdd = prepVcfFile(sc.textFile(index), chromosomeList, "index", dict).sortByKey(true, partitions)
  //
  //   val (regionlist, regions) = connectRegions(indexrdd, chromosomeList.map(x => {(dict(x), header.filter(y => y.contains("ID=" + x + ",")).head.split("=")(3).replace(">","").toInt )}).toMap, dict.toArray)
  //   val (tmpindexheader, tmpfatherheader, tmpmotherheader) = getCorrectHeaders(header, fixRGSM)
  //
  //
  //   val fatherrdd = prepVcfFile(sc.textFile(father), chromosomeList, "father", dict).coalesce(partitions)
  //                             .mapPartitionsWithIndex((idx,it) => tmpfatherheader.map(x => ((x._1._1, -(idx+1), x._1._3), x._2)).iterator++ it)
  //   val motherrdd = prepVcfFile(sc.textFile(mother), chromosomeList, "mother", dict).coalesce(partitions)
  //                             .mapPartitionsWithIndex((idx,it) => tmpmotherheader.map(x => ((x._1._1, -(idx+1), x._1._3), x._2)).iterator ++ it)
  //
  //   val tmpindexrdd =indexrdd.mapPartitionsWithIndex((idx,it) => tmpindexheader.map(x => ((x._1._1, -(idx+1), x._1._3), x._2)).iterator ++ it)
  //   val fullvcf = (tmpindexrdd ++ fatherrdd ++ motherrdd).repartitionAndSortWithinPartitions(new TrioPartitioner(regionlist))
  //                                                        .map(x => (x._1._1, x._2))
  //   (removeDuplicateLines(fullvcf), regions)
  // }
}
