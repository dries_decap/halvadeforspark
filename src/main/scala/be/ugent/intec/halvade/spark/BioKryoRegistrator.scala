package be.ugent.intec.halvade.spark

import com.esotericsoftware.kryo.Kryo
import org.apache.spark.serializer.KryoRegistrator

class BioKryoRegistrator extends KryoRegistrator {
  override def registerClasses(kryo: Kryo) {
    kryo.register(classOf[java.lang.Class[_]])
    kryo.register(classOf[be.ugent.intec.halvade.DictIndexAndPosition]);
    kryo.register(classOf[be.ugent.intec.halvade.RecordWithPosition]);
    kryo.register(classOf[be.ugent.intec.halvade.RecordWithAllPositions]);
    kryo.register(classOf[be.ugent.intec.halvade.PartitionSplit]);
    kryo.register(classOf[be.ugent.intec.halvade.VariantWithLocation]);
    kryo.register(classOf[be.ugent.intec.halvade.SplitIdAndRecordWithPosition]);
    kryo.register(classOf[Array[String]]);
    kryo.register(classOf[Array[be.ugent.intec.halvade.RecordWithPosition]]);
    kryo.register(classOf[Array[be.ugent.intec.halvade.RecordWithAllPositions]]);
    kryo.register(classOf[Array[be.ugent.intec.halvade.DictIndexAndPosition]]);
    kryo.register(classOf[Array[be.ugent.intec.halvade.VariantWithLocation]]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.BqsrReport]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.RecalibrationArgumentCollection]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.BqsrRowData]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.ReportTable0Key]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.ReportTable1Key]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.ReportTable2Key]);
    kryo.register(classOf[be.ugent.intec.halvade.tools.RecalibrationTables]);
    kryo.register(classOf[org.apache.hadoop.conf.Configuration]);
    kryo.register(Class.forName("scala.reflect.ClassTag$GenericClassTag", false, getClass().getClassLoader()))
    kryo.register(Class.forName("scala.reflect.ClassTag$$anon$1", false, getClass().getClassLoader()))
    kryo.register(Class.forName("org.apache.spark.internal.io.FileCommitProtocol$TaskCommitMessage", false, getClass().getClassLoader()))
    kryo.register(Class.forName("scala.collection.Iterator$ConcatIterator", false, getClass().getClassLoader()))
    kryo.register(Class.forName("scala.reflect.ManifestFactory$AnyManifest", false, getClass().getClassLoader()));
    kryo.register(Class.forName("org.apache.spark.util.collection.OpenHashMap$mcJ$sp", false, getClass().getClassLoader()));
    kryo.register(Class.forName("scala.reflect.ManifestFactory$LongManifest", false, getClass().getClassLoader()));
    kryo.register(Class.forName("org.apache.spark.util.collection.OpenHashSet", false, getClass().getClassLoader()));

    kryo.register(scala.reflect.ClassTag(Class.forName("org.apache.spark.util.collection.CompactBuffer")).wrap.runtimeClass)
  }
}
