package be.ugent.intec.halvade.spark.input

import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.hadoop.fs.Path;

class UnsplittableTextInputFormat extends TextInputFormat {
  
  override 
  def isSplitable(context: org.apache.hadoop.mapreduce.JobContext, file: Path) : Boolean = {
    return false 
  }   
}