package be.ugent.intec.halvade.spark.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.Partitioner
import org.apache.spark.ChromosomeRegionRangePartitioner
import scala.collection._
import mutable.ListBuffer
import be.ugent.intec.halvade.Logging
import org.apache.spark.rdd.RDD.rddToOrderedRDDFunctions
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions
import be.ugent.intec.halvade.spark.partitioner.NumberPartitioner
import be.ugent.intec.halvade.Utils._
import scala.language.implicitConversions
import org.apache.log4j.{Level, Logger}
import be.ugent.intec.halvade.spark.SamRecordFunctions._
import scala.collection.Map
import be.ugent.intec.halvade.{RecordWithPosition, RecordWithAllPositions, DictIndexAndPosition, SplitIdAndRecordWithPosition}

@SerialVersionUID(1003L)
class SamRecordRDDFunctions(rdd: RDD[String]) extends Serializable with Logging {

  def determineRegions(dict: Map[String, DictIndexAndPosition], partitions: Int, splits: Int, readLength: Int,
    splitPerChromosome: Boolean): (ChromosomeRegionRangePartitioner, RDD[(Int, SplitIdAndRecordWithPosition)]) = {
      val tmp = rdd.flatMap(record =>  {
        if(record(0) == '@') None
        else {
          val tmp = record.split('\t');
          if(tmp(2) == "*" && (tmp(6) == "*" || tmp(6) == "=")) None
          else {
            val idx = if(tmp(2) != "*") 2 else 6
            val dictidx = dict(tmp(idx)).index
            val dictidxpair = if(tmp(6) == "*" || tmp(6) == "=") dictidx else dict(tmp(6)).index
            val readStart = DictIndexAndPosition(dictidx, tmp(3).toInt)
            val readPairStart = DictIndexAndPosition(dictidxpair, tmp(7).toInt)
            Some((readStart, RecordWithAllPositions(record, Array(readPairStart))))
          }
        }
      })
      val idxtosize = dict.map(x => (x._2.index, x._2.position)).toMap
      val partitioner = new ChromosomeRegionRangePartitioner(partitions*splits, tmp, idxtosize, splitPerChromosome, readLength)
      debug("number of partition splits: " + partitioner.numPartitions)
      (partitioner, overlapSplitsWithPartitioner(tmp, dict, partitioner, splits, readLength))
  }

  def createSplitsWithPartitioner(dict: Map[String, DictIndexAndPosition], partitioner: ChromosomeRegionRangePartitioner, splits: Int, readLength: Int) : RDD[(Int, SplitIdAndRecordWithPosition)] = {
    rdd.flatMap(record =>  {
      if(record(0) == '@') None
      else {
        val tmp = record.split('\t');
        if(tmp(2) == "*" && (tmp(6) == "*" || tmp(6) == "=")) None
        else {
          val idx = if(tmp(2) != "*") 2 else 6
          val dictidx = dict(tmp(idx)).index
          val dictidxpair = if(tmp(6) == "*" || tmp(6) == "=") dictidx else dict(tmp(6)).index
          val readStart = DictIndexAndPosition(dictidx, tmp(3).toInt)
          val readPairStart = Array(DictIndexAndPosition(dictidxpair, tmp(7).toInt))

          val partition = partitioner.getPartition(readStart)
          val set= scala.collection.mutable.Set.empty[Int]
          if(partition < partitioner.rangeBounds.size && implicitly[Ordering[DictIndexAndPosition]].gt(DictIndexAndPosition(readStart.index, readStart.position + readLength), partitioner.rangeBounds(partition)) ) // check readend
            set.add(partition + 1)
          // check if end of read crosses border
          if(readPairStart.nonEmpty) {
            for ( pos <- readPairStart) { // could be optimized if checking difference???
              var part = partitioner.getPartition(pos)
              set.add(part)
              if(part < partitioner.rangeBounds.size && implicitly[Ordering[DictIndexAndPosition]].gt(DictIndexAndPosition(pos.index, pos.position + readLength), partitioner.rangeBounds(part)) )
                set.add(part + 1)
            }
          }
          set.remove(partition) // since we need to tag duplicates, we need to separate!
          Array((partition, SplitIdAndRecordWithPosition(partition %splits, record, readStart))) ++ set.map(part => (part, SplitIdAndRecordWithPosition(part %splits, record + "\tHA:Z:halvade-duplicated", readStart)))
        }
      }
    })
  }

  /**
   * count elements per partition
   * for debugging purposes, like load balancing
   */
  def countPerPartition() : Array[Int] = {
    rdd.mapPartitions(it => Array(it.size.toString).iterator).map(x => x.toInt).collect
  }

  /**
   * Sort functions
   * all assume no header!
   */
  @deprecated ("is incorporated in other functions", "2.0")
  def filterHeaderAndUnmappedRecords() : RDD[String] = {
    rdd.filter(_(0) != '@').filter(x => {val tmp = x.split('\t'); !(tmp(2) == "*" && (tmp(6) == "*" || tmp(6) == "=")) }) // filter unaligned reads! where both are *!
  }
  def sortByReadNameInPartition() : RDD[String] = {
    rdd.mapPartitions(x => x.toArray.sortBy(_.split('\t')(0)).iterator)
  }
  def sortByPositionInPartition(dict: Map[String, DictIndexAndPosition]) : RDD[String] = {
    rdd.mapPartitions(x => x.toArray.sortBy(x => {val tmp = x.split('\t'); (dict(tmp(2)).index, tmp(3).toInt) }).iterator)
  }

  /**
   * Header/ReadGroup functions
   */
  def addHeaderToPartitions(header: Array[String]) : RDD[String] = {
    rdd.mapPartitions(x => header.iterator ++ x)
  }
  def removeHeader() : RDD[String] = {
    rdd.filter(_(0) != '@')
  }
  def addReadgroup(rg_id: String) : RDD[String] = {
    rdd.map(x => x + "\tRG:Z:" + rg_id)
  }

  /**
   * Format Strings as Byte array to be used as binary stream for samtools/gatk etc
   */
  def toBinaryFormat() : RDD[Array[Byte]] = {
    rdd.map(x => (x + '\n').getBytes)
  }
}

object SamRecordRDDFunctions {
  implicit def addSamRecordRDDFunctions(rdd: RDD[String]) = new SamRecordRDDFunctions(rdd)
}
