package be.ugent.intec.halvade.spark

import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import org.apache.spark.rdd.{RDD}
import org.apache.spark.SparkContext
import java.util.concurrent.Executors
import org.apache.spark.ChromosomeRegionRangePartitioner
import java.io.{File, PrintWriter}
import be.ugent.intec.halvade.Logging
import org.apache.log4j.Level
import org.apache.log4j.Logger
import scala.collection.Map // makes sure all maps are the same..
import be.ugent.intec.halvade.spark.partitioner.NumberPartitioner
import be.ugent.intec.halvade.{DictIndexAndPosition, RecordWithAllPositions, RecordWithPosition, PartitionSplit, SplitIdAndRecordWithPosition}
import scala.collection.mutable.ArrayBuffer
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import java.net.URI
import java.io.{FileOutputStream, BufferedOutputStream}

@SerialVersionUID(1004L)
object SamRecordFunctions extends Serializable with Logging {

  protected val SAMHeaderVersion = "1.6"
  protected val SAMHeaderSortOrder = "coordinate"
  protected val timeout = 10
  protected val units = java.util.concurrent.TimeUnit.SECONDS


  def createSamHeader(dict: Map[String, DictIndexAndPosition], readgroups: Array[String]) : Array[String] = {
    Array("@HD	VN:" + SAMHeaderVersion + "	GO:none	SO:" + SAMHeaderSortOrder) ++
    dict.filter(_._1 != "*").toSeq.sortBy(_._2.index).map(x => "@SQ\tSN:" + x._1 + "\tLN:" + x._2.position) ++
    readgroups
  }

  def createReadGroupLines(readgroupIds: Array[String], sample: String, platform: String) : Array[String] = {
    readgroupIds.map(rgid => "@RG\tID:" + rgid + "\tSM:" + sample + "\tPL:" + platform)
  }

  /**
  * overlap function should already be called!
  * two inputs: RDD[(partitionindex: PartitionIndex, record: String)]
  * corresponding read group ids for the inputs
  * final_header has the readgroup lines added already!
  * one output: RDD[(partitionindex: PartitionIndex, record: String)]
  */
  /**
  * BAM INPUT, RG ALREADY IN THE RECORDS
  */
  def mergePartitionAndSortRecords(inputA: RDD[(Int, SplitIdAndRecordWithPosition)], inputB: RDD[(Int, SplitIdAndRecordWithPosition)], partitions: Int, splits: Int): RDD[SplitIdAndRecordWithPosition] = { //}, Iterable[SplitIdAndRecordWithPosition])] = {
    // inputA.coalesce(partitions).cogroup(inputB, new NumberPartitioner(partitions, splits)).map(_._2)
    inputA.union(inputB).coalesce(partitions).partitionBy(new NumberPartitioner(partitions, splits)).map(_._2)
  }

  /**
  * overlap function should already be called!
  * IMPORTANT: number of partitions should already be correct!
  * input: RDD[(partitionindex: PartitionIndex, record: String)]
  * corresponding read group ids for the input
  * final_header has the readgroup lines added already!
  * output: RDD[(partitionindex: PartitionIndex, record: String)]
  */
  def partitionRecords(input: RDD[(Int, SplitIdAndRecordWithPosition)], partitions: Int, splits: Int) : RDD[SplitIdAndRecordWithPosition] = {
    input.coalesce(partitions).partitionBy(new NumberPartitioner(partitions, splits)).map(_._2)
  }

  def overlapSplitsWithPartitioner(rdd: RDD[(DictIndexAndPosition, RecordWithAllPositions)], dict: Map[String, DictIndexAndPosition], partitioner: ChromosomeRegionRangePartitioner, splits: Int, readLength: Int) : RDD[(Int, SplitIdAndRecordWithPosition)] = {
    rdd.flatMap(record =>  {
      val partition = partitioner.getPartition(record._1)
      val set= scala.collection.mutable.Set.empty[Int]
      if(partition < partitioner.rangeBounds.size && implicitly[Ordering[DictIndexAndPosition]].gt(DictIndexAndPosition(record._1.index, record._1.position + readLength), partitioner.rangeBounds(partition)) )
        set.add(partition + 1)
      // check if end of read crosses border
      if(record._2.positions.nonEmpty) {
        for ( pos <- record._2.positions) { // could be optimized if checking difference???
          var part = partitioner.getPartition(pos)
          set.add(part)
          if(part < partitioner.rangeBounds.size && implicitly[Ordering[DictIndexAndPosition]].gt(DictIndexAndPosition(pos.index, pos.position + readLength), partitioner.rangeBounds(part)) )
            set.add(part + 1)
        }
      }
      set.remove(partition) // since we need to tag duplicates, we need to separate!
      Array((partition, SplitIdAndRecordWithPosition(partition %splits, record._2.record, record._1))) ++ set.map(part => (part, SplitIdAndRecordWithPosition(part %splits, record._2.record+ "\tHA:Z:halvade-duplicated", record._1)))
    })
  }
  /**
  * reformat the regions to gatk format
  * current: (chr string, (partition idx, (start, stop)))
  * format to (partition idx, region string)
  * list of lines like this: chr20:1-100
  */
  def collectRegionsForPartitionSplits(dict: Map[String, DictIndexAndPosition], partitioner:  ChromosomeRegionRangePartitioner, splits: Int) : Map[PartitionSplit, Array[String]] = {
    var upper_bounds = partitioner.rangeBounds
    var map: scala.collection.mutable.Map[PartitionSplit, Array[String]] = scala.collection.mutable.Map[PartitionSplit, Array[String]]()
    val dictIndexToChromosomeAndSizeMap: Map[Int, (String, Int)] = dict.map(x => (x._2.index, (x._1, x._2.position)))
    var currentBound = DictIndexAndPosition(0, 1) // first range starts from index 0 (chr 1 ) pos 1
    var i = 0
    while (i < upper_bounds.size) {
      if(upper_bounds(i).index != currentBound.index) {
        // end of chr reached + maybe indexes inbetween that needs to be filled
        var current_region_string = if(currentBound.position != dictIndexToChromosomeAndSizeMap(currentBound.index)._2) dictIndexToChromosomeAndSizeMap(currentBound.index)._1 + ":" + currentBound.position + "-" + dictIndexToChromosomeAndSizeMap(currentBound.index)._2  else ""
        for(tmpidx <- currentBound.index + 1 until upper_bounds(i).index) { // fill between both indexes
          current_region_string += (if(current_region_string == "") "" else "," ) + dictIndexToChromosomeAndSizeMap(tmpidx)._1 + ":1-" + dictIndexToChromosomeAndSizeMap(tmpidx)._2
        }
        current_region_string += (if(current_region_string == "") "" else "," ) + dictIndexToChromosomeAndSizeMap(upper_bounds(i).index)._1 + ":1-" + upper_bounds(i).position
        map += ((PartitionSplit(i / splits, i % splits), current_region_string.split(",")))
        currentBound = upper_bounds(i)

      } else { // upper bound (end of region) is in same chromosome!
        map += ((PartitionSplit(i / splits, i % splits), Array(dictIndexToChromosomeAndSizeMap(upper_bounds(i).index)._1 + ":" + currentBound.position + "-" + upper_bounds(i).position)))
        currentBound = upper_bounds(i)
      }
      i += 1
    }
    // last region has no upper bound (is not in array, but is just until the end)
    var current_region_string = dictIndexToChromosomeAndSizeMap(currentBound.index)._1 + ":" + currentBound.position + "-" + dictIndexToChromosomeAndSizeMap(currentBound.index)._2
    for(tmpidx <- currentBound.index + 1 until dictIndexToChromosomeAndSizeMap.size) {
      current_region_string += "," + dictIndexToChromosomeAndSizeMap(tmpidx)._1 + ":1-" + dictIndexToChromosomeAndSizeMap(tmpidx)._2
    }
    map += ((PartitionSplit(i / splits, i % splits), current_region_string.split(",")))
    map
  }
  def saveBAMSplitsAsFile(sc: SparkContext, rdd: RDD[(Int, Array[Byte])], path: String): Long = {
    val conf_ = new Configuration(sc.hadoopConfiguration)
    val serdeserconf = new ConfigSerDeser(conf_)
    val actualpath = if(path.endsWith(("/"))) path else path + "/"

    rdd.mapPartitionsWithIndex((idx, it) => {
      var i = 0
      val outfs = FileSystem.get(new URI(actualpath), serdeserconf.get());
      while(it.hasNext) {
        val x = it.next()
        val filename = "%spart-%05d-%02d".format(actualpath, idx, x._1)
        val yourFile = new File(filename);
        yourFile.getParentFile.mkdirs()
        val bos = new BufferedOutputStream(new FileOutputStream(filename))
        bos.write(x._2)
        bos.close
        i += 1
      }
      it
    }).count // count makes it happen!
  }
}
