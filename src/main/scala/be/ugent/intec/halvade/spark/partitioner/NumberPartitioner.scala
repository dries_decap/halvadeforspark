package be.ugent.intec.halvade.spark.partitioner

import org.apache.spark.Partitioner
import be.ugent.intec.halvade.Logging
import be.ugent.intec.halvade.PartitionSplit

// splits is # splits per partition!
class NumberPartitioner(numParts: Int, splits: Int) extends Partitioner {
  override def getPartition(key: Any): Int = {
    key match {
       case i: Int => i / splits
       case _ => throw new ClassCastException
    }
  }

  override def numPartitions: Int = numParts
}
