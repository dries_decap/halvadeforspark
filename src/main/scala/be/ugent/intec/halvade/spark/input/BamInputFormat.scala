package be.ugent.intec.halvade.spark.input

import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat
import org.apache.hadoop.io.{NullWritable, BytesWritable}
import org.apache.hadoop.fs.Path;

class BamInputFormat extends SequenceFileInputFormat[NullWritable, BytesWritable] {

  override
  def isSplitable(context: org.apache.hadoop.mapreduce.JobContext, file: Path) : Boolean = {
    return false
  }

}
