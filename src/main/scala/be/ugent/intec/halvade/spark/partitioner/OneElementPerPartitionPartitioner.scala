package be.ugent.intec.halvade.spark.partitioner

import org.apache.spark.Partitioner

class OneElementPerPartitionPartitioner(parts: Int) extends Partitioner {
  override def getPartition(key: Any): Int = {
    key match {
       case i: Long => {
        if ( i < parts) i.toInt
        else throw new Exception("invalid part number: " + i)
       }
        case i: Int => {
         if ( i < parts) i
         else throw new Exception("invalid part number: " + i)
        }
       case _ => throw new ClassCastException("invalid type: " + key)
    }
  }
  override def numPartitions: Int = parts
}
