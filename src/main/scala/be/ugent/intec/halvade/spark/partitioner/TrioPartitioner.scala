package be.ugent.intec.halvade.spark.partitioner

import org.apache.spark.Partitioner
import be.ugent.intec.halvade.Logging
import scala.collection.Map // makes sure all maps are the same..

class TrioPartitioner(regionlist: Map[Int, List[(Int, (Int, Int))]]) extends Partitioner with Logging {
  override def getPartition(key: Any): Int = {
    key match {
      case (trioID: String, chr: Int, pos : Int) => {
        // should be exactly one element always!
        if(chr<0) {
          Math.abs(chr) - 1 // -> abs - 1 so it starts from 0 again
        } else {
          regionlist(chr).filter(x => pos >= x._2._1 && pos <= x._2._2)(0)._1
        }
      }
      case _ => {error("failed key is: " + key); throw new ClassCastException("failed key is: " + key)}
    }
  }

  override def numPartitions: Int = regionlist.flatMap(x => x._2.map(y => (y._1,1))).groupBy(_._1).size;
}
