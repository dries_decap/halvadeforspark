package be.ugent.intec.halvade.spark.rdd

import org.apache.spark.rdd.{RDD}
import org.apache.spark.rdd.RDD.rddToSequenceFileRDDFunctions
import org.apache.spark.SparkContext
import org.apache.hadoop.io.compress.DefaultCodec
import org.apache.hadoop.io.{NullWritable, BytesWritable, LongWritable}
import org.apache.hadoop.conf.Configuration
import java.io.{BufferedOutputStream, File, FileOutputStream}
import be.ugent.intec.halvade.Logging
import scala.language.implicitConversions

@SerialVersionUID(1002L)
class ByteArrayRDDFunctions(rdd: RDD[Array[Byte]]) extends Serializable with Logging {

  def binaryToString() : RDD[String] = {
    rdd.map(x => new String(x.map(_.toChar)) )
  }

  def binSize() : Array[Long] = {
    rdd.map(x => x.size.toLong).mapPartitions(it => List(it.reduce(_+_)).iterator).collect
  }

  def saveSplitsAsFile(path: String): Long = {
      rdd.mapPartitionsWithIndex((idx, it) => {
        var i = 0
        while(it.hasNext) {
          val x = it.next()
          val filename = "%spart-%05d-%02d".format(path, idx, i)
          val yourFile = new File(filename);
          yourFile.getParentFile.mkdirs()
          val bos = new BufferedOutputStream(new FileOutputStream(filename))
          val towrite = x
          bos.write(towrite)
          bos.close
          i += 1
        }
        it
      }).count // count makes it happen!
  }

  def saveAsBinaryFile(path: String): Long = {
      rdd.mapPartitionsWithIndex((idx, it) => {
        val filename = "%spart-%05d".format(path, idx)
        val yourFile = new File(filename);
        yourFile.getParentFile.mkdirs()
        val bos = new BufferedOutputStream(new FileOutputStream(filename))
        for(data <- it) {
          bos.write(data)
        }
        bos.close
        it
      }).count // count makes it happen!
  }

  // def saveBinary(path: String): Unit = {
  //   val codecOpt = Some(classOf[DefaultCodec])
  //   rdd.map(bytesArray => (NullWritable.get(), new BytesWritable(bytesArray)))
  //      .saveAsSequenceFile(path, codecOpt)
  // }
  def saveBinaryWithIndex(path: String): Unit = {
    val codecOpt = Some(classOf[DefaultCodec])
    rdd.zipWithIndex.map(x => (new LongWritable(x._2), new BytesWritable(x._1)))
       .saveAsSequenceFile(path, codecOpt)
  }
}

object ByteArrayRDDFunctions {
  implicit def addByteArrayRDDFunctions(rdd: RDD[Array[Byte]]) = new ByteArrayRDDFunctions(rdd)
}
