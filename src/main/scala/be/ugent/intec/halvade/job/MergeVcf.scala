package be.ugent.intec.halvade.job

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import be.ugent.intec.halvade.Utils._
import java.io.File
import org.apache.log4j.Logger
import org.apache.log4j.Level
import be.ugent.intec.halvade.FileUtils._
import org.apache.hadoop.fs.{FileSystem, Path}
import java.net.URI
import org.apache.hadoop.conf.Configuration
import org.apache.spark.storage.StorageLevel
import java.io._
import java.util.zip._
import scopt.OParser

object MergeVcf extends HalvadeJob {
  override def jobTitle = "Halvade Merge Vcf"
  import baseBuilder._
  override val pipelineParser = OParser.sequence(
    head(jobTitle, halvadeVersion),

    baseParser,

    opt[String]('i', "input")
      .action( (x, c) => c.copy(mergeInput = x) )
      .text("Location of the input files. Either a single file, a folder containing only vcf files.")
      .required(),

    opt[String]('h', "header")
      .action( (x, c) => c.copy(vcfHeader = x) )
      .text("Single vcf file containing the requested header."),

    checkConfig(
      c => // check all required options from general config
        if ( c.reference == null && c.refDict == null) failure("A dictionary needs to be given or a reference with corresponding dict file.")
        else success
      )

  )


  def setJobDefaults(config_ : Config): Config = {
    if(config_.persistLevel == null)
      config_.copy(persistLevel = StorageLevel.MEMORY_AND_DISK_SER)
    else
      config_
  }

  def runPipeline(config: Config): Unit = {

    startTime()
    
    val conf_ = new Configuration(sc.hadoopConfiguration)
    val fs: FileSystem = FileSystem.get(new URI(config.output), conf_);

    var tmp : String = null
    if (config.vcfHeader != "") {
      tmp = config.vcfHeader
    } else {
      tmp = config.mergeInput
      if(config.mergeInput.contains(",")) {
        tmp = config.mergeInput.split(",")(0)
      } else {
        val files = fs.listStatus(new Path(config.mergeInput)).filter(!_.isDirectory).map(_.getPath).filter(x=>x.getName.startsWith("part")).sortBy(x => x.toString())
        tmp = files(0).toString
      }
    }

    // DICT
    val dict = readDict(sc, (if(config.refDict == null) config.reference.replace(".fasta", ".dict") else config.refDict))

    val merged = mergeVcfFiles(sc, dict, config.mergeInput, tmp)
    merged.saveAsTextFile(config.output.replace(".vcf", ".sparkout"))

    saveMergedVcfFiles(sc, config.output.replace(".vcf", ".sparkout"), config.output + ".gz", config.overwriteOutput)

    info(measureTotalTime("VCF merge"))
  }

}
