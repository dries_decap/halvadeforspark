package be.ugent.intec.halvade.job

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import be.ugent.intec.halvade.tools.BioTools
import be.ugent.intec.halvade.spark.SamRecordFunctions._
import be.ugent.intec.halvade.Utils._
import be.ugent.intec.halvade.FileUtils._
import org.apache.spark.storage.StorageLevel
import java.io._
import java.util.zip._
import scala.io.Source
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf;
import be.ugent.intec.halvade.prep.Preprocessor
import org.apache.hadoop.fs.{FileSystem, Path}
import java.net.URI
import org.apache.hadoop.conf.Configuration
import be.ugent.intec.halvade.spark.partitioner.NumberPartitioner
import be.ugent.intec.halvade.Logging
import scopt.OParser


case class Config ( // all possible options for all pipelines should be in here!
    // SomaticPipeline options
    tumor: String = null,
    normal: String = null,
    somatic_variant_caller: String = "mutect2",
    tumorsample: String = "TUMORSAMPLE",
    normalsample: String = "NORMALSAMPLE",
    filterNonPrimaryChromosomes: Boolean = false,
    splitPerChromosome: Boolean = false,
    defaultAverageCoverage: Int = 35, // average coverage of normal sample for strelka2 run
    isExome: Boolean = false, // only relevant for strelka2

    // GermlinePipeline options
    germline: String = null,
    readPreppedSam: Boolean = false,
    callGvcf: Boolean = true,
    germlinesample: String = "GERMLINESAMPLE",

    // common GATK pipeline options
    saveBam: String = null,
    justAlign: Boolean = false,
    useElprep: Boolean = false,
    knownSites: String = null,
    reference: String = null,
    refDict: String = null,
    bindir: String = null,
    tempDir: String = "/tmp/",
    platform: String = "ILLUMINA",
    bqsrTable: String = null,
    overrideTasks: Int = -1,

    // Halvade Preprocess options
    prepManifest: String = null,
    prepFiles: Array[String] = Array.empty[String],
    readLength: Int = 100,

    // Merge VCF options
    mergeInput: String = null,
    vcfHeader: String = null,
    uniqueVariants: Boolean = true,

    // genotypeTrio options
    index: String = null,
    father: String = null,
    mother: String = null,
    hapmap: String = null,
    omni: String = null,
    thousandg: String = null,
    dbsnp: String = null,
    chromosomelist: String = null,
    sampleNames: String = null,

    // input to number of partitions ratios
    bam_partitions_to_size_ratio: Float = 3.5f, // was 3, need to check this...
    sam_partitions_to_size_ratio: Float = 1f,
    fastq_partitions_to_size_ratio: Float = 1.0f, // # partitions is same as # of input files (~60MB per file in preprocessor)
    alignment_size_to_partitions_ratio: Float = 3f,

    // general configuration
    output: String = null,
    partitions: Int = 1,
    overwriteOutput: Boolean = false,
    cleanup: Boolean = true,
    useKryo: Boolean = true,
    memoryMetrics: Boolean = true,
    logLevel: Level = Level.INFO,
    persistLevel: StorageLevel = null // StorageLevel.MEMORY_AND_DISK_SER
  )


abstract class HalvadeJob extends Logging {
  protected var sc: SparkContext = null
  protected var halvadeVersion : String = "2.0.5"
  protected var memory : Int = 0
  protected var overheadMem : Int = 0
  protected var tasks : Int = 1
  protected var gatktasks: Int = 1
  protected var memoryPerGATK : Int = 0
  protected def jobTitle : String
  protected var biotools: BioTools = null

  protected def runPipeline(config: Config) : Unit
  protected def setJobDefaults(config: Config) : Config

  def main(args: Array[String]) : Unit = {

    val config = parseOptions(args)

    config match {
      case Some(config_ : Config) => {
        val config = setJobDefaults(config_)
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)
        Logger.getLogger("be").setLevel(config.logLevel)
        // Logger.getLogger("org.apache.spark.ChromosomeRegionRangePartitioner").setLevel(config.logLevel)

        var conf: SparkConf = null;
        if(config.useKryo) {
          conf = new SparkConf()
              .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
              .set("spark.kryo.registrator","be.ugent.intec.halvade.spark.BioKryoRegistrator")
              .set("spark.kryoserializer.buffer","64M")
              .set("spark.kryo.registrationRequired", "false")
              .set("spark.kryo.unsafe", "true")
        } else {
          conf = new SparkConf();
        }
        if(config.memoryMetrics) {
          conf =  conf.set("spark.executor.processTreeMetrics.enabled", "true")
        }
        val spark = SparkSession.builder
                                .appName(jobTitle)
                                .config(conf)
                                .getOrCreate()
        sc = spark.sparkContext

        debug("serializer [" + sc.getConf.get("spark.serializer", "org.apache.spark.serializer.JavaSerializer") + "]")
        debug("persistLevel [" + config.persistLevel.description+ "]")

        getExecutorStats(config)
        // run actual pipeline
        runPipeline(config)

        sc.stop()
        spark.stop()
      }
      case None => throw new ClassCastException
    }
  }

  def unpersistRDDByName(RDDName: String) {
    val trdds = sc.getPersistentRDDs.filter(_._2.name == RDDName)
    if(trdds.size == 1) trdds.head._2.unpersist()
  }
  def isRDDPersisted(RDDName: String) : Boolean = {
    return sc.getPersistentRDDs.filter(_._2.name == RDDName).size > 0
  }

  def getExecutorStats(config: Config) = {
    val sparkConf = sc.getConf;
    tasks = sparkConf.get("spark.task.cpus", "1").toInt
    if(config.overrideTasks > 0)
      if(config.overrideTasks > tasks) gatktasks = tasks // don't use more tasks than cpu cores available
      else gatktasks = config.overrideTasks
    else gatktasks = (tasks + 1)/ 2 // 1 -> (1+1)/2 = 1 // 2 -> (2+1)/2 = 1 // 3 -> (3+1)/2=2 // 4 -> (4+1)/2=2
    memory = parseMemory(sparkConf.get("spark.executor.memory"))
    overheadMem = parseMemory(sparkConf.get("spark.executor.memoryOverhead", Math.max(384, memory * 0.1).toInt.toString + "M"))
    info("Executors [" + memory + "MB + " + overheadMem + "MB OH / " + tasks + " CPUS - " + gatktasks + " TASKS]")
    memoryPerGATK =  Math.min(1024, ((overheadMem - memory * 0.1  - overheadMem * 0.1) / gatktasks).toInt)
    // memoryPerGATK =  Math.max(2048, Math.min(1024, ((overheadMem - memory * 0.1  - overheadMem * 0.1) / gatktasks).toInt))
     // 10% of memory is typically used as overhead so subtract this (both for executor and gatk) and the rest is for tools/gatk/bwa etc and at least 1gb!
     // at least 1gb, but more than 2gb is typically not needed
  }



  protected val baseBuilder = OParser.builder[Config]
  protected val pipelineParser : OParser[Unit, Config] = null
  def parseOptions(args: Array[String]) : Option[Config] = {
    OParser.parse(pipelineParser, args, Config())
  }

  import baseBuilder._
  protected var baseParser = OParser.sequence(
    programName("Halvade"),

    opt[String]('o', "output")
      .action( (x, c) => c.copy(output = x) )
      .text("The output VCF file.")
      .required(),

    opt[String]("tmp")
      .action( (x, c) => {
        if(x.endsWith("/"))
          c.copy(tempDir = x)
        else
          c.copy(tempDir = x + "/")
      } )
      .text("The directory to store temporary files."),

    opt[Int]("partitions")
      .action( (x, c) => c.copy(partitions = x) )
      .text("The number of partitions in RDD's."),

    opt[String]('r', "reference")
      .action( (x, c) => c.copy(reference = x) )
      .text("Location of the reference Fasta file."),

    opt[String]("ref_dict")
      .action( (x, c) => c.copy(refDict = x) )
      .text("Location of the reference Dict file. Default is the reference name but fastq replaced by dict."),

    opt[String]('b', "bindir")
      .action( (x, c) => c.copy(bindir = x) )
      .text("Location of the directory containing all binaries."),

    opt[Unit]("overwrite")
      .action( (_, c) => c.copy(overwriteOutput = true) )
      .text("Removes the output folder if it exists."),

    opt[Unit]("keep_files")
      .action( (_, c) => c.copy(cleanup = false) )
      .text("keep intermediate files for debugging purposes."),

    opt[Unit]("java_serializer")
      .action( (_, c) => c.copy(useKryo = false) )
      .text("Use java serializer instead of kryo serializer."),

    opt[String]("log")
      .action( (x, c) =>
        x match {
          case "ERROR" => c.copy(logLevel = Level.ERROR)
          case "WARN" => c.copy(logLevel = Level.WARN)
          case "INFO" => c.copy(logLevel = Level.INFO)
          case "DEBUG" => c.copy(logLevel = Level.DEBUG)
          case _ => {info("invalid log level '" + x + "' given, using ERROR!"); c.copy(logLevel = Level.ERROR)}
        })
       .text("Sets the loglevel to one of these possible values: INFO, DEBUG, WARN, ERROR. [ERROR]"),

     opt[String]("persist")
       .action( (x, c) =>
         x match {
           case "mem_only" => c.copy(persistLevel = StorageLevel.MEMORY_ONLY)
           case "mem_only_ser" => c.copy(persistLevel = StorageLevel.MEMORY_ONLY_SER)
           case "mem_disk" => c.copy(persistLevel = StorageLevel.MEMORY_AND_DISK)
           case "mem_disk_ser" => c.copy(persistLevel = StorageLevel.MEMORY_AND_DISK_SER)
           case "disk_only" => c.copy(persistLevel = StorageLevel.DISK_ONLY)
           case _ => {info("invalid persist level '" + x + "' given, using mem_disk_ser!"); c.copy(persistLevel = StorageLevel.MEMORY_AND_DISK_SER)}
         })
        .text("Sets the persist level for RDD's: mem_only, mem_only_ser, mem_disk, mem_disk_ser, disk_only. Default depends on the pipeline."),

     help("help").text("prints this usage text")
  )

}
