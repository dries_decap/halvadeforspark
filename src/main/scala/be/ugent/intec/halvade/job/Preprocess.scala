package be.ugent.intec.halvade.job

import java.net.URI
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import be.ugent.intec.halvade.Utils._
import be.ugent.intec.halvade.FileUtils._
import org.apache.spark.storage.StorageLevel
import java.io.File
import org.apache.spark.rdd.RDD
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf;
import be.ugent.intec.halvade.prep.Preprocessor
import scopt.OParser

object Preprocess extends HalvadeJob {
  final val averageBytesPerLine = 68 // based on reads of 100bp
  final val readblockCapFactor = 25;
  override def jobTitle = "Halvade preprocess FASTQ files"
  import baseBuilder._
  override val pipelineParser = OParser.sequence(
    head(jobTitle, halvadeVersion),

    baseParser,

    opt[String]("manifest")
      .action( (x, c) => c.copy(prepManifest = x) )
      .text("Manifest file with raw input files, these will be processed and stored in the input folder. WARNING: the input folder will be overwitten!"),
      // .required(),

    arg[String]("<file1_1.fq,file1_2.fq(,rg)>...")
        .unbounded()
        .optional()
        .action((x, c) => c.copy(prepFiles = c.prepFiles :+ x))
        .text("List of paired-end FASTQ files"),

      checkConfig(
        c => // check all required options from general config
          if ( c.prepManifest == null && c.prepFiles.size == 0) failure("Either manifest or a list of paired-end FASTQ files needs to be given.")
          else success
      )
  )

  def setJobDefaults(config_ : Config): Config = {
    if(config_.persistLevel == null)
      config_.copy(persistLevel = StorageLevel.MEMORY_AND_DISK_SER)
    else
      config_
  }

  final val advisdedMemorySize = 5120 // MB
  def runSanityChecks(config: Config, sc: SparkContext) : Boolean = {
    // SANITY CHECKS
    val conf = new Configuration(sc.hadoopConfiguration)
    val fs: FileSystem = FileSystem.get(new URI(config.output), conf);
    val outpath: Path = new Path(config.output + "/");
    if (fs.exists(outpath) && !fs.getFileStatus(outpath).isDirectory()) {
      error("please provide a correct output directory");
      return false;
    }

    if(memory + overheadMem < advisdedMemorySize) {
      warn("Executor memory is set to " + memory  + " MB + " + overheadMem + " overhead MB. To increase performance of the preprocessing step, set this to at least " + advisdedMemorySize + " MB.");
    }
    return true;
  }

  def runPipeline(config: Config): Unit = {
    if(!runSanityChecks(config, sc)) return;

    startTime()

    if (config.overwriteOutput) assert(deleteRecursively(new File(config.output)))
    else throw new Exception("enable overwrite, this is to ensure you know what you are doing and that the output folder will be deleted if it exists already!")
    //preprocess
    Preprocessor.preprocessUnaligedReads(config.prepManifest, config.prepFiles, config.output, tasks, sc)

    info(measureTime("Preprocessing"))
  }
}
