package be.ugent.intec.halvade.job

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import be.ugent.intec.halvade.spark.SamRecordFunctions._
import be.ugent.intec.halvade.tools.BioTools
import be.ugent.intec.halvade.Utils._
import be.ugent.intec.halvade.FileUtils._
import org.apache.spark.storage.StorageLevel
import java.io._
import scala.io.Source
import java.util.zip._
import org.apache.spark.rdd.RDD
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf;
import scala.collection.Map
import be.ugent.intec.halvade.prep.Preprocessor
import org.apache.hadoop.fs.{FileSystem, Path}
import java.net.URI
import org.apache.hadoop.conf.Configuration
import be.ugent.intec.halvade.spark.partitioner.NumberPartitioner
import scopt.OParser
import scala.collection.mutable.ArrayBuffer
import be.ugent.intec.halvade.{PartitionSplit, DictIndexAndPosition, SplitIdAndRecordWithPosition}

object GermlinePipeline extends HalvadeJob {
  override def jobTitle = "Halvade Germline pipeline"
  import baseBuilder._
  override val pipelineParser = OParser.sequence(
    head(jobTitle, halvadeVersion),

    baseParser,

    opt[String]('i', "germline")
      .action( (x, c) => c.copy(germline = x) )
      .text("Location of the input files.")
      .required(),

    opt[String]('s', "knownsites")
      .action( (x, c) => c.copy(knownSites = x) )
      .text("Location of the VCF file containing known sites. Skips the BQSR step if not provided"),

    opt[Unit]("just_align")
      .action( (_, c) => c.copy(justAlign = true) )
      .text("Only runs the alignment and sort steps and then writes sam files to the output directory (without extension, see part-xxxxx files)."),

    opt[Unit]("use_elprep")
      .action( (_, c) => c.copy(useElprep = true) )
      .text("Use elprep instead of gatk4/Picard for marking duplicates [and BQSR]."),

    opt[String]("samplename")
      .action( (x, c) => c.copy(germlinesample = x) )
      .text("The samplename to be stored in the read groups."),

    opt[Unit]("no_gvcf")
      .action( (_, c) => c.copy(callGvcf = false) )
      .text("Don't call GVCF, but only the regular variants."),

    opt[Unit]("prepped_sam")
      .action( (_, c) => c.copy(readPreppedSam = true) )
      .text("Use preprocessed SAM as input (from just_align step)."),

    opt[Unit]("filter_non_primary_chr")
      .action( (_, c) => c.copy(filterNonPrimaryChromosomes = true) )
      .text("Filter out non primary chromosomes, uses (chr)?1-22|X|Y|MT? as valid chromosomes(case insensitive)."),

    checkConfig(
      c => // check all required options from general config
        if ( c.reference == null) failure("Reference needs to be set.")
        else if (c.bindir == null) failure("Reference needs to be set.")
        else success
    )
  )

  def setJobDefaults(config_ : Config): Config = {
    val tmp = if(config_.persistLevel == null)
      config_.copy(persistLevel = StorageLevel.DISK_ONLY)
    else
      config_
    tmp
  }

  protected val VariantsRDDName : String = "Variants"
  protected val DuplicateMarkedRecordsRDDName : String = "DuplicateMarkedRecords"
  protected val AlignedRecordsRDDName : String = "AlignedRecords"
  protected val SamToBamRDD : String = "SamToBam"

  protected val vcfRegex = "\\.vcf(\\.gz)?$".r
  protected val vcfExtension = ".vcf"
  protected val vcfGzExtension = ".vcf.gz"
  protected val outputFileName = "halvade-output"
  protected val vcfSplitExtension = ".vcf.split"
  protected val regionsExtension = ".regions"
  protected def getValidOutputVcf(output: String) : String = if(vcfRegex.findFirstMatchIn(output) != None) output else if (output.endsWith("/")) output + outputFileName + vcfGzExtension else output + vcfGzExtension
  protected def getSplitVcfDirectory(output: String) : String = vcfRegex.replaceAllIn(output, vcfSplitExtension)
  protected def getRegionsFile(output: String) : String = vcfRegex.replaceAllIn(output, regionsExtension)

  def runPipeline(config: Config): Unit = {
    runSanityChecks(config)
    biotools = new BioTools(config.bindir, config.reference, config.knownSites, gatktasks, tasks, memoryPerGATK, config.logLevel, tmp = config.tempDir, cleanupTmp = config.cleanup)

    // DICT
    val suffix = config.reference.split("\\.").last
    val dict = readDict(sc, (if(config.refDict == null) config.reference.replace(suffix, "dict") else config.refDict))

    // check if output file ends with vcf else add it
    val output = getValidOutputVcf(config.output)

    startTime()
    // READ SAMPLES/ ALIGN IF NEEDED & SORT OUT HEADERS
    var readgrouplines : Array[String] = Array.empty
    val (givenReadgroups, duplicatesHaveBeenMarked, records) = readOrAlignRecords(config, dict, config.partitions, config.germline, config.germlinesample)
    val tmp = givenReadgroups.map(x => x.split("\t").map(x => x.split(":")).filter(_(0) == "SM")(0)(1)).distinct
    if(tmp.size != 1) throw new Exception("Can only be a single sample per input file. Aborting.")
    val sample_name: String = tmp(0)
    if(!config.germline.endsWith("bam")) records.persist(StorageLevel.DISK_ONLY).setName(AlignedRecordsRDDName)
    var partitions = if(records.partitions.size < config.partitions) records.partitions.size else config.partitions

    // partition
    val (partitioner, records_with_overlap) = records.determineRegions(dict, partitions, gatktasks, config.readLength, false) // strelka2 not yet implemented here
    partitions = (partitioner.numPartitions.toDouble / gatktasks).ceil.toInt // needed if split by chromosome is enabled, this will change # of partitions
    val gatk_regions = collectRegionsForPartitionSplits(dict, partitioner, gatktasks)
    saveGatkRegions(sc, gatk_regions, getRegionsFile(output), true)
    info(measureTime("Job 1"))

    // PARTITION & SORT
    val header = createSamHeader(dict, givenReadgroups)
    val partitioned_records = partitionRecords(records_with_overlap, partitions, gatktasks)
    if(duplicatesHaveBeenMarked) info("duplicates have been marked, skipping Mark duplicates")

    if(config.justAlign) {
      saveAlignedRecords(config, partitioned_records)
    } else {
      // PREPROCESS (MD/BQSR) AND RUN VARIANT CALLING
      if (config.useElprep) {
        pipelineWithElprep(config, output, partitioned_records, gatk_regions, dict, header, duplicatesHaveBeenMarked)
      } else {
        pipelineWithGATK(config, output, partitioned_records, gatk_regions, dict, header, duplicatesHaveBeenMarked, sample_name)
      }
    }
    info(measureTotalTime("Germline variant calling pipeline"))
  }

  def saveAlignedRecords(config: Config, partitioned_records: RDD[SplitIdAndRecordWithPosition]) {

    if (config.overwriteOutput) assert(deleteRecursively(new File(config.output))) // remove output
    partitioned_records.mapPartitionsWithIndex((idx, it) => { // save the sam files per partitionsplit
      val filename = "%spart-%05d".format(config.output, idx)
      val yourFile = new File(filename);
      yourFile.getParentFile.mkdirs()
      val out = new BufferedOutputStream(new FileOutputStream(filename))
      while(it.hasNext) {
        val data = it.next()
        out.write((data.record + '\n').getBytes)
      }
      out.close
      it
    }).count // count makes it happen!
    unpersistAlignedRecords()
    info(measureTime("aligned records"))

  }


  protected def pipelineWithGATK(config: Config, output: String, partitioned_records: RDD[SplitIdAndRecordWithPosition],
    gatk_regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], header: Array[String], duplicatesHaveBeenMarked : Boolean, sample_name: String) {
    //  MARK DUPLICATES
    var recordsWithDuplicatesMarked: RDD[(Int, Array[Byte])] =
        if(duplicatesHaveBeenMarked)
          biotools.sortAndConvertToBam(partitioned_records, header)
        else
          biotools.sortAndMarkDuplicates(partitioned_records, header)

    //  & BQSR IF NEEDED
    if(config.knownSites == null) {
      // CALL VARIANTS
      val variants = biotools.runHaplotypeCaller(recordsWithDuplicatesMarked, gatk_regions, dict, gvcf = config.callGvcf, sample_name)
      variants.persist(config.persistLevel).setName(VariantsRDDName)
      val vcfheader =  createHaplotypeCallerHeader(dict, config.germlinesample)
      saveVariants(config, dict, output, vcfheader, variants)
      unpersistAlignedRecords()
      variants.unpersist()

    } else { // THIS IS THE DEFAULT
      recordsWithDuplicatesMarked.persist(config.persistLevel).setName(DuplicateMarkedRecordsRDDName)
      val variants = biotools.runBqsrAndHaplotypeCaller(recordsWithDuplicatesMarked, gatk_regions, dict, gvcf = config.callGvcf, sample_name)
      unpersistAlignedRecords()
      info(measureTime("Job 2"))
      variants.persist(config.persistLevel).setName(VariantsRDDName)
      val vcfheader = createHaplotypeCallerHeader(dict, config.germlinesample)
      saveVariants(config, dict, output, vcfheader, variants)
      recordsWithDuplicatesMarked.unpersist()
      variants.unpersist()
      info(measureTime("Job 3"))
    }

  }

  @deprecated("Untested. ", "1.6")
  protected def pipelineWithElprep(config: Config, output: String, partitioned_records: RDD[SplitIdAndRecordWithPosition],
    gatk_regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], header: Array[String], duplicatesHaveBeenMarked : Boolean) {
    //  MARK DUPLICATES & BQSR
    var merged_with_rg: RDD[(Int, Array[Byte])] = biotools.sortAndConvertToBam(partitioned_records, header)
    val final_merged: RDD[(Int, Array[Byte])] = biotools.elprep(merged_with_rg, !duplicatesHaveBeenMarked, config.knownSites, config.reference.replace("fasta","elfasta"))
    // CALL VARIANTS
    val variants = biotools.runHaplotypeCaller(final_merged, gatk_regions, dict, gvcf = config.callGvcf, config.germlinesample)

    info(measureTime("Job 2"))
    variants.persist(config.persistLevel).setName(VariantsRDDName)
    val vcfheader = createHaplotypeCallerHeader(dict, config.germlinesample)
    saveVariants(config, dict, output, vcfheader, variants)
    unpersistAlignedRecords()
    variants.unpersist()
    info(measureTime("Job 3"))
  }

  protected def readOrAlignRecords(config: Config, dict: Map[String, DictIndexAndPosition], partitions: Int, input: String, sample: String) :(Array[String], Boolean, RDD[String]) = {
    if(input.endsWith("bam")) {
      val (header, records) = Preprocessor.preprocessAlignedBam(input, dict, sc)
      val duplicatesHaveBeenMarked : Boolean = header.map(_.toLowerCase).filter(fi => fi matches ".*mark.*duplicate.*").length > 0
      val rglines = header.filter(x => x.startsWith("@RG")).distinct

      if(rglines.length > 0) (rglines, duplicatesHaveBeenMarked, records)
      else (createReadGroupLines(Array(sample), sample, config.platform), duplicatesHaveBeenMarked, records.addReadgroup(sample))
    } else if (config.readPreppedSam) {
      val (header, records) = Preprocessor.preprocessAlignedSam(input, dict, sc)
      val duplicatesHaveBeenMarked : Boolean = header.map(_.toLowerCase).filter(fi => fi matches ".*mark.*duplicate.*").length > 0
      val rglines = header.filter(x => x.startsWith("@RG")).distinct

      if(rglines.length > 0) (rglines, duplicatesHaveBeenMarked, records)
      else (createReadGroupLines(Array(sample), sample, config.platform), duplicatesHaveBeenMarked, records.addReadgroup(sample))
    } else {
      biotools.bwamem(input, sample, config.platform, sc)
    }
  }

  protected def unpersistAlignedRecords() {
    unpersistRDDByName(AlignedRecordsRDDName);
  }
  def runSanityChecks(config: Config) {
    // SANITY CHECKS
    if ( config.useElprep && config.knownSites != null && !config.knownSites.endsWith("vcf")) {
      throw new Exception("Elprep known sites should end with instead 'elsites' of 'vcf'. Aborting.")
    }
    if(config.germline.endsWith("bam") && config.justAlign) {
      throw new Exception("cannot read aligned bam and just perform alignment!");
    }
    // MEMORY REQUIREMENTS for BWA and GATK
    if ( !config.germline.endsWith("bam") && overheadMem < 8*1024) {
      warn("WARNING: memory overhead [" + overheadMem + " MB] is probably too low for read mapping with BWA [human genome].")
    }
    if (memoryPerGATK < 1024) {
      warn("WARNING: memory per GATK instance [" + memoryPerGATK + " MB x " + gatktasks + " instances] is less than 1 GB and might cause slow results or go out of memory.")
    }
    // BEST TO USE DISK PERSIST FOR SOMATIC PIPELINE
    if(config.persistLevel != StorageLevel.DISK_ONLY) {
      warn("WARNING: Highly recommended to use 'DISK ONLY' persistence for the Germline Pipeline instead of [" + config.persistLevel.description + "]")
    }
  }

  /**
  * Call this after you persisted the RDD!
  */
  protected def saveVariants(config: Config, dict: Map[String, DictIndexAndPosition], output: String, vcfheader: Array[String], variants: RDD[String]) {
    val merged = deduplicateVariantsAndAddHeader(sc, dict, variants, vcfheader)
    val splitvcfoutput = getSplitVcfDirectory(output)
    if (config.overwriteOutput) assert(deleteRecursively(new File(output)))
    if (config.overwriteOutput) assert(deleteRecursively(new File(splitvcfoutput)))
    merged.saveAsTextFile(splitvcfoutput)

    saveMergedVcfFiles(sc, splitvcfoutput, output, true)
  }
}
