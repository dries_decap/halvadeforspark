package be.ugent.intec.halvade.job

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import be.ugent.intec.halvade.spark.SamRecordFunctions._
import be.ugent.intec.halvade.Utils._
import be.ugent.intec.halvade.FileUtils._
import be.ugent.intec.halvade.tools.BioTools
import org.apache.spark.storage.StorageLevel
import java.io._
import java.util.zip._
import scala.io.Source
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf;
import scala.collection.Map
import be.ugent.intec.halvade.prep.Preprocessor
import org.apache.hadoop.fs.{FileSystem, Path}
import java.net.URI
import org.apache.hadoop.conf.Configuration
import be.ugent.intec.halvade.spark.partitioner.NumberPartitioner
import scopt.OParser
import scala.collection.mutable.ArrayBuffer
import be.ugent.intec.halvade.{PartitionSplit, DictIndexAndPosition, SplitIdAndRecordWithPosition}

object SomaticPipeline extends HalvadeJob {
  override def jobTitle = "Halvade Somatic pipeline"
  import baseBuilder._
  override val pipelineParser = OParser.sequence(
    head(jobTitle, halvadeVersion),

    baseParser,

    opt[String]("normal")
    .action( (x, c) =>
      c.copy(normal = x) )
      .text("Location of the normal input files.")
      .required(),

    opt[String]("tumor")
      .action( (x, c) => c.copy(tumor = x) )
      .text("Location of the tumor input files.")
      .required(),

    opt[String]("save_bam")
      .action( (x, c) => c.copy(saveBam = x) )
      .text("Save the final BAM before variant calling."),

    opt[String]('s', "knownsites")
      .action( (x, c) => c.copy(knownSites = x) )
      .text("Location of the VCF file containing known sites. Skips the BQSR step if not provided"),

    opt[Unit]("use_elprep")
      .hidden // not tested recently
      .action( (_, c) => c.copy(useElprep = true) )
      .text("Use elprep instead of gatk4/Picard for marking duplicates [and BQSR]."),


    opt[String]("tumorsm")
      .action( (x, c) => c.copy(tumorsample = x) )
      .text("The tumor sample name to be stored in the read groups."),

    opt[String]("normalsm")
      .action( (x, c) => c.copy(normalsample = x) )
      .text("The normal sample name to be stored in the read groups."),

    opt[String]("bqsrtable")
      .hidden
      .action( (x, c) => c.copy(bqsrTable = x) )
      .text("Provides a custom BQSR table for BQSR."),

    opt[Unit]("exome")
      .action( (_, c) => c.copy(isExome = true) )
      .text("Use exome option in Strelka2, only relevant for Strelka2."),

    opt[String]("variant_caller")
      .hidden
      .action((x, c) =>
        x match {
          case "mutect2" => c.copy(somatic_variant_caller = x)
          case "strelka2" => c.copy(somatic_variant_caller = x, filterNonPrimaryChromosomes = true) // splitPerChromosome = true not needed anymore if filterNonPrimaryChromosomes
          case "both" => c.copy(somatic_variant_caller = x, filterNonPrimaryChromosomes = true) // splitPerChromosome = true not needed anymore if filterNonPrimaryChromosomes
          case _ => {info("invalid variant_caller '" + x + "' given, using mutect2!"); c.copy(somatic_variant_caller = "mutect2")}
        })
      .text("Set the variant caller: 'mutect2' [default], 'strelka2' or 'both' (creates 2 vcf files). With 'strelka2' or 'both' options, non-primary chromosomes are filtered out."),

    opt[Int]("override_tasks")
      .hidden()
      .action( (x, c) => c.copy(overrideTasks = x) )
      .text("Overrides the automatic number of tasks per executor."),

    checkConfig(
      c => // check all required options from general config
        if ( c.reference == null) failure("Reference needs to be set.")
        else if (c.bindir == null) failure("Reference needs to be set.")
        else success
    )

  )

  def setJobDefaults(config_ : Config): Config = {
    val tmp = if(config_.persistLevel == null)
      config_.copy(persistLevel = StorageLevel.DISK_ONLY)
    else
      config_
    tmp
  }

  protected val VariantsRDDName : String = "Variants"
  protected val DuplicateMarkedRecordsRDDName : String = "DuplicateMarkedRecords"
  protected val BqsrDoneRecordsRDDName : String = "BqsrDoneRecords"
  protected val AlignedTumorRDDName : String = "AlignedTumorRecords"
  protected val AlignedNormalRDDName : String = "AlignedNormalRecords"
  protected val AlignedMergedRDDName : String = "AlignedMergedRecords"


  protected val vcfRegex = "\\.vcf(\\.gz)?$".r
  protected val vcfExtension = ".vcf"
  protected val vcfGzExtension = ".vcf.gz"
  protected val vcfSplitExtension = ".vcf.split"
  protected val outputFileName = "halvade-output"
  protected val regionsExtension = ".regions"
  protected val strelka2VcfGzExtension = "-strelka2.vcf.gz"
  protected val mutect2VcfGzExtension = "-mutect2.vcf.gz"
  protected def getValidOutputVcf(output: String) : String = if(vcfRegex.findFirstMatchIn(output) != None) output else if (output.endsWith("/")) output + outputFileName + vcfGzExtension else output + vcfGzExtension
  protected def getSplitVcfDirectory(output: String) : String = vcfRegex.replaceAllIn(output, vcfSplitExtension)
  protected def getMutect2File(output: String) : String = vcfRegex.replaceAllIn(output, mutect2VcfGzExtension)
  protected def getStrelka2File(output: String) : String = vcfRegex.replaceAllIn(output, strelka2VcfGzExtension)
  protected def getRegionsFile(output: String) : String = vcfRegex.replaceAllIn(output, regionsExtension)

  def runPipeline(config: Config): Unit = {
    runSanityChecks(config)
    biotools = new BioTools(config.bindir, config.reference, config.knownSites, gatktasks, tasks, memoryPerGATK, config.logLevel, tmp = config.tempDir, cleanupTmp = config.cleanup)

    // DICT
    val suffix = config.reference.split("\\.").last
    val dict = readDict(sc, (if(config.refDict == null) config.reference.replace(suffix, "dict") else config.refDict))
    // check if output file ends with vcf else add it
    val output = getValidOutputVcf(config.output)

    startTime()
    // READ SAMPLES/ ALIGN IF NEEDED & SORT OUT HEADERS
    // TUMOR
    var readgrouplines : Array[String] = Array.empty

    val (givenReadgroupsT, duplicatesHaveBeenMarkedT, tumorRecords) = readOrAlignRecords(config, dict, config.partitions, config.tumor, config.tumorsample)
    val tmpT = givenReadgroupsT.map(x => x.split("\t").map(x => x.split(":")).filter(_(0) == "SM")(0)(1)).distinct
    if(tmpT.size != 1) throw new Exception("Can only be a single sample for tumor. Aborting.")
    val tumor_sm: String = tmpT(0)
    readgrouplines = givenReadgroupsT
    if(!config.tumor.endsWith("bam")) tumorRecords.persist(StorageLevel.DISK_ONLY).setName(AlignedTumorRDDName)
    var partitions = if(tumorRecords.partitions.size < config.partitions) tumorRecords.partitions.size else config.partitions
    val (partitioner, tumor_with_overlap) = tumorRecords.determineRegions(dict, partitions, gatktasks, config.readLength, config.splitPerChromosome)
    partitions = (partitioner.numPartitions.toDouble / gatktasks).ceil.toInt // needed if split by chromosome is enabled, this will change # of partitions
    val gatk_regions = collectRegionsForPartitionSplits(dict, partitioner, gatktasks)
    saveGatkRegions(sc, gatk_regions, getRegionsFile(output), true)

    // NORMAL
    val (givenReadgroupsN, duplicatesHaveBeenMarkedN, normalRecords) = readOrAlignRecords(config, dict, partitions, config.normal, config.normalsample)
    val tmpN = givenReadgroupsN.map(x => x.split("\t").map(x => x.split(":")).filter(_(0) == "SM")(0)(1)).distinct
    val normalRGIDs : String = givenReadgroupsN.map(x => x.split("\t").map(x => x.split(":")).filter(_(0) == "ID")(0)(1)).distinct.mkString("\n")
    if(tmpN.size != 1) throw new Exception("Can only be a single sample for normal. Aborting.")
    val normal_sm: String = tmpN(0)
    var chromDepths : Map[String, Long] = null;
    readgrouplines = readgrouplines ++ givenReadgroupsN
    val normal_with_overlap = normalRecords.createSplitsWithPartitioner(dict, partitioner, gatktasks, config.readLength)

      // MERGE
    info("Tumor sample: " + tumor_sm)
    info("Normal sample: " + normal_sm)
    info(measureTime("Job 1"))
    val merged_with_overlap = partitionRecords(tumor_with_overlap.union(normal_with_overlap), partitions, gatktasks)

    // info("part sizes: " + merged_with_overlap.mapPartitionsWithIndex((idx,it) => List((idx, it.size)).iterator).collect.mkString("\n"))

    val duplicatesHaveBeenMarked = duplicatesHaveBeenMarkedT && duplicatesHaveBeenMarkedN // both need to have duplicates marked! but should be the case if from same origin
    if(duplicatesHaveBeenMarked) info("duplicates have been marked, skipping")
    // PREPROCESS (MD/BQSR) AND RUN VARIANT CALLING
    val header = createSamHeader(dict, readgrouplines)
    if (config.useElprep) {
      pipelineWithElprep(config, output, merged_with_overlap, gatk_regions, dict, header, tumor_sm, normal_sm, duplicatesHaveBeenMarked, normalRGIDs)
    } else {
      pipelineWithGATK(config, output, merged_with_overlap, gatk_regions, dict, header, tumor_sm, normal_sm, duplicatesHaveBeenMarked, normalRGIDs)
    }
    info(measureTotalTime("Somatic variant calling pipeline"))
  }

  def runSanityChecks(config: Config) {
    // SANITY CHECKS
    // ELPREP with BQSR should have an ELSITES suffix for the knownsites file
    if ( config.useElprep && config.knownSites != null && !config.knownSites.endsWith("vcf")) {
      throw new Exception("Elprep known sites should end with instead 'elsites' of 'vcf'. Aborting.")
    }
    // MEMORY REQUIREMENTS for BWA and GATK
    if ( !(config.tumor.endsWith("bam") && config.normal.endsWith("bam")) && overheadMem < 8*1024) {
      // if BWA is needed, then at least 8GB is needed for the HUMAN GENOME
      warn("WARNING: memory overhead [" + overheadMem + " MB] is probably too low for read mapping with BWA [human genome].")
    }
    if (memoryPerGATK < 1024) {
      warn("WARNING: memory per GATK instance [" + memoryPerGATK + " MB x " + gatktasks + " instances] is less than 1 GB and might cause slow results or go out of memory.")
    }
    // ONLY SUPPORTS BOTH BAM OR BOTH FASTQ ATM
    // if ((config.tumor.endsWith("bam") != config.normal.endsWith("bam"))) {
      // throw new Exception("Currently Halvade only supports either both BAM or both FASTQ inputs. Aborting.")
    // }
    // BEST TO USE DISK PERSIST FOR SOMATIC PIPELINE
    if(config.persistLevel != StorageLevel.DISK_ONLY) {
      warn("WARNING: Highly recommended to use 'DISK ONLY' persistence for the Somatic Pipeline instead of [" + config.persistLevel.description + "]. It needs ~800 (or more) Gygabytes of memory for the WGS. You have been warned.")
    }
  }

  protected def readOrAlignRecords(config: Config, dict: Map[String, DictIndexAndPosition], partitions: Int, input: String, sample: String) :(Array[String], Boolean, RDD[String]) = {
    if(input.endsWith("bam")) {
      val (header, records) = Preprocessor.preprocessAlignedBam(input, dict, sc)
      val duplicatesHaveBeenMarked : Boolean = header.map(_.toLowerCase).filter(fi => fi matches ".*mark.*duplicate.*").length > 0
      (header.filter(x => x.startsWith("@RG")).distinct, duplicatesHaveBeenMarked, records)
    // } else if (config.readPreppedSam) {
      // val (header, records) = Preprocessor.preprocessAlignedSam(input, dict, sc)
      // val duplicatesHaveBeenMarked : Boolean = header.map(_.toLowerCase).filter(fi => fi matches ".*mark.*duplicate.*").length > 0
      // (header.filter(x => x.startsWith("@RG")).distinct, duplicatesHaveBeenMarked, records)
    } else {
      biotools.bwamem(input, sample, config.platform, sc)
    }
  }

  protected def unpersistAlignedRecordRDDs() {
    unpersistRDDByName(AlignedTumorRDDName);
    unpersistRDDByName(AlignedNormalRDDName);
    unpersistRDDByName(AlignedMergedRDDName);
  }

  protected def pipelineWithGATK(config: Config, output: String, merged_with_overlap: RDD[SplitIdAndRecordWithPosition],
    gatk_regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], header: Array[String], tumor_sm: String, normal_sm: String, duplicatesHaveBeenMarked : Boolean, normalRGIDs : String) {
    //  MARK DUPLICATES
    var final_merged: RDD[(Int, Array[Byte])] =
        if(duplicatesHaveBeenMarked) biotools.sortAndConvertToBam(merged_with_overlap, header)
        else biotools.sortAndMarkDuplicates(merged_with_overlap, header)

    var chromDepths : Map[String, Long] = null;
    if(!config.isExome && (config.somatic_variant_caller == "strelka2" || config.somatic_variant_caller == "both" )) {
      final_merged.persist(StorageLevel.DISK_ONLY).setName(DuplicateMarkedRecordsRDDName);
      chromDepths = biotools.getChromCoverage(final_merged, dict, config.readLength, config.defaultAverageCoverage, normalRGIDs, config.filterNonPrimaryChromosomes)
      debug("chromDepths: " + chromDepths.map(x => x._1 + ":" + x._2).mkString(","))
    }
    //  & BQSR IF NEEDED
    if(config.knownSites == null) runVariantCaller(config, output, final_merged, gatk_regions, dict, header, tumor_sm, normal_sm, chromDepths)
    else if (config.saveBam != null) {
      val customBqsrTable = readBqsrTable(config.bqsrTable)
      val bqsr_final_merged = biotools.runBqsr(final_merged, gatk_regions, customBqsrTable)
      unpersistAlignedRecordRDDs()
      bqsr_final_merged.persist(config.persistLevel).setName(BqsrDoneRecordsRDDName)

      runVariantCaller(config, output, bqsr_final_merged, gatk_regions, dict, header, tumor_sm, normal_sm, chromDepths)
      final_merged.unpersist()
    } else runBqsrAndVariantCaller(config, output, final_merged, gatk_regions, dict, header, tumor_sm, normal_sm, chromDepths)
  }

  protected def runBqsrAndVariantCaller(config: Config, output: String, merged: RDD[(Int, Array[Byte])], gatk_regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], header: Array[String], tumor_sm: String, normal_sm: String, chromDepths : Map[String, Long]) {

    val customBqsrTable = readBqsrTable(config.bqsrTable)
    if(merged.getStorageLevel == StorageLevel.NONE)
      merged.persist(config.persistLevel).setName(DuplicateMarkedRecordsRDDName)
    if(isRDDPersisted(DuplicateMarkedRecordsRDDName))
      unpersistAlignedRecordRDDs()

    config.somatic_variant_caller match {
      case "mutect2" =>  {
        // THIS IS THE DEFAULT
        val variants = biotools.runBqsrAndMutect2(merged, gatk_regions, dict, tumor_sm, normal_sm, customBqsrTable)
        unpersistAlignedRecordRDDs()
        info(measureTime("Job 2"))
        variants.persist(config.persistLevel).setName(VariantsRDDName)
        val callable = variants.map(_._1.toLong).reduce(_+_)
        info("Callable=" + callable.toInt.toFloat + " [Actual=" + callable + "]")
        merged.unpersist()
        val header = createMutect2Header(dict, tumor_sm, normal_sm)
        saveVariants(config, dict, output, header, variants.flatMap(_._2), config.somatic_variant_caller, Some(callable))
        unpersistRDDByName(DuplicateMarkedRecordsRDDName);
        variants.unpersist()
        info(measureTime("Job 3"))
      }
      case "strelka2" =>  {
        val variants =  biotools.runBqsrAndStrelka2(merged, gatk_regions, dict, tumor_sm, normal_sm, chromDepths.map(x => x._1 + "\t" + x._2).mkString("\n"), config.isExome, config.filterNonPrimaryChromosomes, customBqsrTable)
        variants.persist(config.persistLevel).setName(VariantsRDDName)
        unpersistAlignedRecordRDDs()
        info(measureTime("Job 2"))
        val header = createStrelka2Header(dict, chromDepths, tumor_sm, normal_sm)
        saveVariants(config, dict, output, header, variants, config.somatic_variant_caller)
        unpersistRDDByName(DuplicateMarkedRecordsRDDName);
        variants.unpersist()
        info(measureTime("Job 3"))
      }
      case "both" => {
        val final_merged = biotools.runBqsr(merged, gatk_regions, customBqsrTable)
        unpersistAlignedRecordRDDs()
        final_merged.persist(config.persistLevel).setName(BqsrDoneRecordsRDDName)

        val mutect2variants = biotools.runMutect2(final_merged, gatk_regions, dict, tumor_sm, normal_sm)
        info(measureTime("Job 2"))
        mutect2variants.persist(config.persistLevel).setName(VariantsRDDName)
        val callable = mutect2variants.map(_._1.toLong).reduce(_+_)
        info("Callable=" + callable.toInt.toFloat + " [Actual=" + callable + "]")
        val mutect2header = createMutect2Header(dict, tumor_sm, normal_sm)
        saveVariants(config, dict, getMutect2File(output), mutect2header, mutect2variants.flatMap(_._2), "mutect2", Some(callable))
        mutect2variants.unpersist()
        info(measureTime("Job3 - Mutect2"))

        if(isRDDPersisted(BqsrDoneRecordsRDDName))
          unpersistRDDByName(DuplicateMarkedRecordsRDDName)

        val strelka2variants = biotools.runStrelka2(final_merged, gatk_regions, dict, tumor_sm, normal_sm, chromDepths.map(x => x._1 + "\t" + x._2).mkString("\n"), config.isExome, config.filterNonPrimaryChromosomes)
        strelka2variants.persist(config.persistLevel).setName(VariantsRDDName)
        val strelka2header = createStrelka2Header(dict, chromDepths, tumor_sm, normal_sm)
        saveVariants(config, dict, getStrelka2File(output), strelka2header, strelka2variants, "strelka2")
        unpersistRDDByName(BqsrDoneRecordsRDDName)
        strelka2variants.unpersist()
        info(measureTime("Strelka2"))
      }
      case _ => throw new Exception("invalid somatic variant caller: " + config.somatic_variant_caller)
    }
  }

  protected def runVariantCaller(config: Config, output: String, final_merged: RDD[(Int, Array[Byte])], gatk_regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], header: Array[String], tumor_sm: String, normal_sm: String, chromDepths : Map[String, Long]) {

    if(config.saveBam != null) {
      saveBAMSplitsAsFile(sc, final_merged, config.saveBam)
      // unpersist the rdd duplicatemarked if it has been saved
      unpersistAlignedRecordRDDs()
    }
     if(isRDDPersisted(DuplicateMarkedRecordsRDDName)) {
       debug("DuplicateMarked has been persisted, unpersisting alignedrecordRdds")
       unpersistAlignedRecordRDDs()
    }


    config.somatic_variant_caller match {
      case "mutect2" =>  {
        val variants = biotools.runMutect2(final_merged, gatk_regions, dict, tumor_sm, normal_sm)
        info(measureTime("Job 2"))
        variants.persist(config.persistLevel).setName(VariantsRDDName)
        val callable = variants.map(_._1.toLong).reduce(_+_)
        info("Callable=" + callable.toInt.toFloat + " [Actual=" + callable + "]")
        unpersistAlignedRecordRDDs()
        unpersistRDDByName(DuplicateMarkedRecordsRDDName);
        unpersistRDDByName(BqsrDoneRecordsRDDName);
        val header = createMutect2Header(dict, tumor_sm, normal_sm)
        saveVariants(config, dict, output, header, variants.flatMap(_._2), config.somatic_variant_caller, Some(callable))
        variants.unpersist()
        info(measureTime("Job 3"))
      }
      case "strelka2" =>  {
        val variants = biotools.runStrelka2(final_merged, gatk_regions, dict, tumor_sm, normal_sm, chromDepths.map(x => x._1 + "\t" + x._2).mkString("\n"), config.isExome, config.filterNonPrimaryChromosomes)
        info(measureTime("Job 2"))
        variants.persist(config.persistLevel).setName(VariantsRDDName)
        val header = createStrelka2Header(dict, chromDepths, tumor_sm, normal_sm)
        saveVariants(config, dict, output, header, variants, config.somatic_variant_caller)
        unpersistAlignedRecordRDDs()
        unpersistRDDByName(DuplicateMarkedRecordsRDDName);
        unpersistRDDByName(BqsrDoneRecordsRDDName);
        variants.unpersist()
        info(measureTime("Job 3"))
      }
      case "both" => {
        if(final_merged.getStorageLevel == StorageLevel.NONE)
          final_merged.persist(config.persistLevel).setName(BqsrDoneRecordsRDDName)
        val mutect2variants = biotools.runMutect2(final_merged, gatk_regions, dict, tumor_sm, normal_sm)
        info(measureTime("Job 2"))
        mutect2variants.persist(config.persistLevel).setName(VariantsRDDName)
        val callable = mutect2variants.map(_._1.toLong).reduce(_+_)
        info("Callable=" + callable.toInt.toFloat + " [Actual=" + callable + "]")
        unpersistAlignedRecordRDDs()
        if(config.knownSites != null) unpersistRDDByName(DuplicateMarkedRecordsRDDName);
        val mutect2header = createMutect2Header(dict, tumor_sm, normal_sm)
        saveVariants(config, dict, getMutect2File(output), mutect2header, mutect2variants.flatMap(_._2), "mutect2", Some(callable))
        mutect2variants.unpersist()
        info(measureTime("Job 3 - Mutect2"))

        val strelka2variants = biotools.runStrelka2(final_merged, gatk_regions, dict, tumor_sm, normal_sm, chromDepths.map(x => x._1 + "\t" + x._2).mkString("\n"), config.isExome, config.filterNonPrimaryChromosomes)
        strelka2variants.persist(config.persistLevel).setName(VariantsRDDName)
        val strelka2header = createStrelka2Header(dict, chromDepths, tumor_sm, normal_sm)
        final_merged.unpersist()
        saveVariants(config, dict, getStrelka2File(output), strelka2header, strelka2variants, "strelka2")
        unpersistRDDByName(BqsrDoneRecordsRDDName);
        strelka2variants.unpersist()
        info(measureTime("Strelka2"))
      }
      case _ => throw new Exception("invalid somatic variant caller: " + config.somatic_variant_caller)
    }
  }

  @deprecated("Untested. ", "1.6")
  protected def pipelineWithElprep(config: Config, output: String, merged_with_overlap: RDD[SplitIdAndRecordWithPosition],
    gatk_regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], header: Array[String], tumor_sm: String, normal_sm: String, duplicatesHaveBeenMarked : Boolean, normalRGIDs : String) {
    //  MARK DUPLICATES & BQSR
    var merged_with_rg: RDD[(Int, Array[Byte])] = biotools.sortAndConvertToBam(merged_with_overlap, header)
    val final_merged: RDD[(Int, Array[Byte])] = biotools.elprep(merged_with_rg, !duplicatesHaveBeenMarked, config.knownSites, config.reference.replace("fasta","elfasta"))
    var chromDepths : Map[String, Long] = null;
    if(!config.isExome && (config.somatic_variant_caller == "strelka2" || config.somatic_variant_caller == "both" )) {
      final_merged.persist(StorageLevel.DISK_ONLY).setName(DuplicateMarkedRecordsRDDName);
      chromDepths = biotools.getChromCoverage(final_merged, dict, config.readLength, config.defaultAverageCoverage, normalRGIDs, config.filterNonPrimaryChromosomes)
      debug("chromDepths: " + chromDepths.map(x => x._1 + ":" + x._2).mkString(","))
    }
    runVariantCaller(config, output, final_merged, gatk_regions, dict, header, tumor_sm, normal_sm, chromDepths)
  }


  /**
  * Call this after you persisted the RDD!
  */
  protected def saveVariants(config: Config, dict: Map[String, DictIndexAndPosition], output: String, vcfheader: Array[String], variants: RDD[String], variantCallType: String, callable: Option[Long] = None) {
    variantCallType match {
      case "strelka2" => {
        val splitvcfoutput = getSplitVcfDirectory(output)
        // variants.saveAsTextFile(splitvcfoutput.replace(".split", ".variants"))
        val merged = deduplicateVariantsAndAddHeader(sc, dict, variants, vcfheader, compareStrelka2Variants)
        if (config.overwriteOutput) assert(deleteRecursively(new File(output)))
        if (config.overwriteOutput) assert(deleteRecursively(new File(splitvcfoutput)))
        merged.saveAsTextFile(splitvcfoutput)

        saveMergedVcfFiles(sc, splitvcfoutput, output, true)
      }
      case "mutect2" => {
        val splitvcfoutput = getSplitVcfDirectory(output)
        // variants.saveAsTextFile(splitvcfoutput.replace(".split", ".variants"))
        val merged = deduplicateVariantsAndAddHeader(sc, dict, variants, vcfheader, compareMutect2Variants)
        if (config.overwriteOutput) assert(deleteRecursively(new File(output)))
        if (config.overwriteOutput) assert(deleteRecursively(new File(splitvcfoutput)))
        merged.saveAsTextFile(splitvcfoutput)

        saveMergedVcfFiles(sc, splitvcfoutput, output, true, callable)
      }
      case _ => {
        throw new Exception("invalid variantCallType: " + variantCallType)
      }
    }

  }

}
