package be.ugent.intec.halvade.job

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import be.ugent.intec.halvade.Utils._
import be.ugent.intec.halvade.FileUtils._
import org.apache.spark.storage.StorageLevel
import java.io.File
import org.apache.spark.rdd.RDD
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf;
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import scopt.OParser

object GenotypeTrio extends HalvadeJob {
  override def jobTitle = "Halvade Genotype Trio pipeline"
  import baseBuilder._
  override val pipelineParser = OParser.sequence(
    head(jobTitle, halvadeVersion),

    baseParser,

    opt[String]('I', "index")
      .action( (x, c) => c.copy(index = x) )
      .text("Location of the index files.")
      .required(),

    opt[String]('F', "father")
      .action( (x, c) => c.copy(father = x) )
      .text("Location of the father files.")
      .required(),

    opt[String]('M', "mother")
      .action( (x, c) => c.copy(mother = x) ).text("Location of the mother files.")
      .required(),

    opt[String]('o', "output")
      .action( (x, c) => c.copy(output = x) ).text("The output directory.")
      .required(),

    opt[String]("chromosomelist")
      .action( (x, c) => c.copy(chromosomelist = x) ).text("The file containing a list of chromosomes to process [chr1-22,chrX,chrY,chrM]."),

    opt[String]("dbsnp")
      .action( (x, c) => c.copy(dbsnp = x) )
      .text("Location of the VCF file containing dbsnps sites.")
      .required(),

    opt[String]("hapmap")
      .action( (x, c) => c.copy(hapmap = x) )
      .text("Location of the VCF file containing hapmap sites."),

    opt[String]("omni")
      .action( (x, c) =>   c.copy(omni = x) )
      .text("Location of the VCF file containing omni sites."),

    opt[String]("thousandg")
    .action( (x, c) => c.copy(thousandg = x) )
    .text("Location of the VCF file containing 1000g sites."),

    opt[String]("sample_names")
      .action( (x, c) => c.copy(sampleNames = x) )
      .text("The sample names of the input files in this format: IndexRGSM;FatherRGSM;MotherRGSM.")
      .required(),

  )

  def setJobDefaults(config_ : Config): Config = {
    if(config_.persistLevel == null)
      config_.copy(persistLevel = StorageLevel.MEMORY_AND_DISK_SER) // need to check this for genotyping...
    else
      config_
  }

  def runPipeline(config: Config): Unit = {

//     val mychromosomelist = if(config.chromosomelist == null)
//                               Array("1", "2", "3", "4", "5",
//                                "6", "7", "8", "9", "10",
//                                "11", "12", "13", "14", "15",
//                                "16", "17", "18", "19", "20",
//                                "21", "22", "X", "Y", "MT")
//                             else
//                                 sc.textFile(config.chromosomelist).collect()
//
//     startTime()
//     val (splitvcf, regions) = sortAndPartitionVcfFiles(sc, config.index, config.father, config.mother, config.partitions, mychromosomelist, config.sampleNames)
//
//     val genotypedvcf = biotools.genotype(splitvcf, regions.map(x => ( be.ugent.intec.halvade.PartitionSplit(x._1, 0), x._2)))
// //    genotypedvcf.persist(config.persistLevel)
//     genotypedvcf.saveAsTextFile(config.output)
// //    val filteredvcf = gatk.filterAndVqsr(genotypedvcf, regions, config.hapmap, config.omni, config.thousandg);
// //    filteredvcf.saveAsTextFile(config.output)
//
// //    genotypedvcf.unpersist();
//     info(measureTotalTime("Trio Genotyping"))
  }
}
