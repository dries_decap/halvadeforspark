package be.ugent.intec.halvade.prep

import java.io.BufferedReader
import be.ugent.intec.halvade.Logging
import org.apache.hadoop.fs.{FileSystem, Path}

class SingleFastQReader(override val fs: FileSystem, override val readgroup : String, val fileA: Path, override val isInterleaved : Boolean = true) extends BaseFileReader(fs, isInterleaved, readgroup) with Logging {
  var readerA: BufferedReader = null;
  var gzipreaderA: ExposedGZIPInputStream = null;
  val readsFactor: Int = if(isInterleaved) 2 else 1
  override def toString() : String = {
    return fileA.toString;
  }
  override def initStreams() = {
    val (ra, gzipa) = getReader(fileA);
    readerA = ra;
    gzipreaderA = gzipa
  }

  override def getBytesRead() : Long = {
      return gzipreaderA.getBytesRead()
  }

  override def closeStreams() {
    gzipreaderA.close()
    readerA.close()
  }

  override def addNextRead(block: ReadBlock): Int = {
    if(block.checkCapacity(LINES_PER_READ*readsFactor)) {
      block.setCheckPoint();
      var check : Boolean = true;
      var i: Int = 0;
      while(i < LINES_PER_READ*readsFactor && check) {
          check = block.fastAddLine(readerA.readLine());
          i+=1;
      }
      if(!check)  {
        block.revertToCheckPoint();
        return -1;
      }
      return 0;
    } else {
      return 1;
    }
  }
}
