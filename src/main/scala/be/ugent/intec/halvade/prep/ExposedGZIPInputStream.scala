package be.ugent.intec.halvade.prep

import java.util.zip.GZIPInputStream;
import java.io.InputStream

class ExposedGZIPInputStream(stream: InputStream, n: Int) extends GZIPInputStream(stream, n) {

  def this(stream: InputStream) = {
    this(stream, n = 512)
  }

  def getBytesRead(): Long = {
    return this.inf.getBytesRead();
  }
}
