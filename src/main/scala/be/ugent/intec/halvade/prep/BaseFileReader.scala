package be.ugent.intec.halvade.prep

import java.io.{BufferedReader, InputStream, InputStreamReader, FileInputStream, FileReader, IOException}
import java.util.zip.GZIPInputStream;
import org.apache.hadoop.fs.{FileSystem, Path};
import org.apache.hadoop.conf.Configuration
import be.ugent.intec.halvade.Logging
import scala.collection.mutable.ArrayBuffer

abstract class BaseFileReader(val fs: FileSystem, val isInterleaved: Boolean = false, val readgroup: String) extends Logging {
  debug("fs: " + fs)
  final val BUFFERSIZE : Int = 1024*1024 // 128 MB
  final val LINES_PER_READ : Int = 4

  override def toString() : String = {
    return "";
  }

  def getReadGroupId() : String = readgroup

  protected def getReader(file: Path): (BufferedReader, ExposedGZIPInputStream) = {
    val inStream: InputStream = fs.open(file);
    // read the stream in the correct format!
    if(file.getName.endsWith(".gz")) {
      val gzipstream = new ExposedGZIPInputStream(inStream, BUFFERSIZE)
      return (new BufferedReader(new InputStreamReader(gzipstream)), gzipstream);
    } else {
      return (new BufferedReader(new InputStreamReader(inStream)), null);
    }
  }

  def initStreams();
  def addNextRead(block: ReadBlock): Int;
  def closeStreams();
  def getBytesRead() : Long ;
}
