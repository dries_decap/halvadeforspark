package be.ugent.intec.halvade.prep

import java.net.URI
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.compress.{CompressionCodec, CompressionCodecFactory}
import java.io.InputStream
import scala.collection.mutable.ArrayBuffer
import org.seqdoop.hadoop_bam.{BAMInputFormat,SAMRecordWritable}
import be.ugent.intec.halvade.spark.input.UnsplittableTextInputFormat
import be.ugent.intec.halvade.Logging
import be.ugent.intec.halvade.spark.partitioner.OneElementPerPartitionPartitioner
import be.ugent.intec.halvade.spark.ConfigSerDeser
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel
import org.apache.spark.rdd.RDD
import scala.collection._
import mutable.ListBuffer
import be.ugent.intec.halvade.DictIndexAndPosition
import scala.collection.Map // ensure all maps are the same!
import java.util.UUID

object Preprocessor extends Logging {

  def preprocessUnaligedReads(manifest: String, pairedEndFqFiles: Array[String], outputDir: String, threads : Int, sc: SparkContext, persistLevel: StorageLevel = StorageLevel.DISK_ONLY, codectype: String = "") {
    // reset to first state!
    val conf = new Configuration(sc.hadoopConfiguration)
    val codec = getCodec(conf, codectype)
    val (fastqFiles, bamFiles) = parseInputData(manifest, pairedEndFqFiles, conf)
    processBamFiles(bamFiles, conf, outputDir, codec, sc, persistLevel)
    // processFastqFilesOnDriver(fastqFiles, sc, threads, outputDir, conf, codec)
    processFastqFiles(fastqFiles, sc, threads, outputDir, conf, codec)
  }

  protected def getCodec(conf: Configuration, codectype: String)  : String = {
    if(codectype == "snappy") {
      return "org.apache.hadoop.io.compress.SnappyCodec";
    } else if(codectype == "lz4") {
      return "org.apache.hadoop.io.compress.Lz4Codec";
    } else {
      return "org.apache.hadoop.io.compress.GzipCodec"
    }
  }

  protected def parseInputData(manifest: String, pairedEndFqFiles: Array[String], conf: Configuration) : (Array[(Path, Path, String)], Array[String]) = { // returns array of fastq files + array of bam files
    var fastqFiles: ArrayBuffer[(Path, Path, String)] = ArrayBuffer.empty[(Path, Path, String)]
    var bamFiles: ArrayBuffer[String] = ArrayBuffer.empty[String]

    if(manifest != null) {
      info("reading input files from " + manifest);
      // read from file
      val infs: FileSystem = FileSystem.get(new URI(manifest), conf);
      val in: InputStream = infs.open(new Path(manifest));
      val lines = scala.io.Source.fromInputStream(in).getLines
      for (line <- lines) {
        // check if fastq or bamfiles!
        val split: Array[String] = line.split("\t");
        val fs = FileSystem.get(new URI(split(0)), conf)
        if(split.length == 3) {
          fastqFiles += ((fs.makeQualified(new Path(split(0))), fs.makeQualified(new Path(split(1))), split(2)))
        } else if(split.length == 2) {
          if(split(1).startsWith("/"))
            fastqFiles += ((fs.makeQualified(new Path(split(0))), fs.makeQualified(new Path(split(1))), UUID.randomUUID().toString.split("-").last))
          else
            fastqFiles += ((fs.makeQualified(new Path(split(0))), null, split(1)))
        } else if(split.length == 1) {
          if(split(0).endsWith(".bam") ) {
            bamFiles += split(0)
          } else {
            fastqFiles += ((fs.makeQualified(new Path(split(0))), null, UUID.randomUUID().toString.split("-").last))
          }
        } else {
          throw new Exception("invalid line in manifest: " + line)
        }
      }
    }
    for(pair <- pairedEndFqFiles) {
      val split = pair.split(",")
      val fs = FileSystem.get(new URI(split(0)), conf)
      if (split.size == 2) {
        fastqFiles += ((fs.makeQualified(new Path(split(0))), fs.makeQualified(new Path(split(1))), UUID.randomUUID().toString.split("-").last))
      } else if (split.size == 3) {
        fastqFiles += ((fs.makeQualified(new Path(split(0))), fs.makeQualified(new Path(split(1))), split(2)))
      } else {
        throw new Exception("invalid paired-end file identifier: " + pair)
      }
    }

    return (fastqFiles.toArray, bamFiles.toArray)
  }


  protected def processBamFiles(bamfiles: Array[String], conf: Configuration, outputDir: String, codec: String, sc: SparkContext, persistLevel: StorageLevel) {
    if (bamfiles.size > 0) {
      // collect data from bamfiles
      val input = bamfiles.mkString(",")
      val tmp = sc.newAPIHadoopFile(input, classOf[BAMInputFormat], classOf[LongWritable], classOf[SAMRecordWritable], conf)
        .map(x => {
            val rec = x._2.get()
            (rec.getReadName, '@' + rec.getReadName + (if (rec.getFirstOfPairFlag) "/1\n" else "/2\n") + rec.getReadString + "\n+\n" + rec.getBaseQualityString)
          })
      tmp.persist(persistLevel)
      val count = (tmp.count() / 800000).toInt + 1; // +- 0.8125M reads per file.... is about 60mb based on 151 baselength
      tmp.reduceByKey((a:String,b:String) => {
          val splita = a.split("\n")
          val splitb = b.split("\n")
          if(splita.length == 8 ) a
          else if(splitb.length == 8) b
          else {
            if(splita(0) != splitb(0) && splita(1).length == splitb(1).length) a + "\n" + b// not same id -> means 1/2
            else if (splita(1).length >= splitb(1).length) a  // same id, get longest read length, other is trimmed
            else b
          }
        }, count).map(_._2).saveAsTextFile(outputDir, new CompressionCodecFactory(new Configuration()).getCodecByClassName(codec).getClass())
    }
  }


  protected def getFileSize(path: Path, fs: FileSystem) : Long = {
    if(path == null) return 0L
    var length = 0L
    try {
      val cSummary = fs.getContentSummary(path)
      length = cSummary.getLength
    } catch {
      case e: Exception => {
        debug("error from getting filesize: " + e.getMessage())
      }
    }
    return length
  }

  protected def processFastqFilesOnDriver(fastqfiles: Array[(Path, Path, String)], sc: SparkContext, threads: Int, outputDir: String, conf: Configuration, codec: String) {
    var count: Long = 0
    for (fastq <- fastqfiles) {
      count = count + processFastq(fastq._1, fastq._2, fastq._3, threads, outputDir, codec, new ConfigSerDeser(conf))
    }
    info("Total # reads processed: " + count);
  }

  protected def processFastqFiles(fastqfiles: Array[(Path, Path, String)], sc: SparkContext, threads: Int, outputDir: String, conf: Configuration, codec: String) {
    val rdd = sc.parallelize(fastqfiles)
                       .zipWithIndex.map(x => (x._2, x._1))
                       .partitionBy(new OneElementPerPartitionPartitioner(fastqfiles.size))
    debug("data to process:\n" + rdd.collect.mkString("\n"))
    val serdeserconf = new ConfigSerDeser(conf)
    val totalReads = rdd.mapPartitions(it => {
      var count: Long = 0
      for (fastq <- it) {
        count = count + processFastq(fastq._2._1, fastq._2._2, fastq._2._3, threads, outputDir, codec, serdeserconf)
      }
      List(count).iterator
    }).reduce(_+_)

    info("Total # reads processed: " + totalReads);
  }

  protected def processFastq(fileA: Path, fileB: Path, readGroupId: String, threads: Int, outputDir: String, codec: String, conf_ : ConfigSerDeser) : Long = {

    info("processing read group with ID " + readGroupId + " with " + threads + " writing threads");
    val conf = conf_.get()

    val infs = fileA.getFileSystem(conf)
    val outfs = FileSystem.get(new URI(outputDir), conf)
    val fileSize = getFileSize(fileA, infs) + getFileSize(fileB, infs)
    val reader : BaseFileReader = if (fileB == null)  new SingleFastQReader(infs, readGroupId, fileA) else new PairedFastQReader(infs, readGroupId, fileA, fileB);
    info("processing " + reader.toString())
    reader.initStreams()
    val parser : FastqReadBlockParser = new FastqReadBlockParser(reader, fileSize, threads);
    parser.start();

    var writerThreads : Array[FastqReadBlockWriter] = new Array[FastqReadBlockWriter](threads)
    for(t <- 0 until threads) {
      writerThreads(t) = new FastqReadBlockWriter(parser, outputDir + "/" + readGroupId + "/halvade_fq_input_" + t + "_", t, outfs, conf, codec);
      writerThreads(t).start();
    }
    for(t <- 0 until threads) {
      writerThreads(t).join();
    }
    parser.join();
    info("# reads processed in " + readGroupId + ": " + parser.getTotalCount);
    return parser.getTotalCount;
  }

  def preprocessAlignedBam(input: String, dict: Map[String, DictIndexAndPosition], sc: SparkContext) : (Array[String], RDD[String]) = {

    val conf_ = new Configuration(sc.hadoopConfiguration)
    val rdds = input.split(",").map(x => sc.newAPIHadoopFile(x, classOf[BAMInputFormat], classOf[LongWritable], classOf[SAMRecordWritable], conf_).map(_._2.get()))
    val headers = rdds.flatMap(tmprdd => tmprdd.map(x => x.getHeader().getSAMString()).take(1)(0).split("\n")) // ensure we get header of all files present

    (headers, rdds.reduce(_ union _).map(x => x.getSAMString.dropRight(1)))
  }

  def preprocessAlignedSam(input: String, dict: Map[String, DictIndexAndPosition], sc: SparkContext) : (Array[String], RDD[String]) = {

    val conf_ = new Configuration(sc.hadoopConfiguration)
    val tmprdd = sc.newAPIHadoopFile(input, classOf[UnsplittableTextInputFormat], classOf[LongWritable], classOf[Text], conf_).map(_._2.toString)
    val header = tmprdd.mapPartitionsWithIndex((idx, iter) => if (idx == 0) iter else Iterator()).filter(_(0) == '@').collect()

    (header.filter(_.startsWith("@RG")), tmprdd)
  }
}
