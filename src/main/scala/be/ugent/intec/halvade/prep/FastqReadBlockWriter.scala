package be.ugent.intec.halvade.prep

import java.io.{OutputStream, BufferedOutputStream, IOException}
import be.ugent.intec.halvade.Logging
import java.util.zip.GZIPOutputStream;
import org.apache.hadoop.fs.{FSDataOutputStream, FileSystem, Path};
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.compress.{CompressionCodec, CompressionCodecFactory};


class FastqReadBlockWriter(val blockparser: FastqReadBlockParser, val fileBase: String, val thread: Int, fs: FileSystem, conf: Configuration, codec_ : String) extends Thread with Logging {
  final val maxFileSize = 60000000 // < 64M
  final val BUFFERSIZE: Int = 8*1024
  val useHadoopCompression: Boolean = false
  var written: Long = 0
  var read: Long = 0
  var count: Long = 0
  var codec = new CompressionCodecFactory(conf).getCodecByClassName(codec_ )
  debug("fs: " + fs)

  def round(value: Double): Double = {
    return (value * 100 + 0.5).toInt / 100.0;
  }

  def getNewDataStream(part: Int, prefix: String): OutputStream = {
    return fs.create(new Path(prefix + part + (if (useHadoopCompression) ".fq" + codec.getDefaultExtension() else ".fq.gz")),true);
  }
  def getNewCompressedStream(dataStream: OutputStream): BufferedOutputStream = {
    return new BufferedOutputStream(if(useHadoopCompression) codec.createOutputStream(dataStream) else new GZIPOutputStream(dataStream), BUFFERSIZE)
  }
  def getSize(dataStream: OutputStream): Int = {
    return dataStream.asInstanceOf[FSDataOutputStream].size();
  }
  def writeData(part: Int, dataStream: OutputStream, gzipStream: BufferedOutputStream) = {
    gzipStream.close();
    dataStream.close();
  }
  def resetDataStream(part: Int, prefix: String, dataStream: OutputStream): OutputStream = {
    return fs.create(new Path(prefix + part + (if (useHadoopCompression) ".fq" + codec.getDefaultExtension() else ".fq.gz")),true);
  }
  def deleteFile(prefix: String, part: Int) = {
    fs.delete(new Path(prefix + part + (if (useHadoopCompression) ".fq" + codec.getDefaultExtension() else ".fq.gz")), true);
  }
  def closeStreams(dataStream: OutputStream, gzipStream: OutputStream) = {
    dataStream.close();
    gzipStream.close();
  }

  def write(outStream: OutputStream, data: Array[Byte]): Long = {
    outStream.write(data);
    return data.size;
  }

  override def run() {
    try {
      var part: Int = 0
      var tSize: Int = 0
      var fileWritten : Long = 0;
      var dataStream: OutputStream = getNewDataStream(part,fileBase);
      var gzipStream: BufferedOutputStream = getNewCompressedStream(dataStream);
      var block: ReadBlock = blockparser.retrieveBlock();
      while(block != null) {
        fileWritten += block.write(gzipStream);
        count += block.size;
        // info("block size: " + block.size)
        tSize = getSize(dataStream);
        if(tSize > maxFileSize) {
          writeData(part, dataStream, gzipStream);
          written += tSize;
          read += fileWritten;
          fileWritten = 0;
          part+=1;
          dataStream = resetDataStream(part, fileBase, dataStream);
          gzipStream = getNewCompressedStream(dataStream);
        }
        block = blockparser.retrieveBlock();
      }
      // finish the files
      gzipStream.close();
      tSize = getSize(dataStream);
      if(tSize == 0 || fileWritten == 0) {
        deleteFile(fileBase, part);
      } else if(fileWritten != 0) {
        writeData(part, dataStream, gzipStream);
        written += tSize;
        read += fileWritten;
      }
      closeStreams(dataStream, gzipStream);
    } catch {
      case ex: IOException => {
        error("error occured: " + ex.getMessage())
        throw ex
      }
    }
  }
}
