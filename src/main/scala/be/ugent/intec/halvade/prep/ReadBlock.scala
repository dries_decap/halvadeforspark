package be.ugent.intec.halvade.prep

import java.io.OutputStream
import be.ugent.intec.halvade.Logging
import scala.collection.mutable.ArrayBuffer

class ReadBlock(val capacity: Int = 50000) extends Logging {
  var size: Int = 0;
  var lastSize: Int = -1;
  val reads: Array[String] = new Array[String](capacity);

  def reset() {
    size = 0
  }
  def getSize() : Int = {
    return size;
  }
  def getData(): Array[Byte] = {
    val arr : ArrayBuffer[Byte] = new ArrayBuffer[Byte]()
    for(i <- 0 until size) {
      arr ++= reads(i).getBytes()
      arr += '\n'
    }
    arr.toArray
  }
  def write(outStream: OutputStream): Long = {
    var bytes: Long = 0
    var len: Int = 0;
    for(i <- 0 to size -1) {
      len = reads(i).length();
      bytes += len + 1;
      outStream.write((reads(i) + "\n").getBytes(), 0, len + 1);
    }
    return bytes;
  }
  def secureAddLine(line: String): Boolean = {
    if(size < capacity && line != null) {
      reads(size) = line;
      size+=1;
      return true;
    } else {
      return false;
    }
  }
  def fastAddLine(line: String): Boolean ={
    if(line == null) return false;
    reads(size) = line;
    size+=1;
    return true;
  }

  def setCheckPoint() = {
    lastSize = size
  }
  def revertToCheckPoint(): Boolean = {
    if(lastSize != -1) {
      size = lastSize;
      return true;
    } else
      return false;
  }
  def checkCapacity(lineBlockCount: Int) : Boolean = {
    return size + lineBlockCount <= capacity;
  }
}
