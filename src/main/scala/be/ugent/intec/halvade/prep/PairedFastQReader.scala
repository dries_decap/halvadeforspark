package be.ugent.intec.halvade.prep

import java.io.BufferedReader
import be.ugent.intec.halvade.Logging
import org.apache.hadoop.fs.{FileSystem, Path}

class PairedFastQReader(override val fs: FileSystem, override val readgroup: String, val fileA: Path, val fileB: Path, override val isInterleaved : Boolean = false) extends BaseFileReader(fs, isInterleaved, readgroup) with Logging {
  var readerA: BufferedReader = null;
  var readerB: BufferedReader = null;
  var gzipreaderA: ExposedGZIPInputStream = null;
  var gzipreaderB: ExposedGZIPInputStream = null;

  override def toString() : String = {
    return fileA + " & " + fileB;
  }

  override def initStreams() = {
    val (ra, gzipa) = getReader(fileA);
    val (rb, gzipb) = getReader(fileB);
    readerA = ra;
    readerB = rb;
    gzipreaderA = gzipa
    gzipreaderB = gzipb
  }

  override def getBytesRead() : Long = {
      return gzipreaderA.getBytesRead() + gzipreaderB.getBytesRead()
  }

  override def closeStreams() {
    gzipreaderA.close()
    gzipreaderB.close()
    readerA.close()
    readerB.close()
  }

  override def addNextRead(block: ReadBlock): Int = {
    if(block.checkCapacity(LINES_PER_READ*2)) {
      block.setCheckPoint();
      var check: Boolean = true;
      var i: Int = 0;
      while(check && i < LINES_PER_READ) {
        check = block.fastAddLine(readerA.readLine());
        i+=1;
      }
      i = 0;
      while(i < LINES_PER_READ && check) {
        check = check && block.fastAddLine(readerB.readLine());
        i+=1;
      }
      if(!check) {
        // open next read here....
        block.revertToCheckPoint();
        return -1;
      }
      return 0;
    } else {
      return 1;
    }
  }
}
