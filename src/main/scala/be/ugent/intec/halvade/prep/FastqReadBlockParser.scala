package be.ugent.intec.halvade.prep

import scala.collection.mutable.ArrayBuffer
import java.util.concurrent.{TimeUnit, ArrayBlockingQueue};
import be.ugent.intec.halvade.Logging
import be.ugent.intec.halvade.Utils._
import org.apache.hadoop.fs.{FileSystem, Path};
import org.apache.hadoop.conf.Configuration

class FastqReadBlockParser(val reader: BaseFileReader, fileSize: Long, val threads: Int) extends Thread with Logging {

  final val LINES_PER_READ : Int = 4
  final val READ_BLOCK_CAPACITY_PER_THREAD: Int = 10
  var count : Long = 0;
  var blocks: ArrayBlockingQueue[ReadBlock] = new ArrayBlockingQueue(READ_BLOCK_CAPACITY_PER_THREAD*threads);
  var active = -1
  var eofReached = false;

  def retrieveBlock() : ReadBlock = {
    try {
      var block: ReadBlock = null;
      while((!eofReached || blocks.size() > 0) && block == null) {
        block = blocks.poll(1000, TimeUnit.MILLISECONDS);
      }
      return block;
    } catch {
      case ex: InterruptedException => {
        error("error in retrieving block: " + ex.getMessage())
        return null;
      }
    }
  }

  def getTotalCount() : Long = count;

  protected def getNextBlock(): Boolean = synchronized {
    val block: ReadBlock = new ReadBlock();
    // block.reset();
    try {
      while(reader.addNextRead(block) == 0) {
      }
      count = count + block.size / LINES_PER_READ;
    } catch {
      case ex: Exception => {
        error("error occured: " + ex.getMessage());
        throw ex
      }
    }

    if(block.size == 0) {return false; }
    else {
      blocks.put(block);
      return true;
    }
  }

  override def run() = {
    count = 0
    showProgressBarNonSparkFunctions("Preprocessing", 0, active, fileSize)
    try {
      var blockcount: Long = 0
      var hasReads: Boolean = getNextBlock();
      while(hasReads) {
        blockcount += 1
        if(blockcount%100 == 0) {
          showProgressBarNonSparkFunctions("Preprocessing", reader.getBytesRead(), active, fileSize)
        }
        hasReads = getNextBlock();
      }
      eofReached = true;
      reader.closeStreams(); // close streams!
      cleanProgressBarNonSparkFunctions()
    } catch {
      case ex: Exception => {
        error("error in filereaderfactory: " + ex.getMessage());
        throw ex
      }
    }
  }
}
