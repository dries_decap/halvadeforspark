package be.ugent.intec.halvade

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import java.io.{File, PrintWriter}
import org.apache.spark.SparkContext

object FileUtils extends be.ugent.intec.halvade.Logging {
  def deleteRecursively(f: File): Boolean = {
    if(!f.exists()) return true // for if the folder doesn't exist!
    if (f.isDirectory) f.listFiles match {
      case null =>
      case xs   => xs foreach deleteRecursively
    }
    f.delete()
  }

  def deleteRecursively(path: String): Boolean = {
    val f: File = new File(path)
    deleteRecursively(f)
  }

  def printToFile(f: File)(op: PrintWriter => Unit) {
    val p = new PrintWriter(f)
    try { op(p) }
    finally { p.close() }
  }
  def printArrayToFile[T](array: Array[T], outputfile: String) = {
    printToFile(new File(outputfile)) {
      p => array.foreach(p.println)
    }
  }

  def readBqsrTable(path: String): String = {
    if (path != null) scala.io.Source.fromFile(path).mkString
    else null
  }
  def readDict(sc: SparkContext, path: String): Map[String, DictIndexAndPosition] = {
    // path should be on file://
    var path_with_fs = path
    if(path.startsWith("/")) path_with_fs = "file://" + path
    sc.textFile(path_with_fs).filter(x => x.startsWith("@SQ")).zipWithIndex.map(x => {
                       var chr = ""
                       var len = 0
                       x._1.split("\t").foreach(y => {
                         val tmp = y.indexOf(":");
                         if (tmp > 0 && y.substring(0, tmp) == "SN") chr = y.substring(tmp + 1)
                         if (tmp > 0 && y.substring(0, tmp) == "LN") len = y.substring(tmp + 1).toInt
                       })
                       (chr, DictIndexAndPosition(x._2.toInt, len))
                     }).collect.toMap
  }

}
