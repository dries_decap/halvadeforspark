package be.ugent.intec.halvade.tools


import be.ugent.intec.halvade.{Logging, DictIndexAndPosition, PartitionSplit}
import be.ugent.intec.halvade.spark.input.UnsplittableTextInputFormat
import be.ugent.intec.halvade.spark.SamRecordFunctions._
import be.ugent.intec.halvade.spark.rdd.ByteArrayRDDFunctions._
import java.io.{File, PrintWriter}
import java.util.{UUID, StringTokenizer}
import java.util.concurrent.atomic.AtomicReference
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{Path, FileSystem}
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import java.net.URI
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.NewHadoopRDDFunctions._
import org.apache.spark.storage.StorageLevel
import scala.collection.JavaConverters._
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.collection.Map // ensure same maps...
import scala.io.{Codec, Source}


@SerialVersionUID(1007L)
class BioTools(val bindir_ : String, val ref: String, val knownSites: String, val tasks: Int, val threads: Int,
           val memory: Double, val logLevel: Level = Level.ERROR,
           val tmp: String = "/tmp/", val cleanupTmp: Boolean = true) extends Serializable with Logging {

  Logger.getLogger("be.ugent.intec.halvade.tools.BioTools").setLevel(logLevel)
  val bindir = if(bindir_.last == '/') bindir_ else bindir_ + "/"

  // placeholders
  val input_bam_placeholder = "input.bam"
  val intervals_placeholder = "target.intervals"
  val bqsr_table_placeholder = "bqsr.table"
  val output_stdout_placeholder = "/dev/stdout"
  val output_bam_placeholder = "output.bam"
  val output_sam_placeholder = "output.sam"
  val input_sam_placeholder = "input.sam"
  val input_vcf_placeholder = "input.vcf"
  val output_vcf_placeholder = "output.vcf"
  val tmp_vcf_placeholder = "tmp.vcf"
  val tmp_rgids_placeholder = "tmp.rgids"
  val output_gvcf_placeholder = "output.g.vcf"
  val gatk_region_placeholder = "gatkregion.intervals"
  val strelka_tsv_placeholder = "chromdepths.tsv"
  val strelka_region_placeholder = "strelkaregion.intervals"
  val bqsr_fix_region_placeholder = "regionwithbqsrfix.intervals"
  val db_placeholder = "genotype.db"
  val output_vcf_gz_placeholder = "output.vcf.gz"
  val input_vcf_gz_placeholder = "input.vcf.gz"
  val tranches_placeholder = "file.tranches"
  val recal_placeholder = "file.recal"
  val tmp_bam_placeholder = "tmp.bam"
  val samtools_sort_prefix_placeholder = "sort-prefix.tmp"

  /**
  * GATK
  */
  // *****************************************************************************
  // * automagically detect the latest version please, thank you very much! *
  // *****************************************************************************
  val files = getListOfFiles(bindir).map(x => x.getName())
  val gatkfilepattern = "[0-9]+.[0-9]+.[0-9]+.[0-9]+".r
  val gatks = files.filter(x => x.startsWith("gatk-package")).map(x => {var result = gatkfilepattern.findFirstIn(x); result match { case Some(v) =>v case _ => ""} } ).map(x =>x.split("\\.")map(_.toInt))
  if(gatks.size == 0) throw new Exception("no gatk found")
  val newestgatk = gatks.reduce((a,b) => {
    if (a(0) < b(0)) b else if (a(0) > b(0)) a
    else {
      if (a(1) < b(1)) b else if (a(1) > b(1)) a
      else {
        if (a(2) < b(2)) b else if (a(2) > b(2)) a
        else {
          if (a(3) < b(3)) b else if (a(3) > b(3)) a
          else {
            throw new Exception("illegal comparison of gatk versions: " + a + " and " + b)
          }
        }
      }
    }
  }).mkString(".")
  val lastgatkt4jar = "gatk-package-" + newestgatk + "-local.jar"
  val java = "java "
  val mem = "-Xmx" + memory.toInt + "m"
  val gatk4 = java + mem + " -jar " + bindir + lastgatkt4jar

  // HaplotypeCaller
  def runMutect2(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], tumor_sm: String, normal_sm: String): RDD[(Int, Array[String])] = {
    val placeholders = Array[String](
      input_bam_placeholder,
      "variants_" + output_vcf_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " index " + placeholders(0),
      gatk4 + " Mutect2" +
             " -R " + ref +
             " --add-output-vcf-command-line false" +
             " " + gatk_region_placeholder +
             " -I " + placeholders(0) +
             " -tumor " + tumor_sm +
             " -normal " + normal_sm +
             " -O " + placeholders(1))
    val procNames = Array[String]("samtools index", "GATK4 Mutect2")
    new org.apache.spark.MultiInstanceMutectPipedRDD(input, commands, placeholders, procNames, regions, tasks, tmp, cleanupTmp, logLevel = logLevel)
          .map(x => (x._1, new String(x._2.map(_.toChar)).split('\n').filter(x => !x.isEmpty && !x.startsWith("#")) ))
  }

  def runBqsrAndMutect2(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], tumor_sm: String, normal_sm: String, table : String = null): RDD[(Int, Array[String])] = {
    val mergedTable = getBqsrTable(input, regions, table)

    val placeholders = Array[String](
      input_bam_placeholder,
      "bqsr_" + output_bam_placeholder,
      "variants_" + output_vcf_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " index " + placeholders(0),
       gatk4 + " ApplyBQSR" +
              " -I "+  placeholders(0) +
              " -bqsr bqsr_" + bqsr_table_placeholder +
              " --add-output-vcf-command-line false" +
              " -O " + placeholders(1) + " " + gatk_region_placeholder,
      gatk4 + " Mutect2" +
             " -R " + ref +
             " --add-output-vcf-command-line false " + gatk_region_placeholder +
             " -I " + placeholders(1) +
             " -tumor " + tumor_sm +
             " -normal " + normal_sm +
             " -O " + placeholders(2))
    val procNames = Array[String]("samtools index", "GATK4 ApplyBQSR", "GATK4 Mutect2")
    new org.apache.spark.MultiInstanceMutectPipedRDD(input, commands, placeholders, procNames, regions, tasks, tmp, cleanupTmp, extraFiles = Map("bqsr_" + bqsr_table_placeholder -> mergedTable), logLevel = logLevel)
          .map(x => (x._1, new String(x._2.map(_.toChar)).split('\n').filter(x=> !x.isEmpty && !x.startsWith("#")) ))
  }

  // HaplotypeCaller
  def runHaplotypeCaller(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], gvcf: Boolean=true, samplename: String): RDD[String] = {
    val placeholders = Array[String](
      input_bam_placeholder,
      output_stdout_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " index " + placeholders(0),
      gatk4 + " HaplotypeCaller" +
               " -R " + ref +
               " --add-output-vcf-command-line false" +
               " -I "+  placeholders(0) +
               " -O " + placeholders(1) +
               (if(gvcf) " -ERC GVCF --sample-name " + samplename else "")
       )
    new org.apache.spark.MultiInstanceChainedCommandPipedRDD(input, commands, placeholders, Array[String]("samtools index", "GATK4 HaplotypeCaller"), regions, tasks, tmp, cleanupTmp, logLevel = logLevel)
          .flatMap(x => new String(x._2.map(_.toChar)).split('\n').filter(x=> !x.isEmpty && !x.startsWith("#")) )
  }
  def runBqsrAndHaplotypeCaller(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], gvcf: Boolean=true, samplename: String, table : String = null): RDD[String] = {
    val mergedTable = getBqsrTable(input, regions, table)

    val placeholders = Array[String](
      input_bam_placeholder,
      "bqsr_" + output_bam_placeholder,
      "variants_" + output_vcf_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " index " + placeholders(0),
      gatk4 + " ApplyBQSR" +
              " -I "+  placeholders(0) +
              " -bqsr bqsr_" + bqsr_table_placeholder +
              " --add-output-vcf-command-line false" +
              " -O " + placeholders(1) +  " " + gatk_region_placeholder,
      gatk4 + " HaplotypeCaller" +
              " -R " + ref +
              " --add-output-vcf-command-line false" + " " + gatk_region_placeholder +
              " -I "+  placeholders(1) +
              " -O " + placeholders(2) +
              (if(gvcf) " -ERC GVCF --sample-name " + samplename else "")
            )
    val procNames = Array[String]("samtools index", "GATK4 ApplyBQSR", "GATK4 HaplotypeCaller")
    new org.apache.spark.MultiInstanceMutectPipedRDD(input, commands, placeholders, procNames, regions, tasks, tmp, cleanupTmp, extraFiles = Map("bqsr_" + bqsr_table_placeholder -> mergedTable), logLevel = logLevel)
          .flatMap(x => new String(x._2.map(_.toChar)).split('\n').filter(x=> !x.isEmpty && !x.startsWith("#")) )
  }

  // MarkDuplicates
  def sortAndMarkDuplicatesPaired(input: RDD[(Iterable[be.ugent.intec.halvade.SplitIdAndRecordWithPosition], Iterable[be.ugent.intec.halvade.SplitIdAndRecordWithPosition])], header: Array[String]): RDD[(Int, Array[Byte])] = {
    val placeholders = Array[String](
      output_sam_placeholder,
      "md_" + output_bam_placeholder)
    val commands = Array[String](
      gatk4 + " MarkDuplicates --ASSUME_SORTED true --METRICS_FILE /dev/null " + //(if(cleanupTmp) "--REMOVE_DUPLICATES" else "") +
             " --INPUT " + placeholders(0) +
             " --OUTPUT " + placeholders(1))
    new org.apache.spark.MultiInstanceCogroupedPipedRDD(input, commands, placeholders, Array[String]("GATK4 MarkDuplicates"), null, tasks, header, tmp, cleanupTmp, logLevel = logLevel)
  }
  def sortAndMarkDuplicates(input: RDD[be.ugent.intec.halvade.SplitIdAndRecordWithPosition], header: Array[String]): RDD[(Int, Array[Byte])] = {
    val placeholders = Array[String](
      output_sam_placeholder,
      "md_" + output_bam_placeholder)
    val commands = Array[String](
      gatk4 + " MarkDuplicates --ASSUME_SORTED true --METRICS_FILE /dev/null " + //(if(cleanupTmp) "--REMOVE_DUPLICATES" else "") +
             " --INPUT " + placeholders(0) +
             " --OUTPUT " + placeholders(1))
    new org.apache.spark.MultiInstanceSinglePipedRDD(input, commands, placeholders, Array[String]("GATK4 MarkDuplicates"), null, tasks, header, tmp, cleanupTmp, logLevel = logLevel)
  }

  def getBqsrTable(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], table : String = null) : String = {
    if(table == null) {
      val placeholders = Array[String](
        input_sam_placeholder,
        bqsr_table_placeholder)
      val commands = Array[String](
        bindir + "samtools" + " view -h | grep -v halvade-duplicated > " + placeholders(0),
        gatk4 + " BaseRecalibrator " +
                " -I " + placeholders(0) +
                " -R " + ref +
                " --known-sites " + knownSites +
                " --add-output-vcf-command-line false " +
                " -O " + placeholders(1))
      val procNames = Array[String]("filter halvade-duplicated-reads", "GATK4 BaseRecalibrator")
      val tables = new org.apache.spark.MultiInstanceChainedCommandPipedRDD(input, commands, placeholders, procNames, regions, tasks, tmpFolder = tmp, cleanupTmp = cleanupTmp, logLevel = logLevel, streamIn = true).map(x => new String(x._2.map(_.toChar)) ) //.collect()
      // merge!
      mergeBqsrTablesInternal(tables)
    } else table
  }

  def runBqsr(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], table : String = null): RDD[(Int, Array[Byte])] = {
    val mergedTable = getBqsrTable(input, regions, table)

    val placeholders = Array[String](
      input_bam_placeholder,
      "bqsr_" + output_bam_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " index " + placeholders(0),
      gatk4 + " ApplyBQSR" +
              " -I "+  placeholders(0) +
              " -bqsr bqsr_" + bqsr_table_placeholder +
              " --add-output-vcf-command-line false" +
              " -O " + placeholders(1) + " " + gatk_region_placeholder)
    val procNames = Array[String]("samtools index", "GATK4 ApplyBQSR")
    new org.apache.spark.MultiInstanceChainedCommandPipedRDD(input, commands, placeholders, procNames, regions, tasks, tmp, cleanupTmp, extraFiles = Map("bqsr_" + bqsr_table_placeholder -> mergedTable), logLevel = logLevel)
  }

  /**
  * STRELKA
  */
  // *****************************************************************************
  // * automagically detect the latest version please, thank you very much!      *
  // *****************************************************************************
  val subfolders = getListOfSubFolders(bindir).map(x => x.getName())
  val strelkafilepattern = "[0-9]+.[0-9]+.[0-9]+".r
  val strelkas = subfolders.filter(x => x.startsWith("strelka-")) .map(x => {var result = strelkafilepattern.findFirstIn(x); result match { case Some(v) =>v case _ => ""} } ) .map(x =>x.split("\\.").map(_.toInt))
  if(strelkas.size == 0) info("no strelka2 found")
  val newestStrelka = strelkas.reduce((a,b) => {
    if (a(0) < b(0)) b else if (a(0) > b(0)) a
    else {
      if (a(1) < b(1)) b else if (a(1) > b(1)) a
      else {
        if (a(2) < b(2)) b else if (a(2) > b(2)) a
        else {
          throw new Exception("illegal comparison of strelka versions: " + a + " and " + b)
        }
      }
    }
  }).mkString(".")
  val strelka = bindir + "strelka-" + newestStrelka + ".centos6_x86_64/libexec/strelka2"
  val strelkaSNVScoringModel = bindir + "strelka-" + newestStrelka + ".centos6_x86_64/share/config/somaticSNVScoringModels.json"
  val strelkaIndelScoringModel = bindir + "strelka-" + newestStrelka + ".centos6_x86_64/share/config/somaticIndelScoringModels.json"
  val strelkaDefaultVariables = " --max-indel-size 49 --min-mapping-quality 20 --somatic-snv-rate 0.0001 --shared-site-error-rate 0.0000000005" +
                                " --shared-site-error-strand-bias-fraction 0.0 --somatic-indel-rate 0.000001 --shared-indel-error-factor 2.2" +
                                " --tier2-min-mapping-quality 0 --strelka-snv-max-filtered-basecall-frac 0.4 --strelka-snv-max-spanning-deletion-frac 0.75" +
                                " --strelka-snv-min-qss-ref 15 --strelka-indel-max-window-filtered-basecall-frac 0.3 --strelka-indel-min-qsi-ref 40" +
                                " --ssnv-contam-tolerance 0.15 --indel-contam-tolerance 0.15 --strelka-skip-header "
  val scoringModels = " --somatic-snv-scoring-model-file " + strelkaSNVScoringModel + " --somatic-indel-scoring-model-file " + strelkaIndelScoringModel
  val coverageInfo = "--strelka-max-depth-factor 3 --strelka-chrom-depth-file "

  protected final val primarychrfilter = "^(chr)?[0-9]{1,2}$|^(chr)?[xy]{1}$|^(chr)?mt?$"
  def getChromCoverage(input: RDD[(Int, Array[Byte])], dict: Map[String, DictIndexAndPosition], readLength: Int, defaultAverageCoverage: Int, normalRGIDs: String, filterNonPrimaryChromosomes: Boolean) : Map[String, Long] = {
    val placeholders = Array.empty[String]
    val commands = Array[String](
      bindir + "samtools view -F 3844 -R readgroups.txt - | grep -v halvade-duplicated | awk '{print $3}'" // also filter halvade duplicated readds
      // filter 3844:
      // 0x004 (segment unmapped), 0x100 (secondary alignment), 0x200 (not passing filters), 0x400 (PCR/optical duplicate), 0x800 (supplementary alignment)
    )
    val procNames = Array[String]("samtools view")
    val chromDepths = new org.apache.spark.MultiInstanceChainedCommandPipedRDD(input, commands, placeholders, procNames, null, tasks, tmp, cleanupTmp, extraFiles = Map("readgroups.txt" -> normalRGIDs), logLevel = logLevel, streamIn = true, streamOut = true)
                      .flatMap(x => new String(x._2.map(_.toChar)).split('\n'))
                      .countByValue.toMap

    val dictToUse = if (filterNonPrimaryChromosomes) dict.filter(x => primarychrfilter.r.findFirstMatchIn(x._1.toLowerCase) != None) else dict
    dictToUse.map(x => (x._1, (if(chromDepths.contains(x._1)) chromDepths(x._1).toLong*readLength/dict(x._1).position else defaultAverageCoverage)))
  }
  def runStrelka2(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], tumor_sm: String, normal_sm: String, averageNormalCoverage: String,
                  isExome : Boolean, filterNonPrimaryChromosomes: Boolean): RDD[String] = {
    val placeholders = Array[String](
      input_bam_placeholder,
      "tumor" + tmp_rgids_placeholder,
      "normal" + tmp_rgids_placeholder,
      "tumor" + tmp_bam_placeholder,
      "normal" + tmp_bam_placeholder,
      "snvs_" + output_vcf_placeholder,
      "indels_" + output_vcf_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " view -H " + placeholders(0) + " | grep '^@RG.*SM:" + tumor_sm + ".*' | sed 's/.*ID:\\([^\\t]*\\).*/\\1/g' > " + placeholders(1),
      bindir + "samtools" + " view -H " + placeholders(0) + " | grep '^@RG.*SM:" + normal_sm + ".*' | sed 's/.*ID:\\([^\\t]*\\).*/\\1/g' > " + placeholders(2),
      bindir + "samtools" + " view -R " + placeholders(1) + " --write-index -o " + placeholders(3) + " " + placeholders(0),
      bindir + "samtools" + " view -R " + placeholders(2) + " --write-index -o " + placeholders(4) + " " + placeholders(0),
      strelka + " " + strelka_region_placeholder +
                (if(isExome) "" else " " + coverageInfo + strelka_tsv_placeholder) +
                " --tumor-align-file " + placeholders(3) +
                " --normal-align-file "+  placeholders(4) +
                " --somatic-snv-file " + placeholders(5) +
                " --somatic-indel-file  " + placeholders(6) +
                " --ref " + ref +
                strelkaDefaultVariables + scoringModels,
      "cat " + placeholders(5) + " " + placeholders(6)
      )
    val procNames = Array[String]("samtools viewtumorRGS", "samtools viewnormalRGS", "samtools viewtumor", "samtools viewnormal", "Strelka2 run", "merge vcfs")
    val filteredDict = if(filterNonPrimaryChromosomes) dict.filter(x => primarychrfilter.r.findFirstMatchIn(x._1.toLowerCase) != None).map(_._1).toSet else dict.map(_._1).toSet
    new org.apache.spark.MultiInstanceStrelkaPipedRDD(input, commands, placeholders, procNames, regions, filteredDict, tasks, tmp, cleanupTmp, extraFiles = Map.empty, logLevel = logLevel, averageNormalCoverage = averageNormalCoverage, streamOut = true)
          .flatMap(x => new String(x._2.map(_.toChar)).split('\n').filter(!_.isEmpty) )
  }
  def runBqsrAndStrelka2(input: RDD[(Int, Array[Byte])], regions: Map[PartitionSplit, Array[String]], dict: Map[String, DictIndexAndPosition], tumor_sm: String, normal_sm: String, averageNormalCoverage: String,
                          isExome : Boolean, filterNonPrimaryChromosomes: Boolean, table : String = null): RDD[String] = {
    val mergedTable = getBqsrTable(input, regions, table)

    val placeholders = Array[String](
      input_bam_placeholder,
      "bqsr_" + output_bam_placeholder,
      "tumor" + tmp_rgids_placeholder,
      "normal" + tmp_rgids_placeholder,
      "tumor" + tmp_bam_placeholder,
      "normal" + tmp_bam_placeholder,
      "snvs_" + output_vcf_placeholder,
      "indels_" + output_vcf_placeholder)
    val commands = Array[String](
      bindir + "samtools" + " index " + placeholders(0),
      gatk4 + " ApplyBQSR" +
              " -I "+  placeholders(0) +
              " -bqsr bqsr_" + bqsr_table_placeholder +
              " --add-output-vcf-command-line false" +
              " -O " + placeholders(1) + " " + gatk_region_placeholder,
      bindir + "samtools" + " view -H " + placeholders(1) + " | grep '^@RG.*SM:" + tumor_sm + ".*' | sed 's/.*ID:\\([^\\t]*\\).*/\\1/g' > " + placeholders(2),
      bindir + "samtools" + " view -H " + placeholders(1) + " | grep '^@RG.*SM:" + normal_sm + ".*' | sed 's/.*ID:\\([^\\t]*\\).*/\\1/g' > " + placeholders(3),
      bindir + "samtools" + " view -R " + placeholders(2) + " --write-index -o " + placeholders(4) + " " + placeholders(1),
      bindir + "samtools" + " view -R " + placeholders(3) + " --write-index -o " + placeholders(5) + " " + placeholders(1),
      strelka + " " + strelka_region_placeholder +
                (if(isExome) "" else " " + coverageInfo + strelka_tsv_placeholder) +
                " --tumor-align-file " + placeholders(4) +
                " --normal-align-file "+  placeholders(5) +
                " --somatic-snv-file " + placeholders(6) +
                " --somatic-indel-file  " + placeholders(7) +
                " --ref " + ref +
                strelkaDefaultVariables + scoringModels,
      "cat " + placeholders(6) + " " + placeholders(7)
      )
    val filteredDict = if(filterNonPrimaryChromosomes) dict.filter(x => primarychrfilter.r.findFirstMatchIn(x._1.toLowerCase) != None).map(_._1).toSet else dict.map(_._1).toSet
    val procNames = Array[String]("samtools index", "GATK4 ApplyBQSR", "samtools viewtumorRGS", "samtools viewnormalRGS", "samtools viewtumor", "samtools viewnormal", "Strelka2 run", "merge vcfs")
    new org.apache.spark.MultiInstanceStrelkaPipedRDD(input, commands, placeholders, procNames, regions, filteredDict, tasks, tmp, cleanupTmp, extraFiles = Map("bqsr_" + bqsr_table_placeholder -> mergedTable), logLevel = logLevel, averageNormalCoverage = averageNormalCoverage, streamOut = true)
          .flatMap(x => new String(x._2.map(_.toChar)).split('\n').filter(!_.isEmpty) )
  }

  /**
  * ELPREP
  */
  def elprep(input: RDD[(Int, Array[Byte])], markDuplicates: Boolean, sites: String = null, elfasta: String = null): RDD[(Int, Array[Byte])] = {
    val elprep = bindir + "elprep"
    val metrics = false
    val bqsr = sites == null
    if (! (markDuplicates || bqsr) ) throw new IllegalArgumentException("should at least be one filter enabled");
    val placeholders = Array[String](
      input_bam_placeholder,
      output_bam_placeholder)
      // ./elprep $input $output --mark-duplicates --sorting-order keep --nr-of-threads $t
      // samtools sam to bam!! not needed if bam output!
    val commands = Array[String](
      elprep + " filter " +
              " " + placeholders(0)  +
              " " + placeholders(1) +
              " --sorting-order keep" +
              " --nr-of-threads " + threads +
              (if (markDuplicates) " --mark-duplicates" else "") +
              (if (bqsr) (" --bqsr /dev/null --known-sites " + sites + "  --bqsr-reference " + elfasta) else "") )
      val procNames = Array[String]("elprep")
      new org.apache.spark.MultiInstanceChainedCommandPipedRDD(input, commands, placeholders, procNames, null, 1, tmp, cleanupTmp, Map(), Map("HOME" -> tmp))

  }

  /**
  * BWA
  */
  val RGID_PLACEHOLDER = "RGID_PLACEHOLDER"
  val RGSM_PLACEHOLDER = "RGSM_PLACEHOLDER"
  def bwamem(input: String, sample: String, platform: String, sc: SparkContext): (Array[String], Boolean, RDD[String]) = {
    val bwacommand : Seq[String] = Seq(bindir + "bwa", "mem", "-t", threads.toString, "-R", "@RG\\tID:" + RGID_PLACEHOLDER + "\\tSM:" + RGSM_PLACEHOLDER + "\\tPL:" + platform, "-p", ref, "/dev/stdin")
    val conf_ = new Configuration(sc.hadoopConfiguration)
    var fastq: RDD[String] = null;
    fastq = readFastQ(input, sc, conf_)
    (createReadGroupLines(getReadGroupIdsBasedOnFolder(fastq), sample, platform), false, new org.apache.spark.StringPipedRDD(fastq, bwacommand.map(_.replace(RGSM_PLACEHOLDER, sample)), "bwa mem", logLevel = this.logLevel))
  }

  /**
  * SAMTOOLS
  */
  def samtoolsViewToBam(input: RDD[(Int, ArrayBuffer[String])]): RDD[(Int, Array[Byte])] = {
    val placeholders = Array[String]()
    val commands = Array[String](bindir + "samtools view -b -" )
    val procNames = Array[String]("samtools view")
    new org.apache.spark.MultiInstanceChainedCommandPipedRDD(input, commands, placeholders, procNames, null, tasks, tmp, cleanupTmp, logLevel = logLevel, streamIn = true, streamOut = true)
  }
  def sortAndConvertToBam(input: RDD[be.ugent.intec.halvade.SplitIdAndRecordWithPosition], header: Array[String]): RDD[(Int, Array[Byte])] = {
    val placeholders = Array[String](output_sam_placeholder)
    val commands = Array[String](bindir + "samtools" + " view -b " + placeholders(0))
    new org.apache.spark.MultiInstanceSinglePipedRDD(input, commands, placeholders, Array[String]("Samtools view"), null, tasks, header, tmp, cleanupTmp, streamOut = true)
  }
  def sortAndConvertToBamPaired(input: RDD[(Iterable[be.ugent.intec.halvade.SplitIdAndRecordWithPosition], Iterable[be.ugent.intec.halvade.SplitIdAndRecordWithPosition])], header: Array[String]): RDD[(Int, Array[Byte])] = {
    val placeholders = Array[String](output_sam_placeholder)
    val commands = Array[String](bindir + "samtools" + " view -b " + placeholders(0))
    new org.apache.spark.MultiInstanceCogroupedPipedRDD(input, commands, placeholders, Array[String]("Samtools view"), null, tasks, header, tmp, cleanupTmp, streamOut = true)
  }

  /**
  * GENERAL FUNCTIONS
  */
  protected def mergeBqsrTablesInternal(input: RDD[String]) : String = {
    return input.filter(_.size != 0).map(x => BqsrReport.readBqsrReport(x.split("\n"))).treeReduce((a, b) => a.combine(b), scala.math.log(input.partitions.length).toInt + 1).createString // about same performance but is needed if too many partitions, else can be a memory problem in driver
  }


  protected def tokenize(command: String): Seq[String] = {
    val buf = new ListBuffer[String]
    val tok = new StringTokenizer(command)
    while(tok.hasMoreElements) {
      buf += tok.nextToken()
    }
    buf
  }
  protected def getListOfFiles(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }
  protected def getListOfSubFolders(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isDirectory).toList
    } else {
      List[File]()
    }
  }
  protected def readFastQ(input: String, sc: SparkContext, conf_ : Configuration): RDD[String] = { // can read folders inside the folder, which indicate readgroup name
    val fs: FileSystem = FileSystem.get(new URI(input), conf_);
    //the second boolean parameter here sets the recursion to true
    val ls = fs.listStatus(new Path(input))
    if (ls.filter(_.isDirectory).nonEmpty) {
      sc.newAPIHadoopFile(ls.filter(_.isDirectory).map(_.getPath).mkString(","), classOf[UnsplittableTextInputFormat], classOf[LongWritable], classOf[Text], conf_).map(x => x._2.toString)
    } else {
      sc.newAPIHadoopFile(input, classOf[UnsplittableTextInputFormat], classOf[LongWritable], classOf[Text], conf_).map(x => x._2.toString)
    }
  }
}
