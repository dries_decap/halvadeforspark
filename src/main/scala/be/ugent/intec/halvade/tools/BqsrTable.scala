package be.ugent.intec.halvade.tools

import be.ugent.intec.halvade.Logging
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Map
import scala.io.Source
import java.io.{PrintWriter, File}
import scala.collection.mutable.{TreeSet, LinkedHashSet}

object CONSTANTS {
  final val SMOOTHING_CONSTANT : Int = 1
  final val RESOLUTION_BINS_PER_QUAL : Double = 1.0
  final val MAX_GATK_USABLE_Q_SCORE : Int = 40
  final val MAX_REASONABLE_Q_SCORE : Int = 60
  final val MAX_QUAL : Int = 254
  final val MAX_SAM_QUAL_SCORE : Int = 93
  final val MAX_RECALIBRATED_Q_SCORE : Int = 93
  final val MIN_USABLE_Q_SCORE : Int = 6
  final val MAX_NUMBER_OF_OBSERVATIONS : Double = Int.MaxValue - 1
  final val TABLE_REGEX_SEPARATOR = "\\s+"
  final val SEPARATOR = ":"
  final val HEADER_FIELD_NAME = 2
  final val HEADER_FIELD_DESCRIPTION = 3
  final val HEADER_FIELD_NUMBER_OF_TABLES = 2
  final val HEADER_FIELD_COLUMNS = 2
  final val HEADER_FIELD_ROWS = 3
}

@SerialVersionUID(1100L)
class BqsrReport(val ntables: Int, val rac: RecalibrationArgumentCollection, var tables: RecalibrationTables) extends Serializable with Logging {
  val tableHeader = "#:GATKReport.v1.1:" + ntables + "\n"
  def combine(that: BqsrReport) : BqsrReport =  {
    this.tables.combine(that.tables)
    return this
  }

  def writeToFile(filename: String) {
    val pw = new PrintWriter(new File(filename))
    pw.write(this.createString)
    pw.close
  }
  def createString() : String = {
    val quantizationInfo: QuantizationInfo = new QuantizationInfo();
    quantizationInfo.initialize(tables, rac.quantizingLevels)
    return tableHeader + rac.toString + quantizationInfo.toString + tables.toString
  }
  override def toString() : String = {
    return "BqsrReport with " + ntables + " tables"
  }
}

object BqsrReport {
  def readBqsrReport(filename: String) : BqsrReport = {
    return readBqsrReport(Source.fromFile(filename).getLines.toArray)
  }

  def readBqsrReport(lines: Array[String]) : BqsrReport = {
      val ntables = lines(0).split(CONSTANTS.SEPARATOR)(CONSTANTS.HEADER_FIELD_NUMBER_OF_TABLES).toInt
      var i = 1
      var rac = new RecalibrationArgumentCollection()
      var tables = new RecalibrationTables()
      var quantizedInfo = new QuantizationInfo()
      // System.err.println("# tables: " + ntables)
      for (tableidx <- 0 until ntables) {
          val data = lines(i).split(CONSTANTS.SEPARATOR)
          i+= 1
          val namedata = lines(i).split(CONSTANTS.SEPARATOR)
          i+= 1
          // System.err.println("data: " + data.mkString(" "))
          // System.err.println("namedata: " + namedata.mkString(" "))
          val name = namedata(CONSTANTS.HEADER_FIELD_NAME)
          val description = if(namedata.length > CONSTANTS.HEADER_FIELD_DESCRIPTION) namedata(CONSTANTS.HEADER_FIELD_DESCRIPTION) else ""
          val nColumns = data(CONSTANTS.HEADER_FIELD_COLUMNS).toInt
          val nRows = data(CONSTANTS.HEADER_FIELD_ROWS).toInt
          // System.err.println("reading table " + name + ": "  + description)
          val columnNames = lines(i).split(CONSTANTS.TABLE_REGEX_SEPARATOR)
          i+= 1
          val rows = new ArrayBuffer[String]()
          for (k <- 0 until nRows) {
            rows += lines(i)
            i+= 1
          }
          if (name == "Arguments") rac.initialize(rows)
          // else if (name == "Quantized") quantizedInfo.initialize(rows)
          else if (name == "RecalTable0") tables.parseReadGroupTable(rows)
          else if (name == "RecalTable1") tables.parseQualityScoreTable(rows)
          else if (name == "RecalTable2") tables.parseAllCovariatesTable(rows)
          i += 1 // empty line
      }
      return new BqsrReport(ntables, rac, tables)
  }
}
object BqsrRowData {
   var log10QempPriorCache = new ArrayBuffer[Double]()
   val mu = 0.0 // mean
   val sigma = 0.5 // sigma
   val norm = 0.9 // normalizing factor
   for (i <- 0 until CONSTANTS.MAX_GATK_USABLE_Q_SCORE + 1) {
     val gaussval = norm * scala.math.exp(-scala.math.pow(i-mu, 2) / (2*scala.math.pow(sigma, 2)))
     var log10Prior = Double.MinValue
     if (gaussval != 0.0) {
       log10Prior = scala.math.log10(gaussval)
     }
     log10QempPriorCache += log10Prior
   }
   def bayesianEstimateOfEmpiricalQuality(nObservations: Long, nErrors: Long, QReported: Double) : Double = {
     val numBins : Int = (CONSTANTS.MAX_REASONABLE_Q_SCORE + 1 ) * CONSTANTS.RESOLUTION_BINS_PER_QUAL.toInt
     val log10Posteriors = new ArrayBuffer[Double]()
     for (bin <- 0 until numBins){
       val QEmpOfBin = bin / CONSTANTS.RESOLUTION_BINS_PER_QUAL
       val log10Posterior = BqsrRowData.log10QempPrior(QEmpOfBin, QReported) + BqsrRowData.log10QempLikelihood(QEmpOfBin, nObservations, nErrors)
       log10Posteriors += log10Posterior
     }
     val MLEbin = log10Posteriors.indexOf(log10Posteriors.max)
     return MLEbin / CONSTANTS.RESOLUTION_BINS_PER_QUAL
   }
   def log10QempPrior(Qempirical: Double, Qreported: Double) : Double = {
     val difference : Int = scala.math.min(scala.math.abs((Qempirical - Qreported).toInt), CONSTANTS.MAX_GATK_USABLE_Q_SCORE)
     return BqsrRowData.log10QempPriorCache(difference)
   }

   def log10QempLikelihood(Qempirical: Double, nObservations_ : Long, nErrors_ : Long) : Double = {
     if(nObservations_ == 0) return 0.0
     var nErrors = nErrors_
     var nObservations = nObservations_
     if (nObservations_ > CONSTANTS.MAX_NUMBER_OF_OBSERVATIONS){
       val fraction : Double = CONSTANTS.MAX_NUMBER_OF_OBSERVATIONS / nObservations_
       nErrors = (scala.math.round(nErrors * fraction)).toLong
       nObservations = CONSTANTS.MAX_NUMBER_OF_OBSERVATIONS.toInt
     }

     var log10Prob = QualityUtils.log10BinomialProbability(nObservations.toInt, nErrors.toInt, QualityUtils.qualToErrorProbLog10(Qempirical))
     if ( log10Prob.isInfinite || log10Prob.isNaN) {
       log10Prob = Double.MinValue
     }
     return log10Prob
   }
}

object QualityUtils {
  val qualToErrorProbCache : Array[Double] = Array.fill(CONSTANTS.MAX_QUAL + 1)(0)

  for (i <- 0 to CONSTANTS.MAX_QUAL) {
      qualToErrorProbCache(i) = qualToErrorProb(i.toDouble);
  }
  def qualToErrorProb(qual: Double) : Double = {
    if ( qual < 0.0) throw new Exception("qual must be >= 0.0: " + qual)
    return scala.math.pow(10.0, qual / -10.0)
  }

  def qualToErrorProb(qual: Byte) : Double = {
    return qualToErrorProbCache(qual.toInt & 0xFF); // Map: 127 -> 127; -128 -> 128; -1 -> 255; etc.
  }

  def qualToErrorProbLog10(qual: Double) : Double = {
    if ( qual < 0.0) throw new Exception("qual must be >= 0.0: " + qual)
    return qual * -0.1
  }

  def log10Factorial(n: Int) : Double = {
    return org.apache.commons.math3.special.Gamma.logGamma(n+1) * scala.math.log10(scala.math.E)
  }

  def log10BinomialCoefficient(n: Int, k: Int) : Double = {
    if(n< 0) throw new Exception("Must have non-negative number of trials: " + n)
    if(k > n || k < 0) throw new Exception("k: Must have non-negative number of successes, and no more successes than number of trials: " + k)
    return QualityUtils.log10Factorial(n) - QualityUtils.log10Factorial(k) - QualityUtils.log10Factorial(n - k)
  }

  def log10BinomialProbability(n: Int, k: Int, log10p: Double) : Double = {
    if (log10p >= 1.0e-18) throw new Exception("log10p: Log10-prob must be 0 or less")
    if(log10p == Double.NegativeInfinity) {
      if (k == 0) return 0
      else return Double.NegativeInfinity
    } else if (log10p == 0.0) {
      if (k == n) return 0
      else return Double.NegativeInfinity
    }
    val log10OneMinusP = scala.math.log10(1 - scala.math.pow(10.0, log10p))
    return QualityUtils.log10BinomialCoefficient(n, k) + log10p * k + log10OneMinusP * (n - k)
  }
  def errorProbToQual(errorRate: Double) : Byte = {
    return errorProbToQual(errorRate, CONSTANTS.MAX_SAM_QUAL_SCORE.toByte)
  }
  def errorProbToQual(errorRate: Double, maxQual: Byte) : Byte = {
      if(!isValidProbability(errorRate)) throw new Error("errorRate must be good probability but got " + errorRate);
      val d: Double = scala.math.round(-10.0*scala.math.log10(errorRate));
      return boundQual(d.toInt, maxQual);
  }
  def boundQual(qual: Int, maxQual: Byte) : Byte = {
    return (scala.math.max(scala.math.min(qual, maxQual & 0xFF), 1) & 0xFF).toByte;
  }
  def isValidProbability(x: Double) : Boolean = {
    return x >= 0.0 && x <= 1.0;
  }
}

@SerialVersionUID(1101L)
class BqsrRowData(var numObservations: Long, var numMismatches: Double, var estimatedQReported: Double) {
  var empiricalQuality = -1.0
  var multiplier =  100000.0
  numMismatches = numMismatches * multiplier

  def increment(newObservations: Long, newMismatches: Double) {
    this.numObservations += newObservations
    this.numMismatches += newMismatches*multiplier
    this.empiricalQuality = -1.0
  }

  def calcExpectedErrors() : Double = {
    return this.numObservations * QualityUtils.qualToErrorProb(this.estimatedQReported)
  }

  def combine(other: BqsrRowData) {
    var sumErrors = this.calcExpectedErrors() + other.calcExpectedErrors()
    increment(other.numObservations, other.getNumMismatches)
    this.estimatedQReported = -10 * scala.math.log10(sumErrors / this.numObservations)
    this.empiricalQuality = -1.0
  }

  def getEmpiricalErrorRate() : Double = {
    if (this.numObservations == 0) return 0.0
    else {
      val doubleMismatches: Double = (this.numMismatches / this.multiplier) + CONSTANTS.SMOOTHING_CONSTANT
      val doubleObservations: Double = this.numObservations + CONSTANTS.SMOOTHING_CONSTANT + CONSTANTS.SMOOTHING_CONSTANT
      return doubleMismatches / doubleObservations
    }
  }

  def getNumMismatches() : Double = {
     return this.numMismatches / this.multiplier
  }

  def calcEmpiricalQuality(conditionalPrior: Double) {
    val mismatches : Long = (this.getNumMismatches() + 0.5).toLong + CONSTANTS.SMOOTHING_CONSTANT
    val observations : Long = this.numObservations + CONSTANTS.SMOOTHING_CONSTANT + CONSTANTS.SMOOTHING_CONSTANT

    val empiricalQual = BqsrRowData.bayesianEstimateOfEmpiricalQuality(observations, mismatches, conditionalPrior)
    this.empiricalQuality = math.min(empiricalQual, CONSTANTS.MAX_RECALIBRATED_Q_SCORE)
  }

  def getEmpiricalQuality() : Double =  {
    return getEmpiricalQuality(this.estimatedQReported)
  }

  def getEmpiricalQuality(conditionalPrior: Double) : Double = {
    if(this.empiricalQuality == -1.0) {
      calcEmpiricalQuality(conditionalPrior)
    }
    return this.empiricalQuality
  }
}

final case class ReportTable0Key(readGroup: String, eventType: String) extends Ordered[ReportTable0Key] {
  def compare(that: ReportTable0Key): Int = {
    if(this.readGroup == that.readGroup) return this.eventType.compare(that.eventType)
    else return this.readGroup.compare(that.readGroup)
  }
  override def toString() : String = "readGroupIdx " + readGroup + ", eventType: " + eventType
}
final case class ReportTable1Key(readGroup: String, eventType: String, qualityScore: String) extends Ordered[ReportTable1Key] {
  def compare(that: ReportTable1Key): Int = {
    if(this.readGroup == that.readGroup)
      if (this.qualityScore == that.qualityScore) return this.eventType.compare(that.eventType)
      else return this.qualityScore.toInt.compare(that.qualityScore.toInt)
    else return this.readGroup.compare(that.readGroup)
  }
  override def toString() : String = "readGroupIdx " + readGroup + ", eventType: " + eventType
}
final case class ReportTable2Key(readGroup: String, eventType: String, qualityScore: String, covariateValue: String, covariateName: String) extends Ordered[ReportTable2Key] {
  def compare(that: ReportTable2Key): Int = {
    if(this.readGroup == that.readGroup)
      if (this.qualityScore == that.qualityScore)
        if(this.covariateValue == that.covariateValue)
          if(this.covariateName == that.covariateName) return this.eventType.compare(that.eventType)
          else return this.covariateName.compare(that.covariateName)
        else return this.covariateValue.compare(that.covariateValue)
      else return this.qualityScore.toInt.compare(that.qualityScore.toInt)
    else return this.readGroup.compare(that.readGroup)
  }
  override def toString() : String = "readGroupIdx " + readGroup + ", eventType: " + eventType
}
@SerialVersionUID(1102L)
class RecalibrationTables() {
  val readGroupTable = Map.empty[ReportTable0Key, BqsrRowData]
  val qualityScoreTable = Map.empty[ReportTable1Key, BqsrRowData]
  val allCovariatesTable = Map.empty[ReportTable2Key, BqsrRowData]

  def parseReadGroupTable(rows: ArrayBuffer[String]) {
    for (i <- 0 until rows.size) {
      val split = rows(i).split(CONSTANTS.TABLE_REGEX_SEPARATOR)
      val rowdata = new BqsrRowData(split(4).toLong, split(5).toDouble, split(3).toDouble)
      this.readGroupTable(ReportTable0Key(split(0), split(1))) = rowdata
    }
  }

  def parseQualityScoreTable(rows: ArrayBuffer[String]) {
    for (i <- 0 until rows.size) {
      val split = rows(i).split(CONSTANTS.TABLE_REGEX_SEPARATOR)
      val rowdata = new BqsrRowData(split(4).toLong, split(5).toDouble, split(1).toDouble)
      this.qualityScoreTable(ReportTable1Key(split(0), split(2), split(1))) = rowdata
    }
  }

  def parseAllCovariatesTable(rows: ArrayBuffer[String]) {
    for (i <- 0 until rows.size) {
      val split = rows(i).split(CONSTANTS.TABLE_REGEX_SEPARATOR)
      val rowdata = new BqsrRowData(split(6).toLong, split(7).toDouble, split(1).toDouble)
      this.allCovariatesTable(ReportTable2Key(split(0), split(4), split(1), split(2), split(3))) = rowdata
    }
  }

  def combine(that: RecalibrationTables) {
    for ((key, value) <- that.readGroupTable) {
      if(this.readGroupTable.contains(key)) {
        this.readGroupTable(key).combine(value)
      } else {
        this.readGroupTable(key) = value
      }
    }
    for ((key, value) <- that.qualityScoreTable) {
      if(this.qualityScoreTable.contains(key)) {
        this.qualityScoreTable(key).combine(value)
      } else {
        this.qualityScoreTable(key) = value
      }
    }
    for ((key, value) <- that.allCovariatesTable) {
      if(this.allCovariatesTable.contains(key)) {
        this.allCovariatesTable(key).combine(value)
      } else {
        this.allCovariatesTable(key) = value
      }
    }
  }
  override def toString() : String = {
    val rgcolsize = scala.math.max("ReadGroup".length, readGroupTable.map(x => x._1.readGroup.length).max) + 2
    val table0headerline1 = "#:GATKTable:6:ROWS:%s:%s:%.4f:%.4f:%d:%.2f:;"
    val table0headerline2 = "#:GATKTable:RecalTable0:"
    val table0columnNames = Array("ReadGroup", "EventType", "EmpiricalQuality", "EstimatedQReported", "Observations", "Errors");
    val table0columnWidths =
      Array(scala.math.max(table0columnNames(0).length, readGroupTable.map(x => x._1.readGroup.length).max),
            scala.math.max(table0columnNames(1).length, readGroupTable.map(x => x._1.eventType.length).max) + 2,
            scala.math.max(table0columnNames(2).length, readGroupTable.map(x => "%.4f".format(x._2.getEmpiricalQuality).length).max) + 2,
            scala.math.max(table0columnNames(3).length, readGroupTable.map(x => "%.4f".format(x._2.estimatedQReported).length).max) + 2,
            scala.math.max(table0columnNames(4).length, readGroupTable.map(x => "%d".format(x._2.numObservations).length).max) + 2,
            scala.math.max(table0columnNames(5).length, readGroupTable.map(x => "%.2f".format(x._2.getNumMismatches).length).max))

    val table0headerformatstring = "%-" + (table0columnWidths(0) + 2)  + "s%-" + table0columnWidths(1) + "s%-" + table0columnWidths(2) + "s%-" + table0columnWidths(3) + "s%-" + table0columnWidths(4) + "s%-" + table0columnWidths(5) + "s"
    val table0formatstring = "%-" + table0columnWidths(0) + "s  %-" + (table0columnWidths(1) - 2) + "s%" + table0columnWidths(2) + ".4f%" + table0columnWidths(3) + ".4f%" + table0columnWidths(4) + "d%" + (table0columnWidths(5) + 2) + ".2f"

    val table1headerline1 = "#:GATKTable:6:ROWS:%s:%d:%s:%.4f:%d:%.2f:;"
    val table1headerline2 = "#:GATKTable:RecalTable1:"
    val table1columnNames = Array("ReadGroup", "QualityScore", "EventType", "EmpiricalQuality", "Observations", "Errors");
    val table1columnWidths =
      Array(scala.math.max(table1columnNames(0).length, qualityScoreTable.map(x => x._1.readGroup.length).max),
            scala.math.max(table1columnNames(1).length, qualityScoreTable.map(x => x._1.qualityScore.length).max) + 2,
            scala.math.max(table1columnNames(2).length, qualityScoreTable.map(x => x._1.eventType.length).max) + 2,
            scala.math.max(table1columnNames(3).length, qualityScoreTable.map(x => "%.4f".format(x._2.getEmpiricalQuality(x._1.qualityScore.toInt)).length).max) + 2,
            scala.math.max(table1columnNames(4).length, qualityScoreTable.map(x => "%d".format(x._2.numObservations).length).max) + 2,
            scala.math.max(table1columnNames(5).length, qualityScoreTable.map(x => "%.2f".format(x._2.getNumMismatches).length).max))

    val table1headerformatstring = "%-" + (table1columnWidths(0) + 2)  + "s%-" + table1columnWidths(1) + "s%-" + table1columnWidths(2) + "s%-" + table1columnWidths(3) + "s%-" + table1columnWidths(4) + "s%-" + table1columnWidths(5) + "s"
    val table1formatstring = "%-" + table1columnWidths(0) + "s%" + table1columnWidths(1) + "d  %-" + (table1columnWidths(2) - 2) + "s%" + table1columnWidths(3) + ".4f%" + table1columnWidths(4) + "d%" + (table1columnWidths(5) + 2) + ".2f"


    val table2headerline1 = "#:GATKTable:8:ROWS:%s:%d:%s:%s:%s:%.4f:%d:%.2f:;"
    val table2headerline2 = "#:GATKTable:RecalTable2:"
    val table2columnNames = Array("ReadGroup", "QualityScore", "CovariateValue", "CovariateName", "EventType", "EmpiricalQuality", "Observations", "Errors");
    val table2columnWidths =
      Array(scala.math.max(table2columnNames(0).length, allCovariatesTable.map(x => x._1.readGroup.length).max),
            scala.math.max(table2columnNames(1).length, allCovariatesTable.map(x => x._1.qualityScore.length).max) + 2,
            scala.math.max(table2columnNames(2).length, allCovariatesTable.map(x => x._1.covariateValue.length).max) + 2,
            scala.math.max(table2columnNames(3).length, allCovariatesTable.map(x => x._1.covariateName.length).max) + 2,
            scala.math.max(table2columnNames(4).length, allCovariatesTable.map(x => x._1.eventType.length).max) + 2,
            scala.math.max(table2columnNames(5).length, allCovariatesTable.map(x => "%.4f".format(x._2.getEmpiricalQuality(x._1.qualityScore.toInt)).length).max) + 2,
            scala.math.max(table2columnNames(6).length, allCovariatesTable.map(x => "%d".format(x._2.numObservations).length).max) + 2,
            scala.math.max(table2columnNames(7).length, allCovariatesTable.map(x => "%.2f".format(x._2.getNumMismatches).length).max))

    val table2headerformatstring = "%-" + (table2columnWidths(0) + 2)  + "s%-" + table2columnWidths(1) + "s%-" + table2columnWidths(2) + "s%-" + table2columnWidths(3) + "s%-" + table2columnWidths(4) + "s%-" + table2columnWidths(5) + "s%-" + table2columnWidths(6) + "s%-" + table2columnWidths(7) + "s"
    val table2formatstring = "%-" + table2columnWidths(0) + "s%" + table2columnWidths(1) + "d  %-" + (table2columnWidths(2) - 2) + "s  %-" + (table2columnWidths(3) - 2) + "s  %-" + (table2columnWidths(4) - 2) + "s%" + table2columnWidths(5) + ".4f%" + table2columnWidths(6) + "d%" + (table2columnWidths(7) + 2) + ".2f"
    return table0headerline1.replace("ROWS", readGroupTable.size.toString) + "\n" +
           table0headerline2 + "\n" +
           table0headerformatstring.format(table0columnNames(0), table0columnNames(1), table0columnNames(2),
                    table0columnNames(3), table0columnNames(4), table0columnNames(5)) + "\n" +
           readGroupTable.map(x => {
             table0formatstring.format(x._1.readGroup, x._1.eventType, x._2.getEmpiricalQuality, x._2.estimatedQReported, x._2.numObservations, x._2.getNumMismatches())
           }).mkString("\n") +
           "\n\n" +
           table1headerline1.replace("ROWS", qualityScoreTable.size.toString) + "\n" +
           table1headerline2 + "\n" +
           table1headerformatstring.format(table1columnNames(0), table1columnNames(1), table1columnNames(2),
                     table1columnNames(3), table1columnNames(4), table1columnNames(5)) + "\n" +
           qualityScoreTable.toSeq.sortBy(x => x._1).map(x => {
             table1formatstring.format(x._1.readGroup, x._1.qualityScore.toInt, x._1.eventType, x._2.getEmpiricalQuality(x._1.qualityScore.toInt), x._2.numObservations, x._2.getNumMismatches())
           }).mkString("\n") +
           "\n\n" +
           table2headerline1.replace("ROWS", allCovariatesTable.size.toString) + "\n" +
           table2headerline2 + "\n" +
           table2headerformatstring.format(table2columnNames(0), table2columnNames(1), table2columnNames(2),
                     table2columnNames(3), table2columnNames(4), table2columnNames(5),
                     table2columnNames(6), table2columnNames(7)) + "\n" +
           allCovariatesTable.toSeq.sortBy(x => x._1).map(x => {
             table2formatstring.format(x._1.readGroup, x._1.qualityScore.toInt, x._1.covariateValue, x._1.covariateName, x._1.eventType, x._2.getEmpiricalQuality(x._1.qualityScore.toInt), x._2.numObservations, x._2.getNumMismatches())
           }).mkString("\n") +
           "\n\n";
  }
}

@SerialVersionUID(1103L)
class QuantizationInfo() {
  val headerline1 = "#:GATKTable:3:ROWS:%d:%d:%d:;"
  val headerline2 = "#:GATKTable:Quantized:Quality quantization map"
  val columnNames = Array("QualityScore", "Count", "QuantizedScore");
  val header = "\n\n"
  var quals : Array[Byte] = Array.fill(CONSTANTS.MAX_SAM_QUAL_SCORE + 1)(0)
  var counts : Array[Long]  = Array.fill(CONSTANTS.MAX_SAM_QUAL_SCORE + 1)(0L)
  var quantizationLevels = 0

  private def quantizeQualityScores(nLevels: Int) {
      val quantizer = new QualQuantizer(counts, nLevels, CONSTANTS.MIN_USABLE_Q_SCORE); // quantize the qualities to the desired number of levels
      quals = quantizer.getOriginalToQuantizedMap; // map with the original to quantized qual map (using the standard number of levels in the RAC)
  }

  def initialize(tables: RecalibrationTables, quantizationLevels: Int) {
    for (bqsrRowData <- tables.qualityScoreTable.values) {
      val empiricalQual = math.round(bqsrRowData.getEmpiricalQuality).toInt // convert the empirical quality to an integer ( it is already capped by MAX_QUAL )
      counts(empiricalQual) += bqsrRowData.numObservations // add the number of observations for every key
    }
    quantizeQualityScores(quantizationLevels);
    this.quantizationLevels = quantizationLevels;
  }

  override def toString() : String = {

    val columnWidths =
      Array(scala.math.max(columnNames(0).length, "%d".format(quals.size).length),
            scala.math.max(columnNames(1).length, counts.map(x => "%d".format(x).length).max) + 2,
            scala.math.max(columnNames(2).length, quals.map(x => "%d".format(x).length).max))
    val headerformatstring = "%-" + (columnWidths(0) + 2) + "s%-" + columnWidths(1) + "s%-" + columnWidths(2) + "s"
    val formatstring = "%" + columnWidths(0) + "d%" + columnWidths(1) + "d%" + (columnWidths(2) + 2) + "d"
    return headerline1.replace("ROWS", quals.size.toString) + "\n" +
           headerline2 + "\n" +
           headerformatstring.format(columnNames(0), columnNames(1), columnNames(2)) + "\n" +
           (0 until quals.size).map(i => formatstring.format(i, this.counts(i), this.quals(i)) ).mkString("\n") +
           "\n\n";
  }
}


@SerialVersionUID(1104L)
class QualQuantizer(val nObservationsPerQual: Array[Long], val nLevels: Int, val minInterestingQual: Int) {
  // sanity checks
  if (minInterestingQual < 0) throw new Error("minInteresingQual must be >= 0: " + minInterestingQual)
  if (nLevels < 0) throw new Error("nLevels must be >= 0: " + nLevels)
  if (nObservationsPerQual.min  < 0) throw new Error("Quality score histogram has negative values at: " +  nObservationsPerQual.mkString("/"))
  var quantizedIntervals : TreeSet[QualInterval] = quantize();
  var originalToQuantizedMap : Array[Byte] = intervalsToMap(quantizedIntervals)

  def quantize() : TreeSet[QualInterval] = {
    var intervals = new TreeSet[QualInterval]()

    for ( qStart <- 0 until nObservationsPerQual.size) {
        val nObs: Long = nObservationsPerQual(qStart);
        val errorRate: Double = QualityUtils.qualToErrorProb(qStart.toByte);
        val nErrors: Double = nObs * errorRate;
        val qi: QualInterval = new QualInterval(minInterestingQual, qStart, qStart, nObs, scala.math.floor(nErrors).toInt, 0, qStart.toByte);
        intervals += qi;
    }

    // greedy algorithm:
    // while ( n intervals >= nLevels ):
    //   find intervals to merge with least penalty
    //   merge it
    while ( intervals.size > nLevels ) {
       mergeLowestPenaltyIntervals(intervals);
    }
    return intervals
  }

  def mergeLowestPenaltyIntervals(intervals: TreeSet[QualInterval]) { //} :  ArrayBuffer[QualInterval] = {

    // setup the iterators
    val it1 = intervals.iterator;
    val it1p = intervals.iterator;
    it1p.next(); // skip one

    // walk over the pairs of left and right, keeping track of the pair with the lowest merge penalty
    var minMerge: QualInterval = null;
    // System.err.println("mergeLowestPenaltyIntervals: " + intervals.size);
    var lastMergeOrder : Int = 0;
    while ( it1p.hasNext ) {
        val left: QualInterval = it1.next();
        val right: QualInterval = it1p.next();
        val merged: QualInterval = left.merge(right);
        lastMergeOrder = scala.math.max(Math.max(lastMergeOrder, left.mergeOrder), right.mergeOrder);
        if ( minMerge == null || (merged.getPenalty() < minMerge.getPenalty() ) ) {
            // System.err.println("  Updating merge " + minMerge);
            minMerge = merged;
        }
    }

    // now actually go ahead and merge the minMerge pair
    // System.err.println("  => final min merge " + minMerge);
    for (subInt <- minMerge.subIntervals) intervals.remove(subInt);
    intervals.add(minMerge);
    minMerge.mergeOrder = lastMergeOrder + 1;
    // System.err.println("updated intervals: " + intervals);
    // return intervals;
  }

  def intervalsToMap(quantizedIntervals : TreeSet[QualInterval]) : Array[Byte] = {
    val map = Array.fill[Byte](nObservationsPerQual.size)(Byte.MinValue)
    for ( interval <- quantizedIntervals ) {
        for ( q <- interval.qStart to interval.qEnd) {
            map(q) = interval.getQual();
        }
    }
    if(map.min == Byte.MinValue) throw new Error("quantized quality score map contains an un-initialized value: " + map.min)
    return map
  }

  def getOriginalToQuantizedMap(): Array[Byte] ={
    return originalToQuantizedMap;
  }
}

@SerialVersionUID(1104L)
class QualInterval(val minInterestingQual: Int, val qStart: Int, val qEnd: Int, val nObservations: Long, val nErrors: Long, val level: Int,
                   val fixedQual: Int = -1, val subIntervals: LinkedHashSet[QualInterval] = LinkedHashSet()) extends Ordered[QualInterval] {
  var mergeOrder: Int = 0
  def compare(that: QualInterval): Int = {
    return this.qStart.compare(that.qStart)
  }

  def merge(toMerge: QualInterval) : QualInterval = {
    val left: QualInterval = if (this.compareTo(toMerge) < 0) this else toMerge;
    val right: QualInterval = if (this.compareTo(toMerge) < 0) toMerge else this;

    if ( left.qEnd + 1 != right.qStart )
        throw new Error("Attempting to merge non-contiguous intervals: left = " + left + " right = " + right);

    val nCombinedObs:Long = left.nObservations + right.nObservations;
    val nCombinedErr:Long = left.nErrors + right.nErrors;
    val newMinInterestingQual = scala.math.min(left.minInterestingQual, right.minInterestingQual)

    val level: Int = scala.math.max(left.level, right.level) + 1;
    val subIntervals = new LinkedHashSet[QualInterval]();
    subIntervals.add(left)
    subIntervals.add(right)
    val merged: QualInterval = new QualInterval(newMinInterestingQual, left.qStart, right.qEnd, nCombinedObs, nCombinedErr, level, subIntervals = subIntervals);

    return merged;
  }

  def getPenalty() : Double = return calcPenalty(getErrorRate())
  def calcPenalty(globalErrorRate: Double) : Double = {
    if ( globalErrorRate == 0.0 ) // there were no observations, so there's no penalty
        return 0.0;

    if ( subIntervals.isEmpty ) {
        // this is leave node
        if ( this.qEnd <= minInterestingQual )
            // It's free to merge up quality scores below the smallest interesting one
            return 0;
        else {
            return (scala.math.abs(scala.math.log10(getErrorRate()) - scala.math.log10(globalErrorRate))) * nObservations;
        }
    } else {
        var sum: Double = 0;
        for ( interval <- subIntervals )
            sum += interval.calcPenalty(globalErrorRate);
        return sum;
    }
  }
  def getQual() : Byte = {
    if ( !hasFixedQual) return QualityUtils.errorProbToQual(getErrorRate())
    else fixedQual.toByte
  }
  def hasFixedQual() : Boolean = fixedQual != -1
  def getErrorRate() : Double = {
    if ( hasFixedQual() ) return QualityUtils.qualToErrorProb(fixedQual.toByte);
    else if ( nObservations == 0 ) return 0.0;
    else return (nErrors+1) / (1.0 * (nObservations+1));
  }
}

@SerialVersionUID(1105L)
class RecalibrationArgumentCollection {
    val headerline1 = "#:GATKTable:2:ROWS:%s:%s:;"
    val headerline2 = "#:GATKTable:Arguments:Recalibration argument collection values used in this run"
    val columnNames = Array("Argument", "Value");

    var doNotUseStandardCovariates = false
    var solidRecalMode = "SET_Q_ZERO"
    var solidNoCallStrategy = "THROW_EXCEPTION"
    var runWithoutDbSnp = false
    var mismatchesContextSize = 2
    var indelsContextSize = 3
    var maximumCycleValue = 500
    var mismatchesDefaultQuality = -1
    var insertionsDefaultQuality = 45
    var deletionsDefaultQuality = 45
    var lowQualTail = 2
    var quantizingLevels = 16
    var binaryTagName = "null"
    var BAQGOP = 40 // BAG.DEFAULT_GOP
    var preserveQscoresLessThan = 6 // QualityUtils.min_usable_q_score
    var enableBAQ = false
    var computeIndelBQSRTables = false
    var defaultBaseQualities = -1
    var useOriginalBaseQualities = false
    var defaultPlatform = "null"
    var forcePlatform = "null"
    var recalibrationReport = "null"
    var covariate = "ReadGroupCovariate,QualityScoreCovariate,ContextCovariate,CycleCovariate"

    override def toString() : String = {
      val columnWidth = Array(28, "ReadGroupCovariate,QualityScoreCovariate,ContextCovariate,CycleCovariate".length) // "mismatches_default_quality".length + 2
      val formatstring = "%-" + columnWidth(0) + "s%-" + columnWidth(1) + "s"
      return headerline1.replace("ROWS", "17") + "\n" +
             headerline2 + "\n" +
             formatstring.format(columnNames(0), columnNames(1)) + "\n" +
             formatstring.format("binary_tag_name", binaryTagName) + "\n" +
             formatstring.format("covariate", covariate) + "\n" +
             formatstring.format("default_platform", defaultPlatform) + "\n" +
             formatstring.format("deletions_default_quality", deletionsDefaultQuality.toString) + "\n" +
             formatstring.format("force_platform", forcePlatform) + "\n" +
             formatstring.format("indels_context_size", indelsContextSize.toString) + "\n" +
             formatstring.format("insertions_default_quality", insertionsDefaultQuality.toString) + "\n" +
             formatstring.format("low_quality_tail", lowQualTail.toString) + "\n" +
             formatstring.format("maximum_cycle_value", maximumCycleValue.toString) + "\n" +
             formatstring.format("mismatches_context_size", mismatchesContextSize.toString) + "\n" +
             formatstring.format("mismatches_default_quality", mismatchesDefaultQuality.toString) + "\n" +
             formatstring.format("no_standard_covs", doNotUseStandardCovariates.toString) + "\n" +
             formatstring.format("quantizing_levels", quantizingLevels.toString) + "\n" +
             formatstring.format("recalibration_report", recalibrationReport) + "\n" +
             formatstring.format("run_without_dbsnp", runWithoutDbSnp.toString) + "\n" +
             formatstring.format("solid_nocall_strategy", solidNoCallStrategy) + "\n" +
             formatstring.format("solid_recal_mode", solidRecalMode) + "\n" +
             "\n";
    }

    def initialize(rows: ArrayBuffer[String]) = {
      for (i <- 0 until rows.size ) {
        val split = rows(i).split(CONSTANTS.TABLE_REGEX_SEPARATOR)
        split(0) match {
          case "binary_tag_name" => {binaryTagName = split(1)}
          case "covariate" => {covariate = split(1)}
          case "default_platform" => {defaultPlatform = split(1)}
          case "deletions_default_quality" => {deletionsDefaultQuality = split(1).toInt}
          case "force_platform" => {forcePlatform = split(1)}
          case "indels_context_size" => {indelsContextSize = split(1).toInt}
          case "insertions_default_quality" => {insertionsDefaultQuality = split(1).toInt}
          case "low_quality_tail" => {lowQualTail = split(1).toInt}
          case "maximum_cycle_value" => {maximumCycleValue = split(1).toInt}
          case "mismatches_context_size" => {mismatchesContextSize = split(1).toInt}
          case "mismatches_default_quality" => {mismatchesDefaultQuality = split(1).toInt}
          case "no_standard_covs" => {doNotUseStandardCovariates = split(1).toBoolean}
          case "quantizing_levels" => {quantizingLevels = split(1).toInt}
          case "recalibration_report" => {recalibrationReport = split(1)}
          case "run_without_dbsnp" => {runWithoutDbSnp = split(1).toBoolean}
          case "solid_nocall_strategy" => {solidNoCallStrategy = split(1)}
          case "solid_recal_mode" => {solidRecalMode = split(1)}
          case other  => println("Unexpected case: " + other.toString)
        }
      }
    }
}
