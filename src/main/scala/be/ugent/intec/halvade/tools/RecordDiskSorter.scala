package be.ugent.intec.halvade.tools

import scala.collection.mutable.ArrayBuffer
import be.ugent.intec.halvade.SplitIdAndRecordWithPosition
import java.util.UUID
import be.ugent.intec.halvade.IoUtils._
import scala.io.Source
import java.io.{PrintWriter, File, FileOutputStream, FileInputStream}
import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.{Input, Output}
import java.util.PriorityQueue
import scala.reflect.ClassTag
import collection.JavaConverters.asJavaCollection
import be.ugent.intec.halvade.Logging
import java.util.concurrent.Executors

class RecordDiskSorter(val tmpDir_ : String, val outputFiles: Array[String], val maxRecordsInMemory: Int = 500000) extends Logging {
  val tmpDir = if(tmpDir_.last == '/') tmpDir_ else tmpDir_ + "/"
  val splits = outputFiles.size
  val tmpFiles = Array.fill(splits)(new ArrayBuffer[String]());
  val uuid = Array.fill(splits)(UUID.randomUUID().toString());
  val kryo = new Kryo();
  kryo.register(classOf[be.ugent.intec.halvade.SplitIdAndRecordWithPosition]);
  protected val timeout = 30
  protected val units = java.util.concurrent.TimeUnit.SECONDS

  def sort(header: Array[String], title: String, iterators: Iterator[SplitIdAndRecordWithPosition]*) : Array[Long] = {
    val time = System.nanoTime
    tmpFiles.foreach(_.clear())
    var datasize: Array[Long] =  Array.fill(splits)(0L)
    var n = 0;
    val records = Array.fill(splits)(new ArrayBuffer[SplitIdAndRecordWithPosition]());
    for(it <- iterators) {
      for(record <- it) {
        if(n == maxRecordsInMemory) { // sort and save to tmp file
          sortAndWriteToFile(records)
          n = 0
          records.foreach(_.clear)
        }
        records(record.split) += record
        datasize(record.split) += record.record.size + 1
        n += 1
      }
    }
    if(tmpFiles.map(_.size).sum == 0) { // no files yet, so just write this to final file
      sortFromMemoryAndWriteToOutput(header, records);
    } else {
      sortAndWriteToFile(records)
      sortIntermediateFilesAndWriteToOutput(header)
    }
    info("[" + title + "] time (s): "+(System.nanoTime-time)/1.0e9)
    return datasize
  }

  protected def sortAndWriteToFile(records: Array[ArrayBuffer[SplitIdAndRecordWithPosition]]) = {
    for(i <- 0 until splits) {
      if (records(i).size > 0) {
        val newTmpFile = tmpDir + uuid(i) + "-" + tmpFiles(i).size + ".tmp"
        debug("adding tmp file for split " + i + ": " + newTmpFile + " for " + records(i).size + " records")
        val out = new Output(new FileOutputStream(newTmpFile));
        withResources(out)(x => {
          kryo.writeObject(out, records(i).size);
          for (record <- records(i).sorted)
            kryo.writeObject(out, record);
        })
        tmpFiles(i) += newTmpFile
      }
    }
  }

  // fits in memory (based on maxRecordsInMemory)
  protected def sortFromMemoryAndWriteToOutput(header: Array[String], records: Array[ArrayBuffer[SplitIdAndRecordWithPosition]]) {
    for(i <- 0 until splits) {
      debug("writing split " + i + " from memory: " + outputFiles(i))
      val out = new PrintWriter(new File(outputFiles(i)))
      withResources(out)(x => {
        for (line <- header) {
          out.write(line + '\n')
        }
        for (line <- records(i).sorted) {
          out.write(line.toString())
        }
      })
    }
  }


  protected def sortIntermediateFilesAndWriteToOutput(header: Array[String]) {
    for (i <- 0 until splits) {
      debug("writing split " + i + ": " + outputFiles(i))
      val out = new PrintWriter(new File(outputFiles(i)))
      withResources(out)(x => {
        var records = 0;
        for (line <- header) {
          out.write(line + '\n')
        }
        // open all files
        val inputs : ArrayBuffer[InputWithLastObject] = tmpFiles(i).map(filename => new InputWithLastObject(kryo, filename, new Input(new FileInputStream(filename)), null))
        inputs.foreach(_.readOrCloseAtEnd);
        val q = new PriorityQueue[InputWithLastObject]()
        q.addAll(asJavaCollection(inputs))
        while (!q.isEmpty()) {
          records += 1
          val smallest : InputWithLastObject = q.poll // get and remove first element
          out.write(smallest.value.toString);
          val newvalue = smallest.readOrCloseAtEnd()
          if(newvalue != null)
            q.offer(newvalue); // add element in queue
        }
        debug(records + " records written to " + outputFiles(i))
      })
      info("filesize of " + outputFiles(i) + " is " + new File(outputFiles(i)).length)
    }
    // val instances  = Executors.newFixedThreadPool(splits)
    // for (i <- 0 until splits)
    //   instances.execute(task(header, i))
    // // shutdown & wait for termination
    // instances.shutdown
    // while(!instances.awaitTermination(timeout, units)) { debug("Still merging files")} // consider setting a real time out here to kill tasks
  }

  protected def task(header:Array[String], i: Int) : Runnable = () => {
    debug("writing split " + i + ": " + outputFiles(i))
    val out = new PrintWriter(new File(outputFiles(i)))
    withResources(out)(x => {
      for (line <- header) {
        out.write(line + '\n')
      }
      // open all files
      val inputs : ArrayBuffer[InputWithLastObject] = tmpFiles(i).map(filename => new InputWithLastObject(kryo, filename, new Input(new FileInputStream(filename)), null))
      inputs.foreach(_.readOrCloseAtEnd);
      val q = new PriorityQueue[InputWithLastObject]()
      q.addAll(asJavaCollection(inputs))
      while (!q.isEmpty()) {
        val smallest : InputWithLastObject = q.poll // get and remove first element
        out.write(smallest.value.toString);
        val newvalue = smallest.readOrCloseAtEnd()
        if(newvalue != null)
          q.offer(newvalue); // add element in queue
      }
    })
  }

  protected class InputWithLastObject(val kryo: Kryo, val filename: String, val input: Input, var value: SplitIdAndRecordWithPosition) extends Ordered[InputWithLastObject] {
    val n = kryo.readObject(input, classOf[Int])
    var i = 0;
    def compare(that: InputWithLastObject): Int = this.value.compare(that.value)
    def readOrCloseAtEnd() : InputWithLastObject = {
      if(i < n) {
        value = kryo.readObject(input, classOf[SplitIdAndRecordWithPosition])
        i += 1
        this
      } else {
        input.close()
        val f: File = new File(filename)
        if (f.exists) f.delete
        null
      }
    }
  }
}
