package org.apache.spark

import be.ugent.intec.halvade.{DictIndexAndPosition, RecordWithAllPositions}
import java.io.{IOException, ObjectInputStream, ObjectOutputStream}
import org.apache.spark.rdd.{PartitionPruningRDD, RDD}
import org.apache.spark.serializer.JavaSerializer
import org.apache.spark.util.{CollectionsUtils, Utils}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.hashing.byteswap32
import scala.reflect.ClassTag
import be.ugent.intec.halvade.BioCaseClassFunctions._
import scala.collection.Map // makes sure all maps are the same..


/**
 *
 * BASED ON THE APACHE SPARK RANGEPARTITIONER
 *
 * A [[org.apache.spark.Partitioner]] that partitions sortable records by range into roughly
 * equal ranges. The ranges are determined by sampling the content of the RDD passed in.
 *
 * @note The actual number of partitions created by the RangePartitioner might not be the same
 * as the `partitions` parameter, in the case where the number of sampled records is less than
 * the value of `partitions`.
 */
class ChromosomeRegionRangePartitioner(
      partitions: Int,
      rdd: RDD[_ <: Product2[DictIndexAndPosition, RecordWithAllPositions]],
      val idxToSizeMap: scala.collection.Map[Int, Int],
      val splitPerChromosome: Boolean, // split per chrom so no regions span bounds of chromosomes -> will create extra partitions but should be way more efficient for strelka2
      val maxKeyDist : Int = 100, // max key distance for dense regions, should be about readlength maybe to avoid a lot of duplicated reads
      val maxDistanceToBlacklistRegion: Int = 1000000,
      val samplePointsPerPartitionHint: Int = 20,
      private var ascending: Boolean = true)
    extends Partitioner with be.ugent.intec.halvade.Logging {

    // A constructor declared in order to maintain backward compatibility for Java, when we add the
    // 4th constructor parameter samplePointsPerPartitionHint. See SPARK-22160.
    // This is added to make sure from a bytecode point of view, there is still a 3-arg ctor.
    // def this(partitions: Int, rdd: RDD[_ <: Product2[DictIndexAndPosition, RecordWithPosition]],
    //   possibleSplitRegions: Array[(DictIndexAndPosition, DictIndexAndPosition)]) = {
    //   this(partitions, rdd, possibleSplitRegions, true, samplePointsPerPartitionHint = 20)
    // }

    // We allow partitions = 0, which happens when sorting an empty RDD under the default settings.
    require(partitions >= 0, s"Number of partitions cannot be negative but found $partitions.")
    require(samplePointsPerPartitionHint > 0,
      s"Sample points per partition must be greater than 0 but found $samplePointsPerPartitionHint")

    private var ordering = implicitly[Ordering[DictIndexAndPosition]]

    var densePartitions = scala.collection.Map.empty[Int, Long]
    def determineBounds(
          candidates: ArrayBuffer[(DictIndexAndPosition, Float)],
          partitions: Int): Array[DictIndexAndPosition] = {
        info("determine bounds")
        val ordered = candidates.sortBy(_._1)
        val numCandidates = ordered.size
        val sumWeights = ordered.map(_._2.toDouble).sum
        val step : Double = sumWeights / partitions
        var cumWeight = 0.0
        var target = step
        val bounds = ArrayBuffer.empty[DictIndexAndPosition]
        var i = 0
        var j = 0
        var previousBound = Option.empty[DictIndexAndPosition]
        while ((i < numCandidates) && (j < partitions - 1)) {
            val (key, weight) = ordered(i)
            cumWeight += weight
            if (cumWeight >= target) {
                if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
                    val dist = getDistance(key, previousBound.getOrElse(DictIndexAndPosition(0,1)), idxToSizeMap)
                    bounds += key
                    previousBound = Some(key)
                    target += step
                    j += 1
                }
            }
            i += 1
        }
        if(splitPerChromosome) splitBoundsPerChromosome(bounds) else bounds.toArray
    }

    def splitBoundsPerChromosome(ab: ArrayBuffer[DictIndexAndPosition]) :  Array[DictIndexAndPosition] = {
      info("split bounds per chromosome")
      var i = 1
      val maxDistToSepparate = 1000000;
      val bounds = ArrayBuffer.empty[DictIndexAndPosition]
      bounds += ab(0)
      while(i < ab.size) {
        if(ab(i).index != ab(i - 1).index) {
          if(ab(i).index == ab(i-1).index + 1 )
            if((idxToSizeMap(ab(i-1).index) - ab(i-1).position) > maxDistToSepparate && ab(i).position > maxDistToSepparate) { // split region in two, so add the end of chr to bounds
              debug("add new region: " + ab(i-1).index + ":" + idxToSizeMap(ab(i-1).index)) // prev is already added
              bounds += DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
              bounds += ab(i)
            } else { // no need to update region but change bounds -> either i - 1 to end or i to end, depending on which area is biggest
              debug("change bound: " + ab(i-1) + " or " + ab(i))
              if(ab(i).position > (idxToSizeMap(ab(i-1).index) - ab(i-1).position)) {
                debug("update region: " + ab(i-1).index + ":" + ab(i-1).position + " -> " + ab(i-1).index + ":" + idxToSizeMap(ab(i-1).index))
                bounds(bounds.size - 1) = DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
                bounds += ab(i)
              } else {
                debug("update region: " + ab(i).index + ":" + ab(i).position + " -> " + ab(i-1).index + ":" + idxToSizeMap(ab(i-1).index))
                bounds += DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
              }
            }
          else {
            if((idxToSizeMap(ab(i-1).index) - ab(i-1).position) > maxDistToSepparate || bounds(bounds.size - 1).index != ab(i-1).index) {
              // create a new bound to end of last chr in prev bound
              debug("creating new bound to end of chr")
              bounds += DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
            } else {
              debug("update last bound to end of chr")
              bounds(bounds.size - 1) = DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
            }
            var curidx = ab(i-1).index + 1
            while(curidx < ab(i).index) {
              bounds += DictIndexAndPosition(curidx, idxToSizeMap(curidx))
              debug("add new bound" + bounds(bounds.size - 1))
              curidx += 1
            }
            if(ab(i).position > maxDistToSepparate) {
              // create a new bound to end of last chr in prev bound
              debug("add new bound " + ab(i))
              bounds += ab(i)
            } else {
              if(i + 1 < ab.size) {
                if(ab(i+1).index != curidx) {
                  debug("add new bound because next " + ab(i))
                  bounds += DictIndexAndPosition(curidx, idxToSizeMap(curidx))
                }
              }
              debug("update last bound to end of chr (should alread be the case so no need to change it)")
              bounds(bounds.size - 1) = DictIndexAndPosition(ab(i).index-1, idxToSizeMap(ab(i).index-1))
            }
          }
        } else {
          debug("adding regular bound " + ab(i))
          bounds += ab(i)
        }
        i+=1
      }
      // check last bound
      if(ab(i-1).index < idxToSizeMap.size - 1) {
        if((idxToSizeMap(ab(i-1).index) - ab(i-1).position) > maxDistToSepparate || bounds(bounds.size - 1).index != ab(i-1).index) {
          // create a new bound to end of last chr in prev bound
          debug("creating new bound to end of chr")
          bounds += DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
        } else {
          debug("update last bound to end of chr")
          bounds(bounds.size - 1) = DictIndexAndPosition(ab(i-1).index, idxToSizeMap(ab(i-1).index))
        }
        var curidx = ab(i-1).index + 1
        while(curidx < idxToSizeMap.size - 1) { // last chr end doesn't need to be added, this is the last bound
          bounds += DictIndexAndPosition(curidx, idxToSizeMap(curidx))
          debug("add new bound" + bounds(bounds.size - 1))
          curidx += 1
        }
      }
      bounds.toArray
    }

    def determineBoundsWithDenseRegions(
        candidates: ArrayBuffer[(DictIndexAndPosition, Float)],
        partitions: Int): Array[DictIndexAndPosition] = {
      info("determine bounds with dense regions")
      val ordered = candidates.sortBy(_._1)
      val numCandidates = ordered.size
      val sumWeights = ordered.map(_._2.toDouble).sum
      var step : Double = sumWeights / partitions

      var cumWeight = 0.0
      var target = step
      val bounds = ArrayBuffer.empty[DictIndexAndPosition]
      var i = 0
      var j = 0
      var prevWeight = 0.0
      var previousBound = Option.empty[DictIndexAndPosition]
      while ((i < numCandidates) && (j < partitions - 1)) {
          val (key, weight) = ordered(i)
          var keydist = getDistance(key, if(i > 0) ordered(i-1)._1 else DictIndexAndPosition(0,1), idxToSizeMap)
          if (keydist < maxKeyDist) { // entering dense region! check how big the region is thats very dense:
            val lastkeybeforedense = if(i > 0) ordered(i-1)._1 else DictIndexAndPosition(0,1)
            var denseWeight = cumWeight + weight
            var denseTarget = cumWeight + step
            var itmp = i;
            while(itmp < numCandidates && keydist < maxKeyDist) {
              itmp+=1
              denseWeight += ordered(itmp)._2
              keydist = getDistance(ordered(itmp - 1)._1, ordered(itmp)._1, idxToSizeMap)
            }
            val denseKey = ordered(itmp - 1)._1
            i = itmp;
            if(denseWeight > denseTarget) {
              if (cumWeight - prevWeight > step * 0.75) { // if more than 50% of step make its own region
                debug("split regions since weight is > 75%: " + (cumWeight - prevWeight))
                val dist1 = getDistance(lastkeybeforedense, previousBound.getOrElse(DictIndexAndPosition(0,1)), idxToSizeMap)
                debug(lastkeybeforedense + "\t" + dist1 + "\t" + (cumWeight - prevWeight))
                bounds += lastkeybeforedense
                j += 1

                val dist2 = getDistance(denseKey, lastkeybeforedense, idxToSizeMap)
                debug(denseKey + "\t" + dist2 + "\t" + (denseWeight - cumWeight))
                bounds += denseKey
                j += 1

                // recalculate step!!
                cumWeight = denseWeight
                prevWeight = cumWeight
                previousBound = Some(denseKey)
                step = (sumWeights - cumWeight) / (partitions - j)
                target = cumWeight + step
              } else if (denseWeight - prevWeight < step * 1.5) { // join together since dense region and prev is < 150% and prev weight is also < 50%
                debug("joining both regions together since weight is less than 2x step" + (denseWeight - prevWeight))
                val dist = getDistance(denseKey, previousBound.getOrElse(DictIndexAndPosition(0,1)), idxToSizeMap)
                debug(denseKey + "\t" + dist + "\t" + (denseWeight - prevWeight))
                bounds += denseKey
                j += 1

                cumWeight = denseWeight
                prevWeight = cumWeight
                previousBound = Some(denseKey)
                step = (sumWeights - cumWeight) / (partitions - j)
                target = cumWeight + step
              } else { // prev step is less than 50% but this more than 150%! so merge with prev step and create new
                debug("replace previous key and create 1 new region: " + (denseWeight - prevWeight))
                debug("newkey: " + "\t" + lastkeybeforedense)
                bounds(bounds.size - 1) = lastkeybeforedense // change previous key
                val dist = getDistance(denseKey, lastkeybeforedense, idxToSizeMap)
                debug(denseKey + "\t" + dist + "\t" + (denseWeight - prevWeight))
                bounds += denseKey
                j += 1

                cumWeight = denseWeight
                prevWeight = cumWeight
                previousBound = Some(denseKey)
                step = (sumWeights - cumWeight) / (partitions - j)
                target = cumWeight + step
              }
            } else {
              cumWeight = denseWeight
            }
          } else {
            cumWeight += weight
          }
          if (cumWeight >= target) {
              if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
                  val dist = getDistance(key, previousBound.getOrElse(DictIndexAndPosition(0,1)), idxToSizeMap)
                  debug(key + "\t" + dist + "\t" + (cumWeight - prevWeight))
                  prevWeight = cumWeight
                  bounds += key
                  previousBound = Some(key)
                  target += step
                  j += 1
              }
          }
          i += 1
      }
      if(splitPerChromosome) splitBoundsPerChromosome(bounds) else bounds.toArray
    }

    // An array of upper bounds for the first (partitions - 1) partitions
    var rangeBounds : Array[DictIndexAndPosition] = {
      if (partitions <= 1) {
        Array.empty
      } else {
        // This is the sample size we need to have roughly balanced output partitions, capped at 1M.
        // Cast to double to avoid overflowing ints or longs
        val sampleSize = math.min(samplePointsPerPartitionHint.toDouble * partitions, 1e6)
        // Assume the input partitions are roughly balanced and over-sample a little bit.
        val sampleSizePerPartition = math.ceil(3.0 * sampleSize / rdd.partitions.length).toInt
        val (numItems, sketched) = RangePartitioner.sketch(rdd.map(_._1), sampleSizePerPartition)
        if (numItems == 0L) {
          Array.empty
        } else {
          // If a partition contains much more than the average number of items, we re-sample from it
          // to ensure that enough items are collected from that partition.
          val fraction = math.min(sampleSize / math.max(numItems, 1L), 1.0)
          val candidates = ArrayBuffer.empty[(DictIndexAndPosition, Float)]
          val imbalancedPartitions = mutable.Set.empty[Int]
          sketched.foreach { case (idx, n, sample) =>
            if (fraction * n > sampleSizePerPartition) {
              imbalancedPartitions += idx
            } else {
              // The weight is 1 over the sampling probability.
              val weight = (n.toDouble / sample.length).toFloat
              for (key <- sample) {
                candidates += ((key, weight))
              }
            }
          }
          if (imbalancedPartitions.nonEmpty) {
            // Re-sample imbalanced partitions with the desired sampling probability.
            val imbalanced = new PartitionPruningRDD(rdd.map(_._1), imbalancedPartitions.contains)
            val seed = byteswap32(-rdd.id - 1)
            val reSampled = imbalanced.sample(withReplacement = false, fraction, seed).collect()
            val weight = (1.0 / fraction).toFloat
            candidates ++= reSampled.map(x => (x, weight))
          }
          determineBoundsWithDenseRegions(candidates, math.min(partitions, candidates.size))
        }
      }
    }

    def numPartitions: Int = rangeBounds.length + 1

    var binarySearch: ((Array[DictIndexAndPosition], DictIndexAndPosition) => Int) = CollectionsUtils.makeBinarySearch[DictIndexAndPosition]

    def getPartition(key: Any): Int = {
      val k = key.asInstanceOf[DictIndexAndPosition]
      var partition = 0
      if (rangeBounds.length <= 128) {
        // If we have less than 128 partitions naive search
        while (partition < rangeBounds.length && ordering.gt(k, rangeBounds(partition))) {
          partition += 1
        }
      } else {
        // Determine which binary search method to use only once.
        partition = binarySearch(rangeBounds, k)
        // binarySearch either returns the match location or -[insertion point]-1
        if (partition < 0) {
          partition = -partition-1
        }
        if (partition > rangeBounds.length) {
          partition = rangeBounds.length
        }
      }
      if (ascending) {
        partition
      } else {
        rangeBounds.length - partition
      }
    }

    override def equals(other: Any): Boolean = other match {
      case r: ChromosomeRegionRangePartitioner =>
        r.rangeBounds.sameElements(rangeBounds) && r.ascending == ascending
      case _ =>
        false
    }

    override def hashCode(): Int = {
      val prime = 31
      var result = 1
      var i = 0
      while (i < rangeBounds.length) {
        result = prime * result + rangeBounds(i).hashCode
        i += 1
      }
      result = prime * result + ascending.hashCode
      result
    }

    @throws(classOf[IOException])
    private def writeObject(out: ObjectOutputStream): Unit = Utils.tryOrIOException {
      val sfactory = SparkEnv.get.serializer
      sfactory match {
        case js: JavaSerializer => out.defaultWriteObject()
        case _ =>
          out.writeBoolean(ascending)
          out.writeObject(ordering)
          out.writeObject(binarySearch)

          val ser = sfactory.newInstance()
          Utils.serializeViaNestedStream(out, ser) { stream =>
            stream.writeObject(scala.reflect.classTag[Array[DictIndexAndPosition]])
            stream.writeObject(rangeBounds)
          }
      }
    }

    @throws(classOf[IOException])
    private def readObject(in: ObjectInputStream): Unit = Utils.tryOrIOException {
      val sfactory = SparkEnv.get.serializer
      sfactory match {
        case js: JavaSerializer => in.defaultReadObject()
        case _ =>
          ascending = in.readBoolean()
          ordering = in.readObject().asInstanceOf[Ordering[DictIndexAndPosition]]
          binarySearch = in.readObject().asInstanceOf[(Array[DictIndexAndPosition], DictIndexAndPosition) => Int]

          val ser = sfactory.newInstance()
          Utils.deserializeViaNestedStream(in, ser) { ds =>
            implicit val classTag = ds.readObject[ClassTag[Array[DictIndexAndPosition]]]()
            rangeBounds = ds.readObject[Array[DictIndexAndPosition]]()
          }
      }
    }
  }
