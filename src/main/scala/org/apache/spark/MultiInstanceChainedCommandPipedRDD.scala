package org.apache.spark


import be.ugent.intec.halvade.PartitionSplit
import java.util.concurrent.Executors
import java.util.StringTokenizer
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter, DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import java.util.concurrent.atomic.AtomicReference
import java.nio.charset.CodingErrorAction
import org.apache.spark.rdd.HadoopPartition
import org.apache.spark.rdd.RDD
import org.apache.log4j.{Level, Logger}
import org.apache.commons.io.FileUtils
import be.ugent.intec.halvade.IoUtils._
import be.ugent.intec.halvade.Utils.humanReadableMemory
import scala.reflect.ClassTag
import scala.collection.Map // ensure same maps...
import scala.collection.JavaConverters._
import scala.io.Codec.string2codec
import scala.io.{Codec, Source}
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import java.util.UUID


class MultiInstanceChainedCommandPipedRDD[+T: ClassTag](
    prev: RDD[(Int, T)],
    commands: Array[String],
    placeholders: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    numInstances: Int,
    tmpFolder: String = "/tmp/",
    cleanupTmp: Boolean = true,
    extraFiles: Map[String, String] = Map(),
    envVars: Map[String, String] = Map(),
    logLevel: Level = Level.INFO,
    streamIn: Boolean = false,
    streamOut: Boolean = false,
    metrics: Boolean = false
  ) extends MultiInstancePipedRDD(prev, commands, placeholders, procNames, regions, numInstances, tmpFolder, cleanupTmp, extraFiles, envVars, logLevel, streamIn, streamOut, metrics) with be.ugent.intec.halvade.Logging {

  // runs a command on this partition!
  protected def runCommand(split: Partition, number: Int, data: Any, idx: Int, command: String, procName: String): Array[Byte] = {

    val time = System.nanoTime
    val pb = new ProcessBuilder(if (command.contains("|") || command.contains(">") || command.contains("awk")) List("/usr/bin/env" , "sh", "-c", command).asJava else tokenize(command).asJava) // take care of redirects if needed
    setEnvironment(split, pb)

    info("["+ split.index + "-" + number + "] running " + command)
    val proc = pb.start()
    val childThreadException = new AtomicReference[Throwable](null)

    // READ ERROR STREAM
    var gatkHasFinished = false;
    if(logLevel == Level.DEBUG || logLevel == Level.TRACE || logLevel == Level.ALL) {
      new Thread(procName + "|stderr") {
        override def run(): Unit = {
          val err = proc.getErrorStream
          withResources(err)(x => {
            for (line <- Source.fromInputStream(err)(Codec.defaultCharsetCodec.name).getLines) {
              if (line.contains("done. Elapsed time")) gatkHasFinished = procName.contains("GATK4") // only valid for GATK4 runs
              debug("[" + split.index + "-" + number + "|stderr] " + line)
            }
          })
        }
      }.start()
    }

    // WRITE TO STDIN if needed
    if(idx == 0 && streamIn) {
      // Start a thread to feed the process input from our parent's iterator
      new Thread(procName + "|stdin") {
        override def run(): Unit = {
          val out  = new DataOutputStream(proc.getOutputStream());
          withResources(out)(x => {
            var fsize = 0
            data match {
              case bytearray: Array[Byte] => {
                out.write(bytearray)
                fsize = bytearray.size
              }
              case stringarraybuffer: ArrayBuffer[_] => {
                for (line <- stringarraybuffer.asInstanceOf[ArrayBuffer[String]]) {
                  out.write((line + '\n').getBytes)
                  fsize += line.size + 1
                }
              }
              case _ => throw new Exception("data format " + data.getClass + " not yet supported in MultiInstanceChainedCommandPipedRDD")
             }
            info("["+ split.index+ "-" + number + "] has written " + humanReadableMemory(fsize) + " to stdin")
          })
        }
      }.start()
    }

    // STDOUT IS OUTPUT
    val output = org.apache.commons.io.IOUtils.toByteArray(proc.getInputStream)

    val exitStatus = proc.waitFor()

    debug("["+ split.index+ "-" + number + "|" + procName + "|EXITSTATUS]: " + exitStatus)

    // CHECK IF ANY ERRORS HAPPENED
    if (exitStatus != 0) {
      if(gatkHasFinished) {
        info("GATK gave an error AFTER completing run. Ignoring the error and using the output!")
      } else {
        error("["+ split.index+ "-" + number + "] failed running " + idx + ": " + command)
        throw new IllegalStateException("Subprocess " + split.index + s" exited with status $exitStatus. " +
          "Command failed: " + command + "\nSet logging to DEBUG for more information.")
      }
    }
    val t = childThreadException.get()
    if (t != null && !gatkHasFinished) {
      error("["+ split.index+ "-" + number + "] child thread exception in " + command)
      proc.destroy()
      throw t
    }
    info("["+ split.index+ "-" + number + "] finished " + procName + " in "+(System.nanoTime-time)/1.0e9+"s")

    // RETURN DATA FROM STDOUT
    return output
  }

  protected def checkForSkip(partitionSplit: PartitionSplit) : Boolean = false;

  protected def task(split: Partition, data: Any, number: Int) : Runnable = () => {
    var skip = false

    // if empty data just skip
    data match {
      case bytearray: Array[Byte] => {
        if (bytearray.isEmpty) skip = true
      }
      case stringarraybuffer: ArrayBuffer[_] => {
        if(stringarraybuffer.asInstanceOf[ArrayBuffer[String]].isEmpty) skip = true
      }
      case _ => throw new Exception("data format " + data.getClass + " not yet supported in MultiInstanceChainedCommandPipedRDD")
     }

   if(skip || checkForSkip(PartitionSplit(split.index, number))) {
     outputs += ((number, Array.empty[Byte]))
   } else {
      // create necessary files
      val (tmp_files, possible_keep_files, inputfile, outputfile, my_commands) = createFilesForPlaceholders(split, number)

      // START THREADS & ASSIGN
      if(!streamIn) { streamDataToInputFile(split, number, data, inputfile ) }

      // RUN COMMANDS
      var output : Array[Byte] = Array.empty[Byte]
      try {
        for (i <- my_commands.indices) {
          output = runCommand(split, number, data, i, my_commands(i), procNames(i))
        }
        if(!streamOut) {
          debug("["+ split.index+ "-" + number + "] reading partition from " + outputfile)
          output = org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(outputfile))
        }
      } catch {
        case e: Exception => {
          val lsout = scala.sys.process.Process("ls -l " + tmpFolder + "/*_" + split.index + "_*").!!
          debug("> ls -l " + tmpFolder + "\n" + lsout)
          if(cleanupTmp) { // only clean up when not debugging
            cleanupFiles(possible_keep_files)
            cleanupFiles(tmp_files)
            customCleanup(number, outputfile)
           }
          throw e
        }
      }

      // CLEANUP files if no error
      cleanupFiles(possible_keep_files)
      cleanupFiles(tmp_files)
      customCleanup(number, outputfile)

      debug("["+ split.index+ "-" + number + "] output size is " + output.size)

      outputs += ((number, (if(fixGatkStdout) fixGatkStdOut(split, number, output) else output)))
   }
  }


  override def compute(split: Partition, context: TaskContext): Iterator[(Int, Array[Byte])] = {
    setLogLevel();

    // START THREADS & ASSIGN
    val instances  = Executors.newFixedThreadPool(numInstances)
    var i = 0;
    for (elem <- firstParent[(Int, Any)].iterator(split, context)) {
      instances.execute(task(split, elem._2, elem._1)) // iterator does not keep order! apparently!
      i+=1
    }

    // shutdown & wait for termination
    instances.shutdown
    while(!instances.awaitTermination(timeout, units)) { debug("Tasks still running")} // consider setting a real time out here to kill tasks
    outputs.iterator
  }
}
