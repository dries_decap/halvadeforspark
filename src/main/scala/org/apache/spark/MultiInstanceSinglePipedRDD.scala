package org.apache.spark


import be.ugent.intec.halvade.{PartitionSplit, SplitIdAndRecordWithPosition}
import java.util.concurrent.Executors
import java.util.StringTokenizer
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter, DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import java.util.concurrent.atomic.AtomicReference
import java.nio.charset.CodingErrorAction
import org.apache.spark.rdd.HadoopPartition
import org.apache.spark.rdd.RDD
import org.apache.log4j.{Level, Logger}
import org.apache.commons.io.FileUtils
import be.ugent.intec.halvade.IoUtils._
import be.ugent.intec.halvade.Utils.humanReadableMemory
import scala.reflect.ClassTag
import scala.collection.Map // ensure same maps...
import scala.collection.JavaConverters._
import scala.io.Codec.string2codec
import scala.io.{Codec, Source}
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import java.util.UUID
import be.ugent.intec.halvade.tools.RecordDiskSorter

class MultiInstanceSinglePipedRDD(
    prev: RDD[SplitIdAndRecordWithPosition],
    commands: Array[String],
    placeholders: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    numInstances: Int,
    header: Array[String],
    tmpFolder: String = "/tmp/",
    cleanupTmp: Boolean = true,
    readLength: Int = 100,
    extraFiles: Map[String, String] = Map(),
    envVars: Map[String, String] = Map(),
    logLevel: Level = Level.ERROR,
    streamOut: Boolean = false
  ) extends MultiInstancePipedRDD(prev, commands, placeholders, procNames, regions, numInstances, tmpFolder, cleanupTmp,  extraFiles,  envVars, logLevel, streamOut = streamOut) {

    // no need for concurrent triemap as this is only changed before threads are started
  val tmp_files = new ArrayBuffer[ListBuffer[String]]()
  val possible_keep_files = new ArrayBuffer[ListBuffer[String]]()
  val inputfiles = new ArrayBuffer[String]()
  val outputfiles = new ArrayBuffer[String]()
  val my_commands = new ArrayBuffer[Array[String]]()
  var fsize : Array[Long] = null

  protected def streamData(split: Partition, it: Iterator[SplitIdAndRecordWithPosition]) {
    val sorter = new RecordDiskSorter(tmpFolder, inputfiles.toArray)
    fsize = sorter.sort(header, "sort partition " + split.index, it)
    info("[" + split.index + "] has written SAM [" + fsize.map(humanReadableMemory(_)).mkString("\t") + "] to inputfiles")
  }

  // runs a command on this partition!
  def runCommand(split: Partition, number: Int, idx: Int, command: String, procName: String): Array[Byte] = {

    val time = System.nanoTime
    val pb = new ProcessBuilder(if (command.contains("|")) List("/usr/bin/env" , "sh", "-c", command).asJava else tokenize(command).asJava)
    setEnvironment(split, pb)

    info("["+ split.index + "-" + number + "] running " + command)
    val proc = pb.start()
    val childThreadException = new AtomicReference[Throwable](null)

    // READ ERROR STREAM
    if(logLevel == Level.DEBUG || logLevel == Level.TRACE || logLevel == Level.ALL) {
      debug("starting stderr stream")
      new Thread(procName + "|stderr") {
        override def run(): Unit = {
          val err = proc.getErrorStream
          withResources(err)(x => {
            for (line <- Source.fromInputStream(err)(Codec.defaultCharsetCodec.name).getLines) {
              debug("[" + split.index + "-" + number + "|stderr] " + line)
            }
          })
        }
      }.start()
    }

    // STDOUT IS OUTPUT
    val output = org.apache.commons.io.IOUtils.toByteArray(proc.getInputStream)

    val exitStatus = proc.waitFor()

    debug("["+ split.index+ "-" + number + "|" + procName + "|EXITSTATUS]: " + exitStatus)

    // CHECK IF ANY ERRORS HAPPENED
    if (exitStatus != 0) {
      error("["+ split.index+ "-" + number + "] failed running " + idx + ": " + command)
      throw new IllegalStateException("Subprocess " + split.index + s" exited with status $exitStatus. " +
        "Command failed: " + command + "\nSet logging to DEBUG for more information.")
    }
    val t = childThreadException.get()
    if (t != null) {
      error("["+ split.index+ "-" + number + "] child thread exception in " + command)
      proc.destroy()
      throw t
    }
    info("["+ split.index+ "-" + number + "] finished " + procName + " in "+(System.nanoTime-time)/1.0e9+"s")

    // RETURN DATA FROM STDOUT
    return output
  }
  def task(split: Partition, number: Int) : Runnable = () => {
    // CHECK IF DATA EXISTS
    if (fsize(number) == 0) {
      outputs += ((number, Array.empty[Byte]))
    } else {
      // RUN COMMANDS
      var output : Array[Byte] = null
      try {
        for (i <- my_commands(number).indices) {
          output = runCommand(split, number, i, my_commands(number)(i), procNames(i))
        }
        if(!streamOut) {
          debug("["+ split.index+ "-" + number + "] reading partition from " + outputfiles(number))
          output = org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(outputfiles(number)))
        }
      } catch {
        case e: Exception => {
          if(cleanupTmp) {
            cleanupFiles(tmp_files(number), true)
            cleanupFiles(possible_keep_files(number), true)
            customCleanup(number, outputfiles(number))
          }
          throw e
        }
      }

      // CLEANUP
      if(cleanupTmp) {
        cleanupFiles(tmp_files(number))
        cleanupFiles(possible_keep_files(number))
        customCleanup(number, outputfiles(number))
      }
      debug("["+ split.index+ "-" + number + "] output size is " + output.size)

      outputs += ((number, (if(fixGatkStdout) fixGatkStdOut(split, number, output) else output)))
    }
  }


  override def compute(split: Partition, context: TaskContext): Iterator[(Int, Array[Byte])] = {
    setLogLevel();

    // create necessary files
    for (i <- 0 until numInstances) {
      val (tmp_files_ , possible_keep_files_ , inputfile_ , outputfile_ , my_commands_) = createFilesForPlaceholders(split, i) // (ListBuffer[String], ListBuffer[String], String, String, Array[String])
      tmp_files += tmp_files_
      possible_keep_files += possible_keep_files_
      inputfiles += inputfile_
      outputfiles += outputfile_
      my_commands += my_commands_
    }
    // SORT DATA TO FILE
    streamData(split, firstParent[SplitIdAndRecordWithPosition].iterator(split, context))

    // START THREADS & ASSIGN
    val instances  = Executors.newFixedThreadPool(numInstances)
    for (i <- 0 until numInstances) {
      instances.execute(task(split, i)) // true id is the id, rather than position since iterator does not keep order! apparently!
    }

    // shutdown & wait for termination
    instances.shutdown
    while(!instances.awaitTermination(timeout, units)) { debug("Tasks still running")} // consider setting a real time out here to kill tasks
    outputs.iterator
  }
}
