package org.apache.spark

import be.ugent.intec.halvade.PartitionSplit
import java.util.concurrent.Executors
import java.util.StringTokenizer
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter, DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import java.util.concurrent.atomic.AtomicReference
import java.nio.charset.CodingErrorAction
import org.apache.spark.rdd.HadoopPartition
import org.apache.spark.util.Utils
import org.apache.spark.rdd.RDD
import org.apache.log4j.{Level, Logger}
import org.apache.commons.io.FileUtils;
import scala.reflect.ClassTag
import scala.collection.Map // ensure same maps...
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._
import scala.io.Codec.string2codec
import scala.io.Source._
import scala.io.{Codec, Source}
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._

// based on PipedRDD from Spark!
class MultiInstanceMutectPipedRDD[+T: ClassTag](
    prev: RDD[(Int, T)],
    commands: Array[String],
    placeholders: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    numInstances: Int,
    tmpFolder: String = "/tmp/",
    cleanupTmp: Boolean = true,
    extraFiles: Map[String, String] = Map(),
    envVars: Map[String, String] = Map(),
    logLevel: Level = Level.ERROR,
    streamOut: Boolean = false
  )
  extends MultiInstanceChainedCommandPipedRDD(prev, commands, placeholders, procNames, regions, numInstances, tmpFolder,
    cleanupTmp, extraFiles, envVars, logLevel, streamOut = streamOut) {

    var callable = scala.collection.concurrent.TrieMap.empty[Int, Int] // ensure its thread safe

    override def customCleanup(number: Int, outputfile: String) : Unit = {
      val vcfstats = new File(outputfile + ".stats")
      if(vcfstats.exists())
        callable(number) = fromFile(outputfile + ".stats").getLines.toArray.map(x => x.split("\t")).filter(x => x(0) == "callable")(0)(1).toDouble.toInt
      // delete the stats file
      if(vcfstats.exists()) vcfstats.delete()
    }
    override def compute(split: Partition, context: TaskContext): Iterator[(Int, Array[Byte])] = {
      super[MultiInstanceChainedCommandPipedRDD].compute(split, context).map(x => (callable.getOrElse(x._1, 0), x._2))
    }
}
