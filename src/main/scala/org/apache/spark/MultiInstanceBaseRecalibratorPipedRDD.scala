package org.apache.spark


import be.ugent.intec.halvade.PartitionSplit
import java.util.concurrent.Executors
import java.util.StringTokenizer
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter, DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import java.util.concurrent.atomic.AtomicReference
import java.nio.charset.CodingErrorAction
import org.apache.spark.rdd.HadoopPartition
import org.apache.spark.util.Utils
import org.apache.spark.rdd.RDD
import org.apache.log4j.{Level, Logger}
import org.apache.commons.io.FileUtils;
import scala.reflect.ClassTag
import scala.collection.Map // ensure same maps...
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._
import scala.io.Codec.string2codec
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._


// based on PipedRDD from Spark!
class MultiInstanceBaseRecalibratorPipedRDD[+T: ClassTag](
    prev: RDD[(Int, T)],
    commands: Array[String],
    placeholders: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    numInstances: Int,
    readLength: Int = 100,
    tmpFolder: String = "/tmp/",
    cleanupTmp: Boolean = true,
    extraFiles: Map[String, String] = Map(),
    envVars: Map[String, String] = Map(),
    logLevel: Level = Level.ERROR,
  )
  extends MultiInstanceChainedCommandPipedRDD(prev, commands, placeholders, procNames, regions, numInstances, tmpFolder,
    cleanupTmp,  extraFiles,  envVars, logLevel) {

    protected val bqsr_fix_region_placeholder = "regionwithbqsrfix.intervals"

    override def customUpdateCommand(commands: Array[String], partitionSplit: PartitionSplit) : Array[String] = {
      if (commands.filter(_.contains(bqsr_fix_region_placeholder)).size > 0 && regions == null) {
        throw new Exception("bqsr fix region placeholder present but no regions provided")
      }
      if(regions != null) {
        val fixed_region =  "-L " + regions(partitionSplit).map(x => {
            val begin = x.substring(x.indexOf(":")+1, x.indexOf("-", 3)).toInt
            x.replace(begin.toString, (if(begin!=1) begin+readLength else begin).toString)
          }).mkString(" -L ")
        commands.map(_.replace(bqsr_fix_region_placeholder, fixed_region))
      } else commands
    }
}
