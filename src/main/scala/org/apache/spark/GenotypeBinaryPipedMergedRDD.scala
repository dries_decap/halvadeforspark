package org.apache.spark


import org.apache.spark.rdd.HadoopPartition
import scala.reflect.ClassTag
import java.util.StringTokenizer
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._
import org.apache.spark.util.Utils
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter,
  DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import scala.io.{Codec, Source}
import java.util.concurrent.atomic.AtomicReference
import org.apache.spark.rdd.RDD
import scala.io.Codec.string2codec
import org.apache.log4j.{Level, Logger}
import java.nio.charset.CodingErrorAction
import scala.io.Source._
import org.apache.commons.io.FileUtils;
import scala.collection.Map // ensure same maps...
import be.ugent.intec.halvade.PartitionSplit

/*
 *
 *     prev: RDD[T],
    command: Seq[String],
    envVars: Map[String, String],
    printPipeContext: (String => Unit) => Unit,
    printRDDElement: (T, String => Unit) => Unit,
    separateWorkingDir: Boolean,
    bufferSize: Int,
    encoding: String)
 */

/*
 * input to tmp file
 * - give an map of idx -> filename back for re-use
 * two ways for output
 * - give a map of idx -> output files for later use (gives no RDD back!) (e.g. indel intervals + bqsr table creation)
 * - give new RDD (with output of the tool)! like markduplicates
 *
 * give all three always, but use null if not used? -> how to do this? wrapper class!!!
 */


// based on PipedRDD from Spark!
class GenotypeBinaryPipedMergedRDD(
    prev: RDD[(String, String)],
    commands: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    placeholders: Array[String],
    inputSplitFilters: Array[String],
    var logLevel: Level = null,
    var metrics: Boolean = false,
    tmpFolder: String = "/tmp/",
    streamOut: Boolean = false,
    fixGatkStdout: Boolean = false,
    cleanupTmp: Boolean = true,
    bufferSize: Int = 8192,
    encoding: String = Codec.defaultCharsetCodec.name,
    envVars: Map[String, String] = Map(),
    separateWorkingDir: Boolean = false)
  extends RDD[(String, String)](prev) with be.ugent.intec.halvade.Logging {

  val region_placeholder = "region.intervals"
  private val eof: Array[Byte] = Array(0,0,0,0,0,0,0,0,0)
  private var taskDirFile: File = null
  private var taskDirectory: String = null
  private var workInTaskDirectory: Boolean = false

  // Split a string into words using a standard StringTokenizer
  def tokenize(command: String): Seq[String] = {
    val buf = new ArrayBuffer[String]
    val tok = new StringTokenizer(command)
    while(tok.hasMoreElements) {
      buf += tok.nextToken()
    }
    buf
  }

  class NotEqualsFileNameFilter(filterName: String) extends FilenameFilter {
    def accept(dir: File, name: String): Boolean = {
      !name.equals(filterName)
    }
  }

  override def getPartitions: Array[Partition] = firstParent[(String, String)].partitions

  override def compute(split: Partition, context: TaskContext): Iterator[(String,String)] = {

    logLevel = Logger.getLogger(getClass()).getLevel()
    // When spark.worker.separated.working.directory option is turned on, each
    // task will be run in separate directory. This should be resolve file
    // access conflict issue
    taskDirectory = "tasks" + File.separator + java.util.UUID.randomUUID.toString
    debug("["+ split.index+ "] taskDirectory = " + taskDirectory)
    if (separateWorkingDir) {
      val currentDir = new File(".")
      debug("["+ split.index+ "] currentDir = " + currentDir.getAbsolutePath())
      taskDirFile = new File(taskDirectory)
      taskDirFile.mkdirs()

      try {
        val tasksDirFilter = new NotEqualsFileNameFilter("tasks")

        // Need to add symlinks to jars, files, and directories.  On Yarn we could have
        // directories and other files not known to the SparkContext that were added via the
        // Hadoop distributed cache.  We also don't want to symlink to the /tasks directories we
        // are creating here.
        for (file <- currentDir.list(tasksDirFilter)) {
          val fileWithDir = new File(currentDir, file)
            Utils.symlink(new File(fileWithDir.getAbsolutePath()),
            new File(taskDirectory + File.separator + fileWithDir.getName()))
        }
        workInTaskDirectory = true
      } catch {
        case e: Exception => {error("Unable to setup task working directory: " + e.getMessage +
          " (" + taskDirectory + ")", e); throw e; }
      }
    }

  // replace placeholders with new files in all commands!
    val rnd = new scala.util.Random()
    var my_commands = commands
    var inputFiles = ArrayBuffer[String]();
    var outputfile: String = null;
    var files = new ArrayBuffer[String]()
    for (i <- placeholders.indices) {
      val tmp = placeholders(i)
      val extension = if (tmp.contains('.')) tmp.substring(tmp.indexOf(".")) else ".tmp"
      val tmp_file = tmpFolder + "tmpfile_%d_%d%d".format(split.index, System.currentTimeMillis(), rnd.nextInt(Integer.MAX_VALUE)) + extension;
      if (tmp_file.endsWith("bam")) {
        files += tmp_file.replace(".bam", ".bai");
        files += tmp_file + ".bai";
      }
      if (tmp_file.endsWith("tmp") || tmp_file.endsWith("vcf"))
        files += tmp_file + ".idx";
      files += tmp_file
      if(i < inputSplitFilters.size) inputFiles += tmp_file  // assume input placeholder are first N file
      if(i == placeholders.length - 1) outputfile = tmp_file // assume output placeholder is last file
      my_commands = my_commands.map(x => x.replace(tmp, tmp_file))
    }
    if(regions != null)
      my_commands = my_commands.map(x => x.replace(region_placeholder, "-L "+ regions(PartitionSplit(split.index, 0)).mkString(" -L ")))

    // first x (=inputSplitFilters.size) files are input files!
    TaskContext.setTaskContext(context)
    debug("["+ split.index+ "] writing partition to " + inputFiles.mkString(", "))
    val filteridx = inputSplitFilters.zipWithIndex.toMap
    val bos = ArrayBuffer[OutputStreamWriter]();
    inputFiles.foreach(infile => {
      bos += new OutputStreamWriter(new FileOutputStream(infile))
    })
    var fsize = new Array[Int](inputSplitFilters.size)
    try {
      for (elem <- firstParent[(String, String)].iterator(split, context) ) {
        val idx = filteridx(elem._1)
        fsize(idx) += elem._2.size
        bos(idx).write((elem._2 + '\n'))
      }
    } catch {
      case t: Throwable => new IOException("error writing RDD to temporary files [" + inputFiles.mkString(", ") + "]")
    } finally {
      debug("["+ split.index+ "] has written " + fsize.mkString(", ") + " to " + inputFiles.mkString(", "))
      bos.foreach(xbos => xbos.close())
    }



    var data: Array[String] = null
    for (i <- my_commands.indices) {
      data = runCommand(split, context, i, my_commands(i), procNames(i))
      debug("["+ split.index+ "] got data from command " + i)
    }
    if(!streamOut) {
      debug("["+ split.index+ "] reading partition from " + outputfile)
      data = fromFile(outputfile).getLines.toArray // get it in mem before deleting the file...
      debug("["+ split.index+ "] output is " + data.size)
    }

    if(cleanupTmp) {
      for (file <- files) {
        debug("removing " + file)
        val f = new File(file)
        if(f.exists()) {
          if (f.isDirectory()) {
            FileUtils.deleteDirectory(f)
          }else {
            f.delete()
          }
        }
      }
    }
    // cleanup task working directory if used
    if (workInTaskDirectory) {
      scala.util.control.Exception.ignoring(classOf[IOException]) {
        Utils.deleteRecursively(new File(taskDirectory))
      }
      debug("["+ split.index+ "] Removed task working directory $taskDirectory")
    }

    data.map(x => (null, x)).iterator
  }



  // runs a command on this partition!
  def runCommand(split: Partition, context: TaskContext, idx: Int, command: String, procName: String): Array[String] = {

    val time = System.nanoTime
    val pb = new ProcessBuilder(tokenize(if(metrics) ("/usr/bin/time -v " + command) else command).asJava)
    info("["+ split.index+ "] running " + command)
    // Add the environmental variables to the process.
    val currentEnvVars = pb.environment()
    envVars.foreach { case (variable, value) => currentEnvVars.put(variable, value) }
    // for compatibility with Hadoop which sets these env variables
    // so the user code can access the input filename
    if (split.isInstanceOf[HadoopPartition]) {
      val hadoopSplit = split.asInstanceOf[HadoopPartition]
      currentEnvVars.putAll(hadoopSplit.getPipeEnvVars().asJava)
    }
    if (separateWorkingDir) {
      try {
        pb.directory(taskDirFile)
      } catch {
        case e: Exception => {error("Unable to setup task working directory: " + e.getMessage +
          " (" + taskDirectory + ")", e); throw e;}
      }
    }

    val proc = pb.start()
    val env = SparkEnv.get
    val childThreadException = new AtomicReference[Throwable](null)

    var errorstring = "";
    // Start a thread to print the process's stderr to ours
//    if(logLevel == Level.DEBUG) {
      new Thread(s"stderr reader for $command") {
        override def run(): Unit = {
          val err = proc.getErrorStream
          try {
            for (line <- Source.fromInputStream(err)(encoding).getLines) {
              debug("["+ split.index+ "] " + line)
              errorstring += line + "\n";
            }
          } catch {
            case t: Throwable => childThreadException.set(t)
          } finally {
            err.close()
          }
        }
      }.start()
//    } else {
//      errorstring = null;
//    }

    val data = Source.fromInputStream(proc.getInputStream).getLines().toArray

    val exitStatus = proc.waitFor()
//    proc.getInputStream().close();
//    proc.getOutputStream().close();
//    proc.getErrorStream().close();

    debug("["+ split.index+ "] command " + idx + " output size: " + data.size)

    // CHECK IF ANY ERRORS HAPPENED

    if (exitStatus != 0) {
      error("["+ split.index+ "] failed running " + idx + ": " + command)
      throw new IllegalStateException("Subprocess " +split.index + s" exited with status $exitStatus. " +
        s"Command failed: " + command +
                                    (if (errorstring != null)
                                      ("\n-----------------------------------------\n" + errorstring + "-----------------------------------------\n" )
                                      else ("") ) )
    }

    val t = childThreadException.get()
    if (t != null) {
      error("["+ split.index+ "] child thread exception in " + command)
      proc.destroy()
      throw t
    }

    info("["+ split.index+ "] finished " + procName + " in "+(System.nanoTime-time)/1.0e9+"s")
    data
  }
}
