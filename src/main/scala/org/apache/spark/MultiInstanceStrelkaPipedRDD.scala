package org.apache.spark

import be.ugent.intec.halvade.PartitionSplit
import java.util.concurrent.Executors
import java.util.StringTokenizer
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter, DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import java.util.concurrent.atomic.AtomicReference
import java.nio.charset.CodingErrorAction
import org.apache.spark.rdd.HadoopPartition
import org.apache.spark.util.Utils
import org.apache.spark.rdd.RDD
import org.apache.log4j.{Level, Logger}
import org.apache.commons.io.FileUtils;
import scala.reflect.ClassTag
import scala.collection.Map // ensure same maps...
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._
import scala.io.Codec.string2codec
import scala.io.Source._
import scala.io.{Codec, Source}
import be.ugent.intec.halvade.spark.rdd.SamRecordRDDFunctions._
import java.util.UUID
import be.ugent.intec.halvade.IoUtils._


// based on PipedRDD from Spark!
class MultiInstanceStrelkaPipedRDD[+T: ClassTag](
    prev: RDD[(Int, T)],
    commands: Array[String],
    placeholders: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    filteredDict: Set[String],
    numInstances: Int,
    tmpFolder: String = "/tmp/",
    cleanupTmp: Boolean = true,
    extraFiles: Map[String, String] = Map(),
    envVars: Map[String, String] = Map(),
    logLevel: Level = Level.ERROR,
    averageNormalCoverage: String = null,
    streamOut: Boolean = false
  )
  extends MultiInstanceChainedCommandPipedRDD(prev, commands, placeholders, procNames, regions, numInstances, tmpFolder,
    cleanupTmp, extraFiles, envVars, logLevel, streamOut = streamOut) {

    val strelka_tsv_placeholder = "chromdepths.tsv"
    var map_tmp_strelka_files = scala.collection.concurrent.TrieMap.empty[Int, String]  // needs to be thread safe

    override def customCleanup(number: Int, outputfile: String) : Unit = {
      debug("[" + "-" + number + "] tmp_file_map: " + map_tmp_strelka_files.mkString("\t"))
      debug("[" + "-" + number + "] requesting tmp_file_map number " + number)
      val strelkachromdepthfile = new File(map_tmp_strelka_files(number))
      if(strelkachromdepthfile.exists()) strelkachromdepthfile.delete()
    }

    override def checkForSkip(partitionSplit: PartitionSplit) : Boolean = {
       if (regions(partitionSplit).filter(x => filteredDict.contains(x.split(":")(0))).size == 0) true
       else false
    }
    override def updateCommandWithRegion(commands: Array[String], partitionSplit: PartitionSplit): Array[String] = {
      var tmp_commands = super.updateCommandWithRegion(commands, partitionSplit)
      if(tmp_commands.filter(_.contains(strelka_region_placeholder)).size > 0) {
        if(regions != null) {
          return tmp_commands.map(x => x.replace(strelka_region_placeholder, "--region " + regions(partitionSplit).filter(x => filteredDict.contains(x.split(":")(0))).mkString(" --region ")))
        } else {
        throw new Exception("strelka2 region placeholder present but no regions provided")
        }
      } else commands
    }
    override def customUpdateCommand(commands: Array[String], partitionSplit: PartitionSplit) : Array[String] = {
      if(averageNormalCoverage != null) {
        // write the stuff to the file
        val tmp_strelka_file = tmpFolder + "strelkaChromDepth" + "_%d_%d_%s".format(partitionSplit.partition, partitionSplit.split, UUID.randomUUID()) + ".tsv"
        map_tmp_strelka_files += ((partitionSplit.split, tmp_strelka_file))
        debug("[" + partitionSplit.partition + "-" + partitionSplit.split + "] added file " + tmp_strelka_file + " to tmp_file_map: " + map_tmp_strelka_files.mkString("\t"))
        val out = new PrintWriter(new File(tmp_strelka_file));
        withResources(out)(x => {
          out.write(averageNormalCoverage)
        })
        commands.map(_.replace(strelka_tsv_placeholder, tmp_strelka_file))
      } else commands
    }
}
