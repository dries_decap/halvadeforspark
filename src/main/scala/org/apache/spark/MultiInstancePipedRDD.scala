package org.apache.spark


import be.ugent.intec.halvade.PartitionSplit
import java.util.concurrent.Executors
import java.util.StringTokenizer
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter, DataOutputStream,FileOutputStream,BufferedOutputStream, BufferedInputStream, FileInputStream}
import java.util.concurrent.atomic.AtomicReference
import java.nio.charset.CodingErrorAction
import org.apache.spark.rdd.HadoopPartition
import org.apache.spark.rdd.RDD
import org.apache.log4j.{Level, Logger}
import org.apache.commons.io.FileUtils
import be.ugent.intec.halvade.IoUtils._
import be.ugent.intec.halvade.Utils.humanReadableMemory
import scala.reflect.ClassTag
import scala.collection.Map // ensure same maps...
import scala.collection.JavaConverters._
import scala.io.Codec.string2codec
import scala.io.{Codec, Source}
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import java.util.UUID

abstract class MultiInstancePipedRDD[+T: ClassTag](
    prev: RDD[T],
    commands: Array[String],
    placeholders: Array[String],
    procNames: Array[String],
    regions: Map[PartitionSplit, Array[String]],
    numInstances: Int,
    tmpFolder: String = "/tmp/",
    cleanupTmp: Boolean = true,
    extraFiles: Map[String, String] = Map(),
    envVars: Map[String, String] = Map(),
    logLevel: Level = Level.INFO,
    streamIn: Boolean = false,
    streamOut: Boolean = false,
    metrics: Boolean = false
  ) extends RDD[(Int, Array[Byte])](prev) with be.ugent.intec.halvade.Logging {

  protected val gatk_region_placeholder = "gatkregion.intervals"
  protected val strelka_region_placeholder = "strelkaregion.intervals"
  protected val region_placeholder = "region.intervals"
  protected val eof: Array[Byte] = Array(0,0,0,0,0,0,0,0,0)
  protected var bufferSize: Int = 8192
  protected var encoding: String = Codec.defaultCharsetCodec.name
  protected var fixGatkStdout: Boolean = procNames(procNames.size - 1).contains("MarkDuplicates")  || procNames(procNames.size - 1).contains("ApplyVQSR") // this is needed for ApplyVQSR / MarkDuplicates
  protected var outputs= scala.collection.concurrent.TrieMap.empty[Int, Array[Byte]]; // needs to be thread safe
  // fixed timeout
  protected val timeout = 30
  protected val units = java.util.concurrent.TimeUnit.SECONDS

  // Split a string into words using a standard StringTokenizer
  protected def tokenize(command: String): Seq[String] = {
    val buf = new ListBuffer[String]
    val tok = new StringTokenizer(command)
    while(tok.hasMoreElements) {
      buf += tok.nextToken()
    }
    buf
  }

  class NotEqualsFileNameFilter(filterName: String) extends FilenameFilter {
    def accept(dir: File, name: String): Boolean = {
      !name.equals(filterName)
    }
  }

  protected def cleanupFiles(files: ListBuffer[String], hasError: Boolean = false) = {
    for (file <- files) {
      val f = new File(file)
      if(f.exists()) {
        if (f.isDirectory()) {
          FileUtils.deleteDirectory(f)
        } else {
          if(hasError) {
            info("error detect. Checking filesize of " + file + ": " + f.length)
            if(file.endsWith("sam")){
              debug("SAM file check")
              try{
                val lsout = scala.sys.process.Process("ls -l " + file).!!
                debug("> ls -l " + file + "\n" + lsout)
              } catch {
                case e: Exception => {
                  info("error in ls -l :" + e.getMessage())
                }
              }
              try{
                val headout = scala.sys.process.Process("head -n 500 " + file).!!
                debug("> head -n 500 " + file + "\n" + headout)
              } catch {
                case e: Exception => {
                  debug("error in head :" + e.getMessage())
                }
              }
            }
          }
          if (file.endsWith("bam")) { // also remove indexes of bam files
            val bai = new File(file.replace(".bam", ".bai"))
            if(bai.exists()) bai.delete()
            val bambai = new File(file + ".bai")
            if(bambai.exists()) bambai.delete()
            val bamcsi = new File(file + ".csi") // this is the index from samtools --write-index used in strelka
            if(bamcsi.exists()) bamcsi.delete()
          }
          if (file.endsWith("tmp") || file.endsWith("vcf")) { // also remove indexes of vcf files
            val vcfidx = new File(file + ".idx")
            if(vcfidx.exists()) vcfidx.delete()
          }
          val res = f.delete()
          if(res) info("successfully removed " + file)
        }
      }
    }
  }

  override def getPartitions: Array[Partition] = firstParent[(Int, Any)].partitions

  protected def setLogLevel() {
      if(logLevel != null && logLevel != Level.ERROR) {
          Logger.getLogger("be").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.ChromosomeRegionRangePartitioner").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.GenotypeBinaryPipedMergedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstanceBaseRecalibratorPipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstanceChainedCommandPipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstanceCogroupedPipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstanceMutectPipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstancePipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstanceSinglePipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.MultiInstanceStrelkaPipedRDD").setLevel(logLevel)
          Logger.getLogger("org.apache.spark.StringPipedRDD").setLevel(logLevel)
      }
  }

  protected def streamDataToInputFile(split: Partition, number: Int, data: Any, inputfile: String) {
    debug("["+ split.index+ "] writing partition to " + inputfile)
    val out = new BufferedOutputStream(new FileOutputStream(inputfile))
    var fsize = 0
    withResources(out)(x => {
      data match {
        case bytearray: Array[Byte] => {
          out.write(bytearray)
          fsize = bytearray.size
        }
        case stringarraybuffer: ArrayBuffer[_] => {
          for (line <- stringarraybuffer.asInstanceOf[ArrayBuffer[String]]) {
            out.write((line + '\n').getBytes)
            fsize += line.size + 1
          }
        }
        case _ => throw new Exception("data format " + data.getClass + " not yet supported in MultiInstanceChainedCommandPipedRDD")
      }
    })
    info("["+ split.index+ "-" + number + "] has written " + humanReadableMemory(fsize) + " to " + inputfile)
  }

  protected def customUpdateCommand(commands: Array[String], partitionSplit: PartitionSplit) : Array[String] = commands // default dont update
  protected def updateCommandWithRegion(commands: Array[String], partitionSplit: PartitionSplit): Array[String] = {
    if(commands.filter(_.contains(gatk_region_placeholder)).size > 0) {
      if(regions != null) {
      return commands.map(x => x.replace(gatk_region_placeholder, "-L " + regions(partitionSplit).mkString(" -L ")))
      } else {
      throw new Exception("region placeholder present but no regions provided")
      }
    } else commands
  }
  protected def createFilesForPlaceholders(split: Partition, number: Int) : (ListBuffer[String], ListBuffer[String], String, String, Array[String]) = { // comands
      val tmp_files = new ListBuffer[String]()
      val possible_keep_files = new ListBuffer[String]()
      var inputfile : String = null
      var outputfile : String = null
      var my_commands = commands
      // replace placeholders with new files in all commands!
      for (i <- placeholders.indices) {
        val placeholder = placeholders(i)
        val extension = if (placeholder.contains('.')) placeholder.substring(placeholder.indexOf(".")) else ".tmp"
        val prefix = if (placeholder.contains('_')) (placeholder.substring(0, placeholder.indexOf("_"))) else "tmpfile"
        val tmp_file = tmpFolder + prefix + "_%d_%d_%s".format(split.index, number, UUID.randomUUID()) + extension;
        // info("replacing " + placeholder + " with " + tmp_file)
        if(prefix != "tmpfile") {
          possible_keep_files += tmp_file
        } else {
          tmp_files += tmp_file
        }
        if(i == 0) inputfile = tmp_file  // assume input placeholder is first file
        if(i == placeholders.length - 1) outputfile = tmp_file // assume output placeholder is last file
        my_commands = my_commands.map(x => x.replace(placeholder, tmp_file))
      }
      // add region if needed!
      my_commands = updateCommandWithRegion(my_commands, PartitionSplit(split.index, number))
      my_commands = customUpdateCommand(my_commands, PartitionSplit(split.index, number))
      // add extra files, i.e. merged bqsr table
      for( (placeholder, filecontents) <- extraFiles) {
        val extension = if (placeholder.contains('.')) placeholder.substring(placeholder.indexOf(".")) else ".tmp"
        val prefix = if (placeholder.contains('_')) (placeholder.substring(0, placeholder.indexOf("_"))) else "tmpfile"
        val tmp_file = tmpFolder + prefix + "_%d_%d_%s".format(split.index, number, UUID.randomUUID()) + extension;
        if(prefix != "tmpfile") {
          possible_keep_files += tmp_file
        } else {
          tmp_files += tmp_file
        }
        new PrintWriter(tmp_file) { write(filecontents); close }
        my_commands = my_commands.map(x => x.replace(placeholder, tmp_file))

      }

      debug("[" + split.index+ "-" + number + "] inputfile: " + inputfile + ", outputfile: " + outputfile + " for " + procNames.mkString(", "))

      (tmp_files, possible_keep_files, inputfile, outputfile, my_commands)
  }

  protected def fixGatkStdOut(split: Partition, number: Int, data: Array[Byte]) : Array[Byte] = {
    val idx = data.lastIndexOfSlice(eof)
    var n = 0 // should I add eof to data if it isn't found?
    if(idx > 0)
      n = data.size - idx - eof.size
    debug("["+ split.index+ "-" + number + "] fixing bam by dropping last " + n + " bytes")
    data.dropRight(n)
  }

  protected def setEnvironment(split: Partition, pb: ProcessBuilder) {
    // Add the environmental variables to the process.
    val currentEnvVars = pb.environment()
    envVars.foreach { case (variable, value) => currentEnvVars.put(variable, value) }
    // for compatibility with Hadoop which sets these env variables
    // so the user code can access the input filename
    if (split.isInstanceOf[HadoopPartition]) {
      val hadoopSplit = split.asInstanceOf[HadoopPartition]
      currentEnvVars.putAll(hadoopSplit.getPipeEnvVars().asJava)
    }

  }

  // runs a command on this partition!
  protected def customCleanup(number: Int, outputfile: String) : Unit = {} // default dont do anything

  // this must be implemented!
  def compute(split: Partition, context: TaskContext): Iterator[(Int, Array[Byte])]
}
