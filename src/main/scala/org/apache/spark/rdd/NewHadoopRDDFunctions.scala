package org.apache.spark.rdd

import org.apache.hadoop.fs.Path

object NewHadoopRDDFunctions {

  def getReadGroupIdsBasedOnFolder[T](input: RDD[T]) : Array[String] = {
    input.partitions.map(split => {
      val p =new Path(split.asInstanceOf[org.apache.spark.rdd.NewHadoopPartition].serializableHadoopSplit.value.toString())
      p.getParent.getName
    }).distinct
  }
}
