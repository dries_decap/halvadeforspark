# Calling somatic variants on your Spark cluster with Halvade

## Requirements
Halvade on Spark (HoS) runs on Apache Spark version 2.1 or newer. The Spark cluster should be installed on your current machine or cluster.
Additionally, Halvade requires the reference files of the genome that is being analyzed (here we take the human genome reference hg38). Run this to download and create the required files (from the GATK resource storage):

```
# download reference files from https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0
# needed: dbsnp.vcf + idx / fasta / dict / fasta.fai

wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf
wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf.idx
wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dict
wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta
wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta.fai

./bwa index Homo_sapiens_assembly38.fasta
```

A directory of binaries that are used in HoS is also required (they should be present on all machines at the same location), these are the required binaries:

- bwa
- samtools
- GATK 4.0 or newer jar file

With this you should have 2 separate folders, one containing the required reference files and the other the required binaries. These folders will be given as arguments to HoS.


## Spark-submit
Running HoS through `spark-submit` requires setting all the configurations in the submit command. Here is an example of how HoS can be started using the `spark-submit` command on a 2 node cluster with 20 CPU cores and ~150 GBytes RAM. Some Spark configurations that should be set are `the number of executors per node`, `the total number of executors`, `cores per executor`, `executor memory`, but most importantly the `executor memoryOverhead` should be set to at least `6 GBytes` to be able to run the tools in that executor memory.

```
spark-submit --class be.ugent.intec.halvade.DnaPipeline \
             --master yarn \
             --deploy-mode client \
             --driver-memory 4G \
             --executor-memory 6G \
             --total-executor-cores 40 \
             --executor-cores 20 \
             --num-executors 19 \
             --conf "spark.yarn.executor.memoryOverhead=8192" \
             --conf "spark.task.cpus=2" \
             spark-genomics-lib-assembly-1.3.jar -i $i -o $o --fastq_partitions $p1 --genomic_partitions $p2 -t $excores -m $m -r $ref -b $b -s $s --overwrite
```

where `$i` and `$o` are respectivly the input and output folders. `$p1` and `$p2` are the number of partitions for the alignment and the variant calling respectively. For a typical 30x coverage sample `350` and `800` are good values, while a 50x coverage runs fine with the values set to `600` and `1400`. `$excores` are the number of CPU cores available per executor, `$m` is the memory (in GBytes) per executor and should be set to `0.9*executor-memory`, in this case 5.4 GBytes. `$ref`, `$b`, `$s` are respectively the location of the reference file, the binaries directory and the dbsnp file. And `$readid` is the name of the read id's for the Illumina reads.


### Configuring Spark memory
Memory usage in Spark is determined by the memory and the memoryOverhead given to the executors. The memory usage depends on the input file, if these are unaligned reads, then a minimum of 6GB of memory and 10GB of overhead memory is advised. However, when the input are aligned reads (BAM) then the bwa step is not run and the overhead can be lowered to about 5GB. These numbers are estimates and should work for regular human samples.

## HoS run script
Running HoS has been simplified in this script. The number of CPU cores and memory per machine needs to be set to your specifications. This is done here:
```bash
CPU_CORES=20
MEMORY=160 # in GB
```

This scripts needs several arguments:
- normal input
- tumor input
- output
- FASTA reference
- DBSNP file
- binaries directory

## Preprocessing
This can be done automatically by Halvade when you give manifest files as input files, which contains a tab separated list of FASTQ files. These files will then be preprocessed by Halvade and subsequently the somatic variants will be called. This can also be done with a `spark-submit` job with `be.ugent.intec.halvade.Preprocess` as the class.

## Options
| Argument | Type | Description |
|----------|------|-------------|
| --normal   | String| **[Required]** Gives the location of the input file(s) of the 'normal' sample. This can be a folder of preprocessed FASTQ files, a BAM file, a manifest file (per line a tab separated pair of FASTQ files). |
| --tumor   | String| **[Required]** Gives the location of the input file(s) of the 'tumor' sample. This can be a folder of preprocessed FASTQ files, a BAM file, a manifest file (per line a tab separated pair of FASTQ files). |
| --output/-o | String | **[Required]** This is the full path of the output VCF file. This file will be automatically gzipped, adding gz as extention is not required. |
| --threads/-t | Int | **[Required]** Set the number of threads per partition. |
| --memory/-m | Int | **[Required]** Set the avialabe memory for the tools to run in the executor (in GB). |
| --reference/-r | String | **[Required]** Give the full path of the reference FASTA file (needs to be accessible on all nodes). |
| --bindir/-b | String| **[Required]** Give the full path of the directory containing all required binaries (needs to be accessible on all nodes). |
| --knownsites/-s | String | **[Required]** Give the full path of the corresponding dbSNP file (VCF). |
| --fastq_partitions | String | Use this option if you want to specifically set the number of partitions that the unaligned reads will be split in. This is done automatically if not provided. |
| --genomic_partitions | String | Use this option if you want to specifically set the number of partitions to split the reference in, influences the performance. This is done automatically based on the input size if not provided. |
| --sample | String | Give the sample name that should be given to the input files if FASTQ files are given. By default the tumor will be `${sample}-tumor` and the normal will be named `${sample}-normal`. Is by default set to 'sample' and won't be used if the input is an aligned BAM file.
| --lanes | Int | The number of lanes used to sequence the input data, which will be used in the BQSR step. Only used for Illumina data, if you are unsure, do not provide this argument.
| --overwrite | n/a | With this option the output file is overwritten if it already exists. |
| --use_elprep | n/a | Use elPrep when possible, marking duplicates and/or base quality score recalibration. |
| --skip_bqsr | n/a | Skips the entire base quality score recalibration step. |
| --get_regions | n/a | Does not perform any steps past alignment and prints a list of the used genomic regions to `stdout`.|
| --java_serializer | n/a | Use the java serializer instead of the default kryo serializer. Might have a slight impact on performance. |
| --log_level | String | Set the logging level to one of these values: INFO, DEBUG, WARN, ERROR. The default is ERROR. |
| --help | n/a | Gives an overview of all arguments |
