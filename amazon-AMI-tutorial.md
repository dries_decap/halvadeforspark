# Halvade on Amazon

Theres is two options to start Halvade on Amazon AWS, either through the AWS Console or the AWS cli. Both are described below.

## Preparation
- Use the [S3 AWS console](https://s3.console.aws.amazon.com/s3) to upload the following files:
    - [halvade-aws-helper-assembly-1.0.jar](https://bitbucket.org/dries_decap/halvadeforspark/raw/4987223bffe840cbfeeae8371d45b29a219c8721/aws_helper/halvade-aws-helper-assembly-1.0.jar), only required once the first time
    - your two fastq input files


- Add an AIM role that lets EC2 instances access your S3 via your [IAM AWS console](https://console.aws.amazon.com/iam). Open the Roles tab and click `Create role`
    - Choose the *EC2 service*
    - Next find the *AmazonS3FullAccess* and add this policy
    - Optionally add the tags you want
    - set the Role name to i.e. *Ec2FullS3Access*



## Start Halvade using the Amazon AWS Console
In the EMR console, click on *Create cluster* and then go to the *advanced options* to configure cluster.
### Software and steps
- In the software Configuration select only *Spark* and *Hadoop*:

![](images/AmazonAMI-1.PNG)

- Then you will have to add a step *custom JAR* to start Halvade with. Select the `halvade-aws-helper-assembly` jar and give the two input files and the output file as s3 URLs as arguments. Make sure the Terminate the cluster on failure, to avoid unnecessary costs.

![](images/AmazonAMI-1-custom-jar.PNG)

- Also terminate the cluster automatically after completion:

![](images/AmazonAMI-1-custom-jar-2.PNG)

### Hardware
- In the hardware settings, choose **30GB** for the root device EBS volume and select only one Master node with an additional **1000GB** EBS volume.

![](images/AmazonAMI-2-EBS-volume.PNG)

- Also select the type of machine you want to use here, i.e. *r5.8xlarge*, here you can also opt to use spot instances. Make sure you set the number of additional nodes in the Core node type to 0. Halvade can run on more than one node but isn't configured optimally for it in this AMI.

![](images/AmazonAMI-2.PNG)

## General cluster settings
- Here you just need to select the custom AMI ID. **The AMI ID in the image below is outdated, use this id: ami-0faf15d5e4cc2af53**. Select enter AMI ID and fill in this ID.

![](images/AmazonAMI-3.PNG)

## security
- The most important thing here is to set the EC2 instance profile to the custom IAM role you created, i.e. *Ec2FullS3Access* that has *S3* access from the EC2 instances.

![](images/AmazonAMI-4.PNG)

- If you want `ssh` access for monitoring/debugging purposes be sure to select your EC2 key pair and enable the ssh port in your security groups.


## Create Cluster
Now click *Create cluster*, this should start everything for you and terminate once the process has completed.


## Start Halvade using the aws cli
Install the AWS CLI and configure with your account by following [this guide](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html). Then run this command, adapted for your file locations and account specific parameters.

``` bash
fq1="s3://halvade-io/input/na12878-30x.r1.fq.gz"
fq2="s3://halvade-io/input/na12878-30x.r2.fq.gz"
out="s3://halvade-io/output/na12878-30x.fq.gz"
halvade_helper="s3://halvade-io/lib/halvade-aws-helper-assembly-1.0.jar"
log_uri="s3n://aws-logs-<myid>-eu-west-1/elasticmapreduce/"
ami_id="ami-0faf15d5e4cc2af53"
node_type="r5.8xlarge"
region="eu-west-1"

aws emr create-cluster --termination-protected --applications Name=Hadoop Name=Spark \
--ec2-attributes '{"InstanceProfile":"Ec2FullS3Access"}' --release-label emr-5.28.0 --log-uri '$log_uri' \
--steps '[{"Args":["$fq1","$fq2","$out"],"Type":"CUSTOM_JAR","ActionOnFailure":"TERMINATE_CLUSTER","Jar":"$halvade_helper","Properties":"","Name":"Halvade"}]' \
--instance-groups '[{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":1000,"VolumeType":"gp2"},"VolumesPerInstance":1}],"EbsOptimized":true},"InstanceGroupType":"MASTER","InstanceType":"$node_type","Name":"Master - 1"}]' \
--auto-terminate --custom-ami-id $ami_id --auto-scaling-role EMR_AutoScaling_DefaultRole \
--ebs-root-volume-size 30 --service-role EMR_DefaultRole --enable-debugging \
--repo-upgrade-on-boot SECURITY --name 'My cluster' --scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
--region $regions

```

This command starts the cluster and will terminate when completed.
