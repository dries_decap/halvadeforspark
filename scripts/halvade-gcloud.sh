#!/bin/bash

help="halvade-gcloud.sh help:\n\
halvade-gcloud.sh germline germlineIn outputFolder [options]\n\
halvade-gcloud.sh somatic tumorIn normalIn outputFolder [options]\n\
options:\n\
\t--exome: run the exome pipeline (less partitions)\n\
\t--tmpdir <string>: set the dir for tmp files\n\
\t--germlineSM <string>: germline samplename\n\
\t--tumorSM <string>: tumor samplename\n\
\t--normalSM <string>: normal samplename\n\
\t--nodes <int>: number of worker nodes\n\
\t--executor_memory <int>: sets the memory in MB per executor\n\
\t--executor_cpus <int>: sets the number of CPUs per executor\n\
\t--keep_cluster: Do not terminate the cluster after Halvade completes. Allows to run other samples on this cluster afterwards\n\
\t--ssh: Use ssh command instead of pig job to start the halvade job. WARNING: Deletes the cluster on completion (unless --keep_cluster is given) in this script, so if this script is interrupted the cluster will not be deleted.\n\
\t--cluster <string>: existing cluster id to run Halvade on (halvade bootstrap script should have been called on every node in this cluster), also enables --keep_cluster\n\
\t--partitions <int>: set number of partitions\n\
Additional Halvade options can be set with the variable \${HALVADE_OPTS} like this:\n\
\tHALVADE_OPTS=\"--variant_caller both\""

if [[ "$#" == "0" ]]; then
  echo -e $help
  exit;
fi

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

function parseOptions() {
  # parse options
  while [[ $# -gt 0 ]]
  do
    key="$1"
    case $key in
        --exome)
          EXOME_PIPELINE="true"
          shift # past argument
          ;;
        --germlineSM)
          SM="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_memory)
          EXECUTOR_MEMORY="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_cpus)
          TASK_CPUS="$2"
          shift # past argument
          shift # past value
          ;;
        --partitions)
          PARTITIONS="$2"
          shift # past argument
          shift # past value
          ;;
        --tumorSM)
          TUMOR_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --normalSM)
          NORMAL_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --nodes)
          NR_NODES="$2"
          shift # past argument
          shift # past value
          ;;
        --cluster)
          CLUSTER_ID="$2"
          KEEP_CLUSTER="keep_cluster"
          shift # past argument
          shift # past value
          ;;
        --ssh)
          USE_SSH_COMMAND="true"
          shift # past argument
          ;;
        --keep_cluster)
          KEEP_CLUSTER="keep_cluster"
          shift # past ar gument
          ;;
        *)    # unknown option
          echo "unknown option $1"
          exit 100;
          ;;
    esac
  done
}


# CHECK REQUIRED INPUT
function checkGCLOUDvariables() {
  # OPTIONAL USER INPUT, set default if not given
  GCLOUD_CONFIG=${GCLOUD_CONFIG:-gcloud-config.sh}
  WORKER_INSTANCE_COUNT=${WORKER_INSTANCE_COUNT:-1}
  WORKER_INSTANCE_TYPE=${WORKER_INSTANCE_TYPE:-n2-highmem-32} # or m1-ultramem-40 which should be default?
  MASTER_INSTANCE_TYPE=${MASTER_INSTANCE_TYPE:-c2-standard-4}
  if [[ -f $GCLOUD_CONFIG ]]; then
    . ${GCLOUD_CONFIG}
  else
    echo "no config file '${GCLOUD_CONFIG}' found"; exit 10;
  fi
  if [[ ! -z $NR_NODES ]]; then
    WORKER_INSTANCE_COUNT=$NR_NODES
  fi
  CUSTOM_GCLOUD_CONFIG=""
  CUSTOM_GCLOUD_NOREGION_CONFIG=""
  CUSTOM_GCLOUD_NOZONE_CONFIG=""
  if [[ -z $GCLOUD_REGION ]]; then
    GCLOUD_REGION=$(gcloud config get-value dataproc/region);
    if [[ -z $GCLOUD_REGION ]]; then
      echo "default google dataproc region not set and not configured."; exit 11;
    fi
  else
    CUSTOM_GCLOUD_CONFIG="$CUSTOM_GCLOUD_CONFIG --region $GCLOUD_REGION"
    CUSTOM_GCLOUD_NOZONE_CONFIG="$CUSTOM_GCLOUD_NOZONE_CONFIG --region $GCLOUD_REGION"
  fi
  if [[ -z $GCLOUD_ZONE ]]; then
    GCLOUD_ZONE=$(gcloud config get-value compute/region);
    if [[ -z $GCLOUD_ZONE ]]; then
      echo "default google compute zone not set and not configured."; exit 12;
    fi
  else
    CUSTOM_GCLOUD_CONFIG="$CUSTOM_GCLOUD_CONFIG --region $GCLOUD_ZONE"
    CUSTOM_GCLOUD_NOREGION_CONFIG="$CUSTOM_GCLOUD_NOREGION_CONFIG --region $GCLOUD_ZONE"
  fi
  if [[ -z $GCLOUD_PROJECT ]]; then
    GCLOUD_PROJECT=$(gcloud config get-value core/project);
    if [[ -z $GCLOUD_PROJECT ]]; then
      echo "default google cloud project not set and not configured."; exit 13;
    fi
  else
    CUSTOM_GCLOUD_CONFIG="$CUSTOM_GCLOUD_CONFIG --project $GCLOUD_PROJECT"
    CUSTOM_GCLOUD_NOREGION_CONFIG="$CUSTOM_GCLOUD_NOREGION_CONFIG --project $GCLOUD_PROJECT"
    CUSTOM_GCLOUD_NOZONE_CONFIG="$CUSTOM_GCLOUD_NOZONE_CONFIG --project $GCLOUD_PROJECT"
  fi
  if [[ ! -z $CUSTOM_GCLOUD_CONFIG ]]; then
    echo "Google cloud config is set to project ${GCLOUD_PROJECT}, region ${GCLOUD_REGION}, zone ${GCLOUD_ZONE}"
  fi
  # GCLOUD_REGION=eu-west-1
  # GCLOUD_ZONE=eu-west-1-d
  # GCLOUD_PROJECT=halvade-project
  if [[ -z $GCLOUD_LIB_DIR ]]; then
    echo "no gs lib dir provided"; exit 3;
  fi
  if [[ -z $GCLOUD_REF_DIR ]]; then
    echo "no gs ref dir provided"; exit 4;
  fi
  if [[ -z $GCLOUD_BIN_DIR ]]; then
    echo "no gs bin dir provided"; exit 5;
  fi
  if [[ $GCLOUD_LIB_DIR != */ ]]; then GCLOUD_LIB_DIR="${GCLOUD_LIB_DIR}/"; fi
  LIB_FILES=$(gsutil ls ${GCLOUD_LIB_DIR} | awk '{print $NF}')
  if [[ -z $(echo $LIB_FILES | grep "gcloud-bootstrap.sh") ]]; then echo "gcloud-bootstrap.sh not found in gs library folder."; exit 7; fi

  # check memory/cpus of nodes
  CPU_MEMORY=$(gcloud compute machine-types describe ${WORKER_INSTANCE_TYPE} --format="csv[no-heading](guestCpus,memoryMb)")
  IFS=, read RESOURCE_CPU RESOURCE_MEMORY_MB <<< ${CPU_MEMORY}
  RESOURCE_MEMORY=$(( RESOURCE_MEMORY_MB / 1024))
  if [[ -z $RESOURCE_MEMORY ]]; then
    echo "unable to detect resource memory of ${WORKER_INSTANCE_TYPE}"; exit 8;
  fi
  if [[ -z $RESOURCE_CPU ]]; then
    echo "unable to detect resource vCPUs of ${WORKER_INSTANCE_TYPE}"; exit 9;
  fi
  MIN_STORAGE=1000 # for performance
  if [[ $WORKER_INSTANCE_COUNT == "1" ]]; then
    WORKER_BOOT_DISK_SIZE=${MIN_STORAGE}
    MASTER_GROUP=""
    WORKER_GROUP="--num-masters=1 --master-machine-type=${WORKER_INSTANCE_TYPE} --master-boot-disk-size=${WORKER_BOOT_DISK_SIZE}GB --single-node "
  else
    MASTER_BOOT_DISK_SIZE=50 # doesnt really require big IO performance on master typically
    WORKER_BOOT_DISK_SIZE=${MIN_STORAGE}
    MASTER_GROUP="--num-masters=1  --master-machine-type=${MASTER_INSTANCE_TYPE} --master-boot-disk-size=${MASTER_BOOT_DISK_SIZE}GB"
    WORKER_GROUP="--num-workers=${WORKER_INSTANCE_COUNT} --worker-machine-type=${WORKER_INSTANCE_TYPE} --worker-boot-disk-size=${WORKER_BOOT_DISK_SIZE}GB"
  fi
}


# check if input is ok
function getInput() {
  local INPUT=$1
  if [[ $INPUT == *bam ]]; then # should also work if it is on gs
    NEWINPUT=$INPUT
  else
    if [[ $INPUT != */ ]]; then INPUT="${INPUT}/"; fi
    FILES=$(gsutil ls $INPUT | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq|bam)$")
    if [ -z "$FILES" ]; then
      # check for subdirectories with fq files
      FILES=$(for f in $(gsutil ls $INPUT | grep "PRE" | awk '{print $NF}'); do gsutil ls ${INPUT}$f; done | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq)$")
      if [ -z "$FILES" ]; then
        echo "no FASTQ files found in ${INPUT} or its subdirectories";
        exit 6;
      else
        NEWINPUT=$INPUT # should be preprocessed with a folder per read group
      fi
    else
      # needs to be processed but should be valid info
      NEWINPUT=$INPUT
    fi
  fi
}

# read arguments and process
echo "CHECKING INPUT AND CONFIGURATION..."
MODE=$1
shift
if [[ "$MODE" = "germline" ]]; then
  if [[ "$#" -lt "2" ]]; then
    echo -e $help
    exit
  fi
  getInput $1; shift;
  GERMLINE=$NEWINPUT
  OUTPUT=$1; shift;
  SM="SAMPLE"
  parseOptions "$@"
  # CONFIGURE GCLOUD
  checkGCLOUDvariables
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $INPUT == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $INPUT == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  REF_DIR=/halvade/ref/
  BIN_DIR=/halvade/bin/
  HALVADE_SCRIPT=/halvade/lib/halvade.sh
  HALVADE_ARGUMENTS="germline ${REF_DIR} ${BIN_DIR} ${GERMLINE} ${OUTPUT}"
  if [[ ! -z $TMP_DIR ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --tmpdir ${TMP_DIR}"; fi
  if [[ ! -z $PARTITIONS ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --partitions ${PARTITIONS}"; fi
  if [[ ! -z $EXOME_PIPELINE ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --exome"; fi
  if [[ "$SM" != "SAMPLE" ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --germlineSM ${SM}"; fi
  if [[ ! -z "$EXECUTOR_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_memory $EXECUTOR_MEMORY"; fi
  if [[ ! -z "$TASK_CPUS" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_cpus $TASK_CPUS"; fi
  HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --memory ${RESOURCE_MEMORY} --cpus ${RESOURCE_CPU} --nodes ${WORKER_INSTANCE_COUNT}"
elif [[ "$MODE" = "somatic" ]]; then
  if [[ "$#" -lt "3" ]]; then
    echo -e $help
    exit
  fi
  getInput $1; shift;
  TUMOR_IN=$NEWINPUT
  getInput $1; shift;
  NORMAL_IN=$NEWINPUT
  OUTPUT=$1; shift;
  TUMOR_SM="TUMOR_SM"
  NORMAL_SM="NORMAL_SM"
  parseOptions "$@"
  # CONFIGURE GCLOUD
  checkGCLOUDvariables
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  REF_DIR=/halvade/ref/
  BIN_DIR=/halvade/bin/
  HALVADE_SCRIPT=/halvade/lib/halvade.sh
  HALVADE_ARGUMENTS="somatic ${REF_DIR} ${BIN_DIR} ${TUMOR_IN} ${NORMAL_IN} ${OUTPUT}"
  if [[ ! -z $TMP_DIR ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --tmpdir ${TMP_DIR}"; fi
  if [[ ! -z $PARTITIONS ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --partitions ${PARTITIONS}"; fi
  if [[ ! -z $EXOME_PIPELINE ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --exome"; fi
  if [[ "$TUMOR_SM" != "TUMOR_SM" ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --tumorSM ${TUMOR_SM}"; fi
  if [[ "$NORMAL_SM" != "NORMAL_SM" ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --normalSM ${NORMAL_SM}"; fi
  if [[ ! -z "$EXECUTOR_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_memory $EXECUTOR_MEMORY"; fi
  if [[ ! -z "$TASK_CPUS" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_cpus $TASK_CPUS"; fi
  HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS} --memory ${RESOURCE_MEMORY} --cpus ${RESOURCE_CPU} --nodes ${WORKER_INSTANCE_COUNT}"
else
  echo -e $help
  exit
fi


if [[ -z $CLUSTER_ID ]]; then
  if [[ ! -z $HALVADE_OPTS ]]; then
    meta_halvade_opts=",halvade_opts=${HALVADE_OPTS// /;}" # // /; replaces all spaces with ; instead of just one (double slash needed in front for global)
  fi
  if [[ -z $KEEP_CLUSTER && -z $USE_SSH_COMMAND ]]; then
    DELETE_CLUSTER="--max-idle=5m" # min of 5min after last job
  fi
  NAME="halvadegcloud" # no spaces!
  CLUSTER_ID=${NAME}$(echo $(uuidgen) | awk -F"-" '{print $1}')
  CMD="gcloud dataproc clusters create ${CLUSTER_ID} ${MASTER_GROUP} ${WORKER_GROUP} \
      --image-version 2.0-ubuntu18 ${CUSTOM_GCLOUD_CONFIG} ${DELETE_CLUSTER} \
      --initialization-actions=${GCLOUD_LIB_DIR}gcloud-bootstrap.sh \
      --metadata=gs_ref_folder=${GCLOUD_REF_DIR},gs_bin_folder=${GCLOUD_BIN_DIR},gs_lib_folder=${GCLOUD_LIB_DIR},halvade_args=${HALVADE_ARGUMENTS// /;}${meta_halvade_opts}"

  # start cluster
  echo "STARTING CLUSTER ${CLUSTER_ID}"
  echo $CMD
  $CMD
  CMD_RESULT=$?
  if [[ $CMD_RESULT -ne 0 ]]; then
    echo "FAILED STARTING CLUSTER; EXITING"
    exit 14;
  fi
fi

if [[ -z ${USE_SSH_COMMAND} ]]; then
  echo "STARTING JOB:"
  PIG_JOB="sh /halvade/lib/halvade.sh"
  JOB="gcloud dataproc jobs submit pig --execute \"$PIG_JOB\" --cluster ${CLUSTER_ID} ${CUSTOM_GCLOUD_NOZONE_CONFIG}"
  echo $JOB
  gcloud dataproc jobs submit pig --execute "$PIG_JOB" --cluster ${CLUSTER_ID} ${CUSTOM_GCLOUD_NOZONE_CONFIG}
  JOB_RESULT=$?
  if [[ $JOB_RESULT -ne 0 ]]; then
    echo "HALVADE JOB FAILED"
    exit 15;
  fi
else
  echo "STARTING JOB:"
  JOB="gcloud compute ssh ${CLUSTER_ID}-m ${CUSTOM_GCLOUD_NOREGION_CONFIG}  --command '/halvade/lib/halvade.sh'" # should detect arguments through the gce metadata
  echo $JOB
  $JOB
  JOB_RESULT=$?
  # delete job
  if [[ -z $KEEP_CLUSTER ]]; then
    echo "DELETING CLUSTER $CLUSTER_ID"
    gcloud dataproc clusters delete ${CLUSTER_ID} ${CUSTOM_GCLOUD_NOZONE_CONFIG} --quiet
  fi
  if [[ $JOB_RESULT -ne 0 ]]; then
    echo "HALVADE JOB FAILED"
    exit 15;
  fi
fi
