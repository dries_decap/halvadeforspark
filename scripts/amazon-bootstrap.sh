#!/usr/bin/bash

# this script is run on all nodes

help="amazon-bootstrap.sh referenceFolder BinariesFolder librariesFolder:\n\
referenceFolder: The S3 URI to the folder containing all necessary reference files. \n\
binariesFolder: The S3 URI to the folder with the required binaries: bwa, samtools, GATK4, Halvade and Strelka (optional)\n\
librariesFolder: The S3 URI to the folder containing the halvade jar, halvade runscript and the htsjdk and hadoopbam libraries.\n"

function checkURLs(){
  FOLDER=$1
  if [[ -z $FOLDER ]]; then
    echo "no folder given"; exit 2;
  else
    if [[ -z $(echo ${FOLDER} | grep -E "^s3(a|n)?:")  ]]; then
      echo "not an S3 folder: " $FOLDER; exit 3;
    fi
  fi
}

function getLocalFolders() {
  if [[ -z "$EBR_MOUNT" ]]; then
    # assume local ssds that need to be mounted
    LOCAL_REF_FOLDER="/mnt/ref"
    LOCAL_BIN_FOLDER="/mnt/bin"
    LOCAL_LIB_FOLDER="/mnt/lib"
  else
    LOCAL_REF_FOLDER="$EBR_MOUNT/ref"
    LOCAL_BIN_FOLDER="$EBR_MOUNT/bin"
    LOCAL_LIB_FOLDER="$EBR_MOUNT/lib"
  fi
}

function unTarIfNeeded() {
  # check if things need to be untarred
  cd $LOCAL_REF_FOLDER
  for f in $(find ./ -maxdepth 1 -mindepth 1 -name *.tar.gz); do
    echo $f;
    tar xvf $f;
    rm -f $f;
  done
  cd $LOCAL_BIN_FOLDER
  for f in $(find ./ -maxdepth 1 -mindepth 1 -name *.tar.gz); do
    echo $f;
    tar xvf $f;
    rm -f $f;
  done
  cd $LOCAL_LIB_FOLDER
  for f in $(find ./ -maxdepth 1 -mindepth 1 -name *.tar.gz); do
    echo $f;
    tar xvf $f;
    rm -f $f;
  done
}



function checkRequiredFiles() {
  NOT_FOUND=""
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "htsjdk.*jar") ]]; then NOT_FOUND="htsjdk jar"; fi
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "hadoop-bam.*jar") ]]; then NOT_FOUND="${NOT_FOUND}, hadoop-bam jar"; fi
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "halvade.*jar") ]]; then NOT_FOUND="${NOT_FOUND}, halvade jar"; fi
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "halvade.sh") ]]; then NOT_FOUND="${NOT_FOUND}, halvade.sh"; fi
  if [[ ! -z ${NOT_FOUND} ]]; then
    echo "${NOT_FOUND} not found, downloading halvade bundle";
    cd ${LOCAL_LIB_FOLDER}
    if [[ -z $(which wget) ]]; then echo "installing wget"; apt-get install -y wget; fi
    wget https://bitbucket.org/dries_decap/halvadeforspark/downloads/halvade-cloud-bundle.tar.gz
    tar xvf halvade-cloud-bundle.tar.gz
    rm -f halvade-cloud-bundle.tar.gz
    ls -l $LOCAL_LIB_FOLDER
  fi
  # check if bwa/samtools executable
  ${LOCAL_BIN_FOLDER}/samtools
  if [[ $? == "127" ]]; then echo "error running samtools"; exit 6; fi # error 127 means non executable
  ${LOCAL_BIN_FOLDER}/bwa
  if [[ $? == "127" ]]; then echo "error running bwa"; exit 7; fi # error 127 means non executable
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "gatk.*jar") ]]; then NOT_FOUND="gatk jar"; fi
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "bwa") ]]; then NOT_FOUND="${NOT_FOUND}, bwa"; fi
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "samtools") ]]; then NOT_FOUND="${NOT_FOUND}, samtools"; fi
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "strelka") ]]; then echo "missing strelka2 folder, will not be able to run the strelka2 variant caller."; fi
  if [[ ! -z ${NOT_FOUND} ]]; then
    echo "binaries missing: $NOT_FOUND ";
    ls -l $LOCAL_BIN_FOLDER
    exit 4;
  fi
  NOT_FOUND=""
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa)$") ]]; then NOT_FOUND="ref.fasta"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "dict$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.dict"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "fai$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fai"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).amb$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.amb"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).ann$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.ann"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).bwt$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.bwt"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).pac$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.pac"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).sa$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.sa"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "vcf(.gz)?$") ]]; then echo "missing dbsnp file, BQSR will be skipped"; fi
  if [[ ! -z ${NOT_FOUND} ]]; then
    echo "reference files missing: $NOT_FOUND ";
    ls -l $LOCAL_REF_FOLDER
    exit 5;
  fi
}

function parseOptions() {
  # parse options
  while [[ $# -gt 0 ]]
  do
    key="$1"
    case $key in
        --EBRmount)
          EBR_MOUNT=$2
          shift # past argument
          shift # past value
          ;;
        *)    # unknown option
          echo "unknown option $1"
          shift # past argument
          ;;
    esac
  done
}

if [[ "$#" != "3" ]]; then
  echo -e $help
  exit 1;
else
  TIME_START=$(date +%s.%N)
  S3_REF_FOLDER=$1; shift;
  S3_BIN_FOLDER=$1; shift;
  S3_LIB_FOLDER=$1; shift;
  if [[ $S3_REF_FOLDER != */ ]]; then S3_REF_FOLDER="${S3_REF_FOLDER}/"; fi
  if [[ $S3_BIN_FOLDER != */ ]]; then S3_BIN_FOLDER="${S3_BIN_FOLDER}/"; fi
  if [[ $S3_LIB_FOLDER != */ ]]; then S3_LIB_FOLDER="${S3_LIB_FOLDER}/"; fi
  parseOptions
  checkURLs ${S3_REF_FOLDER}
  checkURLs ${S3_BIN_FOLDER}
  checkURLs ${S3_LIB_FOLDER}
  getLocalFolders
  aws s3 sync $S3_REF_FOLDER $LOCAL_REF_FOLDER
  aws s3 sync $S3_BIN_FOLDER $LOCAL_BIN_FOLDER
  aws s3 sync $S3_LIB_FOLDER $LOCAL_LIB_FOLDER
  unTarIfNeeded
  checkRequiredFiles
  TIME_END=$(date +%s.%N)
  TIME_DIFF=$(echo "$TIME_END - $TIME_START" | bc)
  echo "bootstrap time (s): ${TIME_DIFF}"
fi
