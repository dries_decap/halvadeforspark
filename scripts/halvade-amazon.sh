#!/bin/bash

help="halvade-amazon.sh help:\n\
halvade-amazon.sh germline germlineIn outputFolder [options]\n\
halvade-amazon.sh somatic tumorIn normalIn outputFolder [options]\n\
options:\n\
\t--exome: run the exome pipeline (less partitions)\n\
\t--tmpdir <string>: set the dir for tmp files\n\
\t--germlineSM <string>: germline samplename\n\
\t--tumorSM <string>: tumor samplename\n\
\t--normalSM <string>: normal samplename\n\
\t--nodes <int>: number of worker nodes\n\
\t--executor_memory <int>: sets the memory in MB per executor\n\
\t--executor_cpus <int>: sets the number of CPUs per executor\n\
\t--keep_cluster: Do not terminate the cluster after Halvade completes. Allows to run other samples on this cluster afterwards\n\
\t--cluster <string>: existing cluster id to run Halvade on (halvade bootstrap script should have been called on every node in this cluster), also enables --keep_cluster\n\
\t--partitions <int>: set number of partitions\n\
Additional Halvade options can be set with the variable \${HALVADE_OPTS} like this:\n\
\tHALVADE_OPTS=\"--variant_caller both\""


if [[ "$#" == "0" ]]; then
  echo -e $help
  exit;
fi

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

function parseOptions() {
  # parse options
  while [[ $# -gt 0 ]]
  do
    key="$1"
    case $key in
        --exome)
          EXOME_PIPELINE="true"
          shift # past argument
          ;;
        --germlineSM)
          SM="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_memory)
          EXECUTOR_MEMORY="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_cpus)
          TASK_CPUS="$2"
          shift # past argument
          shift # past value
          ;;
        --partitions)
          PARTITIONS="$2"
          shift # past argument
          shift # past value
          ;;
        --tumorSM)
          TUMOR_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --normalSM)
          NORMAL_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --nodes)
          NR_NODES="$2"
          shift # past argument
          shift # past value
          ;;
        --cluster)
          CLUSTER_ID="$2"
          KEEP_CLUSTER="keep_cluster"
          shift # past argument
          shift # past value
          ;;
        --keep_cluster)
          KEEP_CLUSTER="keep_cluster"
          shift # past ar gument
          ;;
        *)    # unknown option
          echo "unknown option $1"
          exit 100;
          ;;
    esac
  done
}


# CHECK REQUIRED INPUT
function checkAWSvariables() {
  # OPTIONAL USER INPUT, set default if not given
  AWS_CONFIG=${AWS_CONFIG:-aws-config.sh}
  CORE_INSTANCE_COUNT=${CORE_INSTANCE_COUNT:-1}
  CORE_INSTANCE_TYPE=${CORE_INSTANCE_TYPE:-r5d.8xlarge}
  MASTER_INSTANCE_TYPE=${MASTER_INSTANCE_TYPE:-r5d.xlarge}
  if [[ -f $AWS_CONFIG ]]; then
    . ${AWS_CONFIG}
  else
    echo "no config file '${GCLOUD_CONFIG}' found"; exit 10;
  fi
  if [[ ! -z $NR_NODES ]]; then
    CORE_INSTANCE_COUNT=$NR_NODES
  fi
  if [[ -z $EC2_KEY_PAIR ]]; then
    echo "no ec2 key pair provided"; exit 1;
  fi
  if [[ -z $AWS_REGION ]]; then
    AWS_REGION=$(aws configure get region)
    if [[ -z $AWS_REGION ]]; then
      echo "no AWS region given"; exit 4;
    fi
  fi
  if [[ -z $S3_LIB_DIR ]]; then
    echo "no s3 lib dir provided"; exit 3;
  fi
  if [[ -z $S3_REF_DIR ]]; then
    echo "no s3 ref dir provided"; exit 4;
  fi
  if [[ -z $S3_BIN_DIR ]]; then
    echo "no s3 bin dir provided"; exit 5;
  fi
  if [[ ! -z $LOG_URI ]]; then
    AWS_LOG="--log-uri ${LOG_URI}"
  fi
  if [[ $S3_LIB_DIR != */ ]]; then S3_LIB_DIR="${S3_LIB_DIR}/"; fi
  LIB_FILES=$(aws s3 ls ${S3_LIB_DIR} | awk '{print $NF}')
  if [[ -z $(echo $LIB_FILES | grep "amazon-bootstrap.sh") ]]; then echo "amazon-bootstrap.sh not found in s3 library folder."; exit 7; fi

  # check memory/cpus of nodes
  RESOURCE_MEMORY=$(( $(aws ec2 describe-instance-types --instance-types ${CORE_INSTANCE_TYPE} --query "InstanceTypes[].MemoryInfo.SizeInMiB" | awk 'NR==2')/ 1024))
  if [[ -z $RESOURCE_MEMORY ]]; then
    echo "unable to detect resource memory of ${CORE_INSTANCE_TYPE}"; exit 8;
  fi
  RESOURCE_CPU=$(( 1*$(aws ec2 describe-instance-types --instance-types ${CORE_INSTANCE_TYPE} --query "InstanceTypes[].VCpuInfo.DefaultVCpus" | awk 'NR==2') )) # get rid of spaces
  if [[ -z $RESOURCE_CPU ]]; then
    echo "unable to detect resource vCPUs of ${CORE_INSTANCE_TYPE}"; exit 9;
  fi
  MASTER_GROUP="InstanceGroupType=MASTER,InstanceType=${MASTER_INSTANCE_TYPE},InstanceCount=1"
  CORE_GROUP="InstanceGroupType=CORE,InstanceType=${CORE_INSTANCE_TYPE},InstanceCount=${CORE_INSTANCE_COUNT}"
  if [[ ! -z $MASTER_BID_PRICE ]]; then
    MASTER_GROUP="${MASTER_GROUP},BidPrice=$MASTER_BID_PRICE"
  fi
  if [[ ! -z $CORE_BID_PRICE ]]; then
    CORE_GROUP="${CORE_GROUP},BidPrice=$CORE_BID_PRICE"
  fi
}


# check if input is ok
function getInput() {
  local INPUT=$1
  if [[ $INPUT == *bam ]]; then # should also work if it is on s3
    NEWINPUT=$INPUT
  else
    if [[ $INPUT != */ ]]; then INPUT="${INPUT}/"; fi
    FILES=$(aws s3 ls $INPUT | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq|bam)$")
    if [ -z "$FILES" ]; then
      # check for subdirectories with fq files
      FILES=$(for f in $(aws s3 ls $INPUT | grep "PRE" | awk '{print $NF}'); do aws s3 ls ${INPUT}$f; done | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq)$")
      if [ -z "$FILES" ]; then
        echo "no FASTQ files found in ${INPUT} or its subdirectories";
        exit 6;
      else
        NEWINPUT=$INPUT # should be preprocessed with a folder per read group
      fi
    else
      # needs to be processed but should be valid info
      NEWINPUT=$INPUT
    fi
  fi
}

# read arguments and process
MODE=$1
shift
if [[ "$MODE" = "germline" ]]; then
  if [[ "$#" -lt "2" ]]; then
    echo -e $help
    exit
  fi
  getInput $1; shift;
  GERMLINE=$NEWINPUT
  OUTPUT=$1; shift;
  SM="SAMPLE"
  parseOptions "$@"
  # CONFIGURE AWS
  checkAWSvariables
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $INPUT == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $INPUT == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  REF_DIR=/mnt/ref/
  BIN_DIR=/mnt/bin/
  HALVADE_SCRIPT=/mnt/lib/halvade.sh
  HALVADE_ARGUMENTS="Args=[${HALVADE_SCRIPT},germline,${REF_DIR},${BIN_DIR},${GERMLINE},${OUTPUT}"
  if [[ ! -z $TMP_DIR ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--tmpdir,${TMP_DIR}"; fi
  if [[ ! -z $PARTITIONS ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--partitions,${PARTITIONS}"; fi
  if [[ ! -z $EXOME_PIPELINE ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--exome"; fi
  if [[ "$SM" != "SAMPLE" ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--germlineSM,${SM}"; fi
  if [[ ! -z $HALVADE_OPTS ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--halvade_opts,${HALVADE_OPTS// /;}"; fi
  if [[ ! -z "$EXECUTOR_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_memory $EXECUTOR_MEMORY"; fi
  if [[ ! -z "$TASK_CPUS" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_cpus $TASK_CPUS"; fi
  HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--memory,${RESOURCE_MEMORY},--cpus,${RESOURCE_CPU},--nodes,${CORE_INSTANCE_COUNT}]"
elif [[ "$MODE" = "somatic" ]]; then
  if [[ "$#" -lt "3" ]]; then
    echo -e $help
    exit
  fi
  getInput $1; shift;
  TUMOR_IN=$NEWINPUT
  getInput $1; shift;
  NORMAL_IN=$NEWINPUT
  OUTPUT=$1; shift;
  TUMOR_SM="TUMOR_SM"
  NORMAL_SM="NORMAL_SM"
  parseOptions "$@"
  # CONFIGURE AWS
  checkAWSvariables
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  REF_DIR=/mnt/ref/
  BIN_DIR=/mnt/bin/
  HALVADE_SCRIPT=/mnt/lib/halvade.sh
  HALVADE_ARGUMENTS="Args=[${HALVADE_SCRIPT},somatic,${REF_DIR},${BIN_DIR},${TUMOR_IN},${NORMAL_IN},${OUTPUT}"
  if [[ ! -z $TMP_DIR ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--tmpdir,${TMP_DIR}"; fi
  if [[ ! -z $PARTITIONS ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--partitions,${PARTITIONS}"; fi
  if [[ ! -z $EXOME_PIPELINE ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--exome"; fi
  if [[ "$TUMOR_SM" != "TUMOR_SM" ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--tumorSM,${TUMOR_SM}"; fi
  if [[ "$NORMAL_SM" != "NORMAL_SM" ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--normalSM,${NORMAL_SM}"; fi
  if [[ ! -z $HALVADE_OPTS ]]; then HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--halvade_opts,${HALVADE_OPTS// /;}"; fi
  if [[ ! -z "$EXECUTOR_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_memory $EXECUTOR_MEMORY"; fi
  if [[ ! -z "$TASK_CPUS" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_cpus $TASK_CPUS"; fi
  HALVADE_ARGUMENTS="${HALVADE_ARGUMENTS},--memory,${RESOURCE_MEMORY},--cpus,${RESOURCE_CPU},--nodes,${CORE_INSTANCE_COUNT}]"
else
  echo -e $help
  exit
fi

# create halvade_steps
HALVADE_STEPS="Type=CUSTOM_JAR,Name=RunHalvade,ActionOnFailure=TERMINATE_CLUSTER,Jar=s3://${AWS_REGION}.elasticmapreduce/libs/script-runner/script-runner.jar,${HALVADE_ARGUMENTS}"

NAME="HalvadeEMR" # no spaces!
RELEASE_LABEL="emr-6.3.0"
if [[ -z $KEEP_CLUSTER ]]; do
  AUTO_TERMINATE="--auto-terminate"
fi
if [[ -z $CLUSTER_ID ]]; do
  CMD="aws emr create-cluster \
  --applications Name=Spark Name=Hadoop \
  --name ${NAME} \
  --release-label ${RELEASE_LABEL} \
  --ec2-attributes KeyName=${EC2_KEY_PAIR} \
  --instance-groups $MASTER_GROUP $CORE_GROUP \
  --use-default-roles ${AWS_LOG} \
  --region ${AWS_REGION} \
  --bootstrap-actions Path="${S3_LIB_DIR}amazon-bootstrap.sh",Args=[${S3_REF_DIR},${S3_BIN_DIR},${S3_LIB_DIR}] \
  --scale-down-behavior TERMINATE_AT_TASK_COMPLETION ${AUTO_TERMINATE} \
  --steps ${HALVADE_STEPS}"
else
  CMD="aws emr add-steps \
  --cluster-id ${CLUSTER_ID}\
  --steps ${HALVADE_STEPS}"
fi
echo "STARTING JOB:"
echo $CMD
$CMD


if [[ ! -z $KEEP_CLUSTER ]]; then
  echo "Cluster '${CLUSTER_ID}' will not be killed"
fi
