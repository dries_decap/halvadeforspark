#!/bin/bash

# this script is run on all nodes

function checkURLs(){
  FOLDER=$1
  if [[ -z $FOLDER ]]; then
    echo "no folder given"; exit 2;
  else
    if [[ -z $(echo ${FOLDER} | grep -E "^gs:")  ]]; then
      echo "not a GS folder: " $FOLDER; exit 3;
    fi
  fi
}

function getLocalFolders() {
  # assume local ssds that need to be mounted
  LOCAL_REF_FOLDER="/halvade/ref/"
  LOCAL_BIN_FOLDER="/halvade/bin/"
  LOCAL_LIB_FOLDER="/halvade/lib/"
  mkdir -p ${LOCAL_REF_FOLDER}
  mkdir -p ${LOCAL_BIN_FOLDER}
  mkdir -p ${LOCAL_LIB_FOLDER}
}

function unTarIfNeeded() {
  # check if things need to be untarred
  cd $LOCAL_REF_FOLDER
  for f in $(find ./ -maxdepth 1 -mindepth 1 -name *.tar.gz); do
    echo $f;
    tar xvf $f;
    rm -f $f;
  done
  cd $LOCAL_BIN_FOLDER
  for f in $(find ./ -maxdepth 1 -mindepth 1 -name *.tar.gz); do
    echo $f;
    tar xvf $f;
    rm -f $f;
  done
  cd $LOCAL_LIB_FOLDER
  for f in $(find ./ -maxdepth 1 -mindepth 1 -name *.tar.gz); do
    echo $f;
    tar xvf $f;
    rm -f $f;
  done
  ls -l ${LOCAL_LIB_FOLDER}
  ls -l ${LOCAL_BIN_FOLDER}
  ls -l ${LOCAL_REF_FOLDER}
}



function checkRequiredFiles() {
  NOT_FOUND=""
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "htsjdk.*jar") ]]; then NOT_FOUND="htsjdk jar"; fi
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "hadoop-bam.*jar") ]]; then NOT_FOUND="${NOT_FOUND}, hadoop-bam jar"; fi
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "halvade.*jar") ]]; then NOT_FOUND="${NOT_FOUND}, halvade jar"; fi
  if [[ -z $(ls -l ${LOCAL_LIB_FOLDER} | grep "halvade.sh") ]]; then NOT_FOUND="${NOT_FOUND}, halvade.sh"; fi
  if [[ ! -z ${NOT_FOUND} ]]; then
    echo "${NOT_FOUND} not found, downloading halvade bundle";
    cd ${LOCAL_LIB_FOLDER}
    if [[ -z $(which wget) ]]; then echo "installing wget"; apt-get install -y wget; fi
    wget https://bitbucket.org/dries_decap/halvadeforspark/downloads/halvade-cloud-bundle.tar.gz
    tar xvf halvade-cloud-bundle.tar.gz
    rm -f halvade-cloud-bundle.tar.gz
    ls -l $LOCAL_LIB_FOLDER
  fi
  # check if bwa/samtools executable
  ${LOCAL_BIN_FOLDER}/samtools
  if [[ $? == "127" ]]; then echo "error running samtools"; exit 6; fi # error 127 means non executable
  ${LOCAL_BIN_FOLDER}/bwa
  if [[ $? == "127" ]]; then echo "error running bwa"; exit 7; fi # error 127 means non executable
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "gatk.*jar") ]]; then NOT_FOUND="gatk jar"; fi
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "bwa") ]]; then NOT_FOUND="${NOT_FOUND}, bwa"; fi
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "samtools") ]]; then NOT_FOUND="${NOT_FOUND}, samtools"; fi
  if [[ -z $(ls -l ${LOCAL_BIN_FOLDER} | grep -E "strelka") ]]; then echo "missing strelka2 folder, will not be able to run the strelka2 variant caller."; fi
  if [[ ! -z ${NOT_FOUND} ]]; then
    echo "binaries missing: $NOT_FOUND ";
    ls -l $LOCAL_BIN_FOLDER
    exit 4;
  fi
  NOT_FOUND=""
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa)$") ]]; then NOT_FOUND="ref.fasta"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "dict$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.dict"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "fai$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fai"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).amb$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.amb"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).ann$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.ann"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).bwt$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.bwt"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).pac$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.pac"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "(fasta|fa).sa$") ]]; then NOT_FOUND="${NOT_FOUND}, ref.fasta.sa"; fi
  if [[ -z $(ls -l ${LOCAL_REF_FOLDER} | grep -E "vcf(.gz)?$") ]]; then echo "missing dbsnp file, BQSR will be skipped"; fi
  if [[ ! -z ${NOT_FOUND} ]]; then
    echo "reference files missing: $NOT_FOUND ";
    ls -l $LOCAL_REF_FOLDER
    exit 5;
  fi
}

TIME_START=$(date +%s.%N)
GS_REF_FOLDER=$(/usr/share/google/get_metadata_value attributes/gs_ref_folder)
GS_BIN_FOLDER=$(/usr/share/google/get_metadata_value attributes/gs_bin_folder)
GS_LIB_FOLDER=$(/usr/share/google/get_metadata_value attributes/gs_lib_folder)
if [[ $GS_REF_FOLDER != */ ]]; then GS_REF_FOLDER="${GS_REF_FOLDER}/"; fi
if [[ $GS_BIN_FOLDER != */ ]]; then GS_BIN_FOLDER="${GS_BIN_FOLDER}/"; fi
if [[ $GS_LIB_FOLDER != */ ]]; then GS_LIB_FOLDER="${GS_LIB_FOLDER}/"; fi
checkURLs ${GS_REF_FOLDER}
checkURLs ${GS_BIN_FOLDER}
checkURLs ${GS_LIB_FOLDER}
getLocalFolders
gsutil -m rsync -r $GS_REF_FOLDER $LOCAL_REF_FOLDER
gsutil -m rsync -r $GS_BIN_FOLDER $LOCAL_BIN_FOLDER
gsutil -m rsync -r $GS_LIB_FOLDER $LOCAL_LIB_FOLDER
unTarIfNeeded
checkRequiredFiles
TIME_END=$(date +%s.%N)
TIME_DIFF=$(echo "$TIME_END - $TIME_START" | bc)
echo "bootstrap time (s): ${TIME_DIFF}"
