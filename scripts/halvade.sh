#!/bin/bash


help="halvade.sh help:\n\
\thalvade.sh germline referenceFolder binaryFolder germlineIn output [options]\n\
\thalvade.sh somatic referenceFolder binaryFolder tumorIn normalIn output [options]\n\
\toptions:\n\
\t\t--exome: run the exome pipeline (less partitions)\n\
\t\t--tmpdir <string>: set the dir for tmp files\n\
\t\t--germlineSM <string>: germline samplename\n\
\t\t--tumorSM <string>: tumor samplename\n\
\t\t--normalSM <string>: normal samplename\n\
\t\t--partitions <int>: set number of partitions\n\
\t\t--memory <int>: sets the available memory in GB\n\
\t\t--cpus <int>: sets the number of available CPUs\n\
\t\t--executor_memory <int>: sets the memory in MB per executor\n\
\t\t--executor_cpus <int>: sets the number of CPUs per executor\n\
\t\t--nodes <int>: sets the number of nodes\n\
\t\t--quiet: prints the command to start this job and exits\n\
\tAdditional Halvade options can be set with the variable \${HALVADE_OPTS} like this:\n\
\t\tHALVADE_OPTS=\"--variant_caller both\n\
\tAdditional Spark options can be set with the variable \${EXTRA_SPARK_OPTIONS} like this:\n\
\t\EXTRA_SPARK_OPTIONS=\"--master spark://\$ip:7077\""

if [[ "$#" == "0" ]]; then
  if [[ -f /usr/share/google/get_metadata_value && ! -z $(dig +short metadata.google.internal) ]]; then #check if we are on GCE
    echo "Running on GCE, using variables from metadata value attributes/halvade_args";
    $0 $(/usr/share/google/get_metadata_value attributes/halvade_args | sed 's/;/ /g') # call script with GCE arguments
    HALVADE_EXIT_CODE=$?
    exit $HALVADE_EXIT_CODE; # give same return as
  else
    echo -e $help
    customexit 1;
  fi
fi

function customexit() { # to ensure the GCE cluster is killed if needed!
  exitcode=$1
  if [[ $exitcode != 0 ]]; then
    echo "Halvade command failed with error code $exitcode, exiting"
  fi
  exit $exitcode
}

function run() {
  CMD="$@"
  # print command used and run
  if [[ -z $QUIET ]]; then
    echo "running $CMD"
    $CMD
    exitcode=$?
    if [[ $exitcode != 0 ]]; then
      echo "Halvade job failed with error code $exitcode, exiting"
      customexit -1
    fi
  else
    echo "Halvade job spark-submit command:"
    echo "$CMD"
  fi
}

function configureSpark() {
  if [[ ! $BIN_DIR = */ ]]; then
    BIN_DIR="${BIN_DIR}/"
  fi
  # determine latest version of libraries
  if [[ -z "$HALVADE_HOME" ]]; then
    HALVADE_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
  fi
  HALVADE=$(find $HALVADE_HOME -name 'halvade-assembly*' | sed 's/.*halvade-assembly-\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\.jar/\1\t\2\t\3/g' | sort -k1,3n | head -n 1 | awk '{print "halvade-assembly-"$1"."$2"."$3".jar"}')
  if [[ -z $HALVADE ]]; then
      echo "halvade-assembly-*.jar not found in ${HALVADE_HOME}."
      customexit 2;
  fi
  HALVADE_JAR=${HALVADE_HOME}/${HALVADE}
  HADOOPBAM=$(find $HALVADE_HOME -name 'hadoop-bam*' | sed 's/.*hadoop-bam-\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\.jar/\1\t\2\t\3/g' | sort -k1,3n | head -n 1 | awk '{print "hadoop-bam-"$1"."$2"."$3".jar"}')
  if [[ -z $HADOOPBAM ]]; then
      echo "hadoop-bam-*.jar not found in ${HALVADE_HOME}."
      customexit 3;
  fi
  HTSJDK=$(find $HALVADE_HOME -name 'htsjdk*' | sed 's/.*htsjdk-\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\.jar/\1\t\2\t\3/g' | sort -k1,3n | head -n 1 | awk '{print "htsjdk-"$1"."$2"."$3".jar"}')
  if [[ -z $HTSJDK ]]; then
      echo "htsjdk-*.jar not found in ${HALVADE_HOME}."
      customexit 4;
  fi
  JARS=${HALVADE_JAR},${HALVADE_HOME}/${HADOOPBAM},${HALVADE_HOME}/${HTSJDK}

  # detect node resources, assumes all nodes are same as master
  if [[ -z $RESOURCE_CPU ]]; then
    CORES=$(grep -c processor /proc/cpuinfo)
  else
    CORES=$RESOURCE_CPU
  fi
  if [[ -z $RESOURCE_MEMORY ]]; then
    MEM_KB=$(grep MemTotal /proc/meminfo | awk '{print $2}') # in KB
    MEMORY=$((MEM_KB/1024)) # in MB
  else
    MEMORY=$RESOURCE_MEMORY
  fi
  MEM_PER_CORE=$((MEMORY/CORES))
  echo "$MEMORY MB & $CORES CPUs available -> $MEM_PER_CORE MB per core"

  # determine resources
  if [ -z "$RESOURCES" ]; then # set resources if not given by options
    DRIVER_MEM="1024M"
    # 1. check if input is bam or not
    if [[ ! -z "$MANIFEST" ]]; then # manifest
      MIN_TOTAL_EXEC_MEM=$((8*1024)) # 8 GB memory
      MEM_PER_CORE=$(( (MEMORY - 2048) /CORES)) # to make sure
    elif [[ $GERMLINE = *bam || $TUMOR_IN = *bam || $NORMAL_IN = *bam ]]; then # FROM BAM
      if [[ -z "$EXOME_PIPELINE" ]]; then # WGS PIPELINE FROM BAM
        MIN_TOTAL_EXEC_MEM=$((8*1024))
        MIN_EXEC_MEM=$((6*1024)) # MIN_TOTAL_EXEC_MEM - MIN_EXEC_MEM is overhead
      else # WXS PIPELINE FROM BAM
        MIN_TOTAL_EXEC_MEM=$((6*1024))
        MIN_EXEC_MEM=$((4*1024)) # MIN_TOTAL_EXEC_MEM - MIN_EXEC_MEM is overhead
      fi
    else  # FROM FASTQ
      if [[ -z "$EXOME_PIPELINE" ]]; then # WGS PIPELINE FROM FASTQ
        MIN_TOTAL_EXEC_MEM=$((16*1024))
        MIN_EXEC_MEM=$((6*1024)) # MIN_TOTAL_EXEC_MEM - MIN_EXEC_MEM is overhead
      else # WXS PIPELINE FROM FASTQ
        MIN_TOTAL_EXEC_MEM=$((14*1024))
        MIN_EXEC_MEM=$((4*1024)) # MIN_TOTAL_EXEC_MEM - MIN_EXEC_MEM is overhead
      fi
    fi
    if [[ -z "$MANIFEST" ]]; then # variables only set if not manifest
      if [[ -z $TASK_CPUS ]]; then
        TASK_CPUS_VAL=$(( (MIN_TOTAL_EXEC_MEM + MEM_PER_CORE-1)/ MEM_PER_CORE)) # add mem_per_core-1 to round up to get at least 8GB
      else
        TASK_CPUS_VAL=$TASK_CPUS
      fi
      TOTAL_EXEC_MEM=$((TASK_CPUS_VAL * MEM_PER_CORE)) # total memory per executor (Depends on CPUS)
      # divide leftover memory between overhead and exector memory
      if [[ -z $EXECUTOR_MEMORY ]]; then
        EXECUTOR_MEMORY_VAL=$(( MIN_EXEC_MEM + ( TOTAL_EXEC_MEM - MIN_TOTAL_EXEC_MEM ) / 2))
      else
        EXECUTOR_MEMORY_VAL=$EXECUTOR_MEMORY
      fi
    else
      if [[ -z $TASK_CPUS ]]; then
        TASK_CPUS_VAL=$(( ($CORES-1)/4 > 4 ? ($CORES-1)/4 : 4 )) # cores
      else
        TASK_CPUS_VAL=$TASK_CPUS
      fi
      TOTAL_EXEC_MEM=$(( TASK_CPUS_VAL * MEM_PER_CORE > MIN_TOTAL_EXEC_MEM ? TASK_CPUS_VAL * MEM_PER_CORE : MIN_TOTAL_EXEC_MEM)) # total memory per executor (Depends on CPUS)
      # divide leftover memory between overhead and exector memory
      if [[ -z $EXECUTOR_MEMORY ]]; then
        EXECUTOR_MEMORY_VAL=$(( TOTAL_EXEC_MEM * 100 / 110 ))  # overhead is 10%
      else
        EXECUTOR_MEMORY_VAL=$EXECUTOR_MEMORY
      fi
    fi
    OVERHEAD_MEMORY_MB="$((TOTAL_EXEC_MEM - EXECUTOR_MEMORY_VAL))M"
    EXECUTOR_MEMORY_MB="${EXECUTOR_MEMORY_VAL}M"
    RESOURCES="--driver-memory ${DRIVER_MEM}"
    if [[ ! -z "$EXECUTOR_MEMORY_MB" ]]; then
      RESOURCES="$RESOURCES --executor-memory ${EXECUTOR_MEMORY_MB} --executor-cores ${TASK_CPUS_VAL} --conf spark.task.cpus=${TASK_CPUS_VAL}"
    fi
    if [[ ! -z "$OVERHEAD_MEMORY_MB" ]]; then
      RESOURCES="$RESOURCES --conf spark.executor.memoryOverhead=${OVERHEAD_MEMORY_MB}"
    fi
  fi

  echo "$TASK_CPUS_VAL CPUs & $TOTAL_EXEC_MEM MB ($EXECUTOR_MEMORY_MB & $OVERHEAD_MEMORY_MB overhead) per executor required"
  MASTER=yarn
  OTHER_OPTIONS="--conf spark.ui.showConsoleProgress=true"
  # check for dynamicAllocation
  HADOOP_CONF_DIR=${HADOOP_CONF_DIR:-/etc/hadoop/conf} # is typically the default hadoop conf dir
  if [[ $(grep -c spark_shuffle $HADOOP_CONF_DIR/yarn-site.xml) -gt "0" ]]; then
    OTHER_OPTIONS="$OTHER_OPTIONS --conf spark.shuffle.service.enabled=true --conf spark.dynamicAllocation.enabled=true"
  else
    if [[ -z $NODES ]]; then
      if [[ -f $HADOOP_CONF_DIR/slaves ]]; then
        NODES=$(cat $HADOOP_CONF_DIR/slaves | wc -l);
      elif [[ -f $HADOOP_CONF_DIR/workers ]]; then
        NODES=$(cat $HADOOP_CONF_DIR/workers | wc -l);
      else
        NODES=1;
      fi
    fi
    echo "number of nodes is $NODES"
    NUM_EXECUTORS_PER_NODE_MEM=$(( (MEMORY-1024) / TOTAL_EXEC_MEM ))
    NUM_EXECUTORS_PER_NODE_CPUS=$(( CORES / TASK_CPUS ))
    NUM_EXECUTORS_PER_NODE=$(( NUM_EXECUTORS_PER_NODE_CPUS > NUM_EXECUTORS_PER_NODE_MEM ? NUM_EXECUTORS_PER_NODE_MEM : NUM_EXECUTORS_PER_NODE_CPUS ))
    NUM_EXECUTORS=$((NODES * NUM_EXECUTORS_PER_NODE));
    RESOURCES="$RESOURCES --num-executors $NUM_EXECUTORS"
  fi
  SPARK_COMMAND="spark-submit --master ${MASTER} ${OTHER_OPTIONS} ${RESOURCES} --jars $JARS --class $CLASS ${EXTRA_SPARK_OPTIONS} "

  # gather metrics? default off
  METRICS=FALSE
  CSV_METRICS="--files metrics.properties --conf spark.metrics.conf=metrics.properties --conf spark.eventLog.logStageExecutorMetrics=true" # change to correct file name when used
  if [ ${METRICS} == "TRUE" ]; then
    SPARK_COMMAND="${SPARK_COMMAND} ${CSV_METRICS}"
  fi
}

function getHalvadeOpts() {
  # set directory with or without / as halvade wants it!
  if [[ $REF_DIR = */ ]]; then
    REF_DIR="${REF_DIR::-1}"
  fi
  # get reference FASTA/VCF
  REF=$(find $REF_DIR -maxdepth 1 -iregex ".*\.\(fa\|fasta\)" -print -quit)
  if [ -z "$REF" ]; then
    echo "no reference FASTA file found in ${REF_DIR}: $(ls $REF_DIR)";
    customexit 5;
  fi
  SNIP_DB=$(find $REF_DIR -maxdepth 1 -iregex ".*\.\(vcf\|vcf.gz\)" -print -quit)
  if [ -z "$SNIP_DB" ]; then
    echo "no reference DB SNIP file found in ${REF_DIR}: $(ls $REF_DIR)";
  fi
  # extra options
  EXTRA_HALVADE_OPTS=""
  HALVADE_OPTS_ARRAY=(${HALVADE_OPTS})
  while [ "${#HALVADE_OPTS_ARRAY[@]}" -gt 0 ]; do
    OPTION=${HALVADE_OPTS_ARRAY[0]}
    VALUE=${HALVADE_OPTS_ARRAY[1]}
    if [[ $VALUE = -* ]];then
      HALVADE_OPTS_ARRAY=("${HALVADE_OPTS_ARRAY[@]:1}")
    	EXTRA_HALVADE_OPTS="$EXTRA_HALVADE_OPTS $OPTION"
    else
      HALVADE_OPTS_ARRAY=("${HALVADE_OPTS_ARRAY[@]:2}")
      if [[ "$OPTION" = "-r" || "$OPTION" = "--reference" ]]; then
        REF=$VALUE
      elif [[ "$OPTION" = "-b" || "$OPTION" = "--bindir" ]]; then
        BIN_DIR=$VALUE
      elif [[ "$OPTION" = "-s" || "$OPTION" = "--knownsites" ]]; then
        SNIP_DB=$VALUE
      elif [[ "$OPTION" = "-o" || "$OPTION" = "--output" ]]; then
        OUTPUT=$VALUE
      elif [[ "$OPTION" = "--normal" ]]; then
        NORMAL_IN=$VALUE
      elif [[ "$OPTION" = "--tumor" ]]; then
        TUMOR_IN=$VALUE
      elif [[ "$OPTION" = "--germline" || "$OPTION" = "-i" || "$OPTION" = "--manifest" ]]; then
        INPUT=$VALUE
      elif [[ "$OPTION" = "--partitions" ]]; then
        PARTITIONS=$VALUE
      else
    	 EXTRA_HALVADE_OPTS="$EXTRA_HALVADE_OPTS $OPTION $VALUE"
      fi
    fi
  done
  if [ ! -z "$SNIP_DB" ]; then
    SNIP_DB="--knownsites ${SNIP_DB}"
  fi
  if [ ! -z "$TMP_DIR" ]; then
    TMP_DIR="--tmp ${TMP_DIR}"
  fi
}

function preprocessIfNeeded() {
  local INPUT=$1
  if [[ $INPUT == *bam ]]; then # should also work if it is on s3/gs
      NEWINPUT=$INPUT
  elif [[ $INPUT == s3* ]]; then # if it is not a bam then it is a folder... or it should be!
      # is preprocessing required??
      if [[ $INPUT != */ ]]; then INPUT="${INPUT}/"; fi
      FILES=$(aws s3 ls $INPUT | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq|bam)$")
      if [ -z "$FILES" ]; then
        # check for subdirectories with fq files
        FILES=$(for f in $(aws s3 ls $INPUT | grep "PRE" | awk '{print $NF}'); do aws s3 ls ${INPUT}$f; done | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq)$")
        if [ -z "$FILES" ]; then
          echo "no FASTQ files found in ${INPUT} or its subdirectories";
          customexit 6;
        else
          NEWINPUT=$INPUT # should be preprocessed with a folder per read group
        fi
      else
        # FQ files found in $INPUT that need to be preprocessed!
        ## if input is on s3 then create newinput on hdfs!
        MANIFEST=/tmp/input.manifest
        if [ -f $MANIFEST ]; then rm $MANIFEST; fi # clear the file
        CORRECTS3INPUT=$(echo ${INPUT} | sed 's/^s3\(n\|a\|\):/s3:/')
        for f1 in $(echo $FILES | sed 's/ /\n/g' | grep -E "*(_1.fq.gz|_1.fastq.gz|_1.fastq|_1.fq|bam)"); do
          if [[ $f1 == *bam ]]; then
            id=$(echo $f1 | sed 's/\(.*\).bam$/\1/g')
            echo -e "${CORRECTS3INPUT}$f1\t$id" >> $MANIFEST
          else
            f2="${f1/_1/_2}"
            if [[ ! -z $(echo $FILES | sed 's/ /\n/g' | grep "$f2") ]]; then
              id=$(echo $f1 | sed 's/\(.*\)_1.*/\1/g')
              echo -e "${CORRECTS3INPUT}$f1\t${CORRECTS3INPUT}$f2\t$id" >> $MANIFEST
            else
              echo "did not find the second paired-end read file for $f1"
              customexit 6;
            fi
          fi
        done
        # hadoop fs -put -f file://$MANIFEST $MANIFEST // not needed, manifest should never be on hdfs!

        NEWINPUT=/preprocessed/$(uuidgen) # make sure it's unique so that both normal/tumor can be preprocessed
        CLASS=be.ugent.intec.halvade.job.Preprocess
        configureSpark
        if [[ ! -d $NEWINPUT ]]; then
          CMD="${SPARK_COMMAND} ${HALVADE_JAR} --overwrite --manifest file://${MANIFEST} --output ${NEWINPUT}"
          run $CMD
        fi
        rm $MANIFEST
        unset MANIFEST RESOURCES
      fi
  elif [[ $INPUT == gs* ]]; then # if it is not a bam then it is a folder... or it should be!
  ## TODO check this
      # is preprocessing required??
      if [[ $INPUT != */ ]]; then INPUT="${INPUT}/"; fi
      FILES=$(gsutil ls $INPUT | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq|bam)$")
      if [ -z "$FILES" ]; then
        # check for subdirectories with fq files
        FILES=$(for f in $(gsutil ls $INPUT | grep "PRE" | awk '{print $NF}'); do gsutil ls ${INPUT}$f; done | awk '{print $NF}' | grep -E "*(fq.gz|fastq.gz|fastq|fq)$")
        if [ -z "$FILES" ]; then
          echo "no FASTQ files found in ${INPUT} or its subdirectories";
          customexit 6;
        else
          NEWINPUT=$INPUT # should be preprocessed with a folder per read group
        fi
      else
        # FQ files found in $INPUT that need to be preprocessed!
        ## if input is on gs then create newinput on hdfs!
        MANIFEST=/tmp/input.manifest
        if [ -f $MANIFEST ]; then rm $MANIFEST; fi # clear the file
        for f1 in $(echo $FILES | sed 's/ /\n/g' | grep -E "*(_1.fq.gz|_1.fastq.gz|_1.fastq|_1.fq|bam)"); do
          if [[ $f1 == *bam ]]; then
            id=$(basename $f1 | sed 's/\(.*\).bam$/\1/g')
            echo -e "$f1\t$id" >> $MANIFEST
          else
            f2="${f1/_1/_2}"
            if [[ ! -z $(echo $FILES | sed 's/ /\n/g' | grep "$f2") ]]; then
              id=$(basename $f1 | sed 's/\(.*\)_1.*/\1/g')
              echo -e "$f1\t$f2\t$id" >> $MANIFEST
            else
              echo "did not find the second paired-end read file for $f1"
              customexit 6;
            fi
          fi
        done
        # hadoop fs -put -f file://$MANIFEST $MANIFEST // not needed, manifest should never be on hdfs!

        NEWINPUT=/preprocessed/$(uuidgen) # make sure it's unique so that both normal/tumor can be preprocessed
        CLASS=be.ugent.intec.halvade.job.Preprocess
        configureSpark
        if [[ ! -d $NEWINPUT ]]; then
          CMD="${SPARK_COMMAND} ${HALVADE_JAR} --overwrite --manifest file://${MANIFEST} --output ${NEWINPUT}"
          run $CMD
        fi
        rm $MANIFEST
        unset MANIFEST RESOURCES
      fi
    ## TODO check this until here
  else
    # is preprocessing required??
    FILES=$(find $INPUT -maxdepth 1 -mindepth 1 -name "*fq.gz" -o -name "*fq" -o -name "*fastq.gz" -o -name "*fastq" -o -name "*bam")
    if [ -z "$FILES" ]; then
      # check for subdirectories with fq files
      FILES=$(find $INPUT -maxdepth 2 -mindepth 2 -name "*fq.gz" -o -name "*fq" -o -name "*fastq.gz" -o -name "*fastq")
      if [ -z "$FILES" ]; then
        echo "no FASTQ files found in ${INPUT} or its subdirectories";
        customexit 6;
      else
        NEWINPUT=$INPUT # should be preprocessed with a folder per read group
      fi
    else
      # FQ files found in $INPUT that need to be preprocessed!
      MANIFEST=$INPUT/input.manifest
      if [ -f $MANIFEST ]; then rm $MANIFEST; fi # clear the file
      NEWINPUT=$INPUT/preprocessed
      for f1 in $(echo $FILES | sed 's/ /\n/g' | grep -E "*(_1.fq.gz|_1.fastq.gz|_1.fastq|_1.fq|bam)"); do # search _1 files and bam files
        if [[ $f1 == *bam ]]; then
          id=$(echo $f1 | sed 's/.*\/\(.*\).bam$/\1/g')
          echo -e "$f1\t$id" >> $MANIFEST
        else
          f2="${f1/_1/_2}"
          if [ -f "$f2" ]; then
            id=$(echo $f1 | sed 's/.*\/\(.*\)_1.*/\1/g')
            echo -e "$f1\t$f2\t$id" >> $MANIFEST
          else
            echo "did not find the second paired-end read file for $f1"
            customexit 6;
          fi
        fi
      done

      CLASS=be.ugent.intec.halvade.job.Preprocess
      configureSpark
      if [[ ! -d $NEWINPUT ]]; then
        CMD="${SPARK_COMMAND} ${HALVADE_JAR} --overwrite --manifest file://${MANIFEST} --output ${NEWINPUT}"
        run $CMD
      fi
      rm $MANIFEST
      unset MANIFEST RESOURCES
    fi
  fi
}

function getInput() {
  if [[ $1 == s3* ]]; then
    local input=$(echo $1 | sed 's/^s3\(n\|a\|\):/s3:/') # make it s3a so that the AWS sdk can be used
  elif [[ $1 == gs* ]]; then
      local input=$1 # gs link
  else
    local input=$(readlink -f $1)
  fi
  echo "$input"
}
function getOutput() {
  if [[ $1 == s3* ]]; then
    S3_OUTPUT=$1
    HDFS_OUTPUT="/output-$(uuidgen)" # default fs should be hdfs
    if [[ $S3_OUTPUT == *vcf || $S3_OUTPUT == *vcf.gz ]]; then
      OUTPUT="${HDFS_OUTPUT}/$(basename ${S3_OUTPUT})"
    else
      OUTPUT="${HDFS_OUTPUT}/$(date +%F-%H-%M-%S.%N).vcf.gz"
    fi
  elif [[ $1 == gs* ]]; then
    GS_OUTPUT=$1
    HDFS_OUTPUT="/output-$(uuidgen)" # default fs should be hdfs
    if [[ $GS_OUTPUT == *vcf || $GS_OUTPUT == *vcf.gz ]]; then
      OUTPUT="${HDFS_OUTPUT}/$(basename ${GS_OUTPUT})"
    else
      OUTPUT="${HDFS_OUTPUT}/$(date +%F-%H-%M-%S.%N).vcf.gz"
    fi
  else
    if [[ -f $1 ]]; then
      OUTPUT=$(readlink -f $1); shift;
      if [[ $OUTPUT != *vcf ]]; then OUTPUT=${OUTPUT}.vcf; else echo "$OUTPUT already exist and will be overwritten"; fi
      mkdir -p $(dirname $OUTPUT)
    elif [[ -d $1 ]]; then
       OUTPUT=$(readlink -f $1); shift;
       if [[ -z $GERMLINE ]]; then # empty var so its somatic pipeline
         OUTPUT=$OUTPUT/somatic.vcf
       else
         OUTPUT=$OUTPUT/germline.vcf
       fi
    else
      OUTPUT=$1 # new file will be created
      mkdir -p $(dirname $OUTPUT)
    fi
  fi
}
function cleanup() {
  if [[ ! -z ${S3_OUTPUT} ]]; then # assumes we are on Amazon EMR
    echo "running s3-dist-cp --dest=${S3_OUTPUT} --src=${HDFS_OUTPUT}"
    s3-dist-cp --dest=${S3_OUTPUT} --src=${HDFS_OUTPUT}
  elif [[ ! -z ${GS_OUTPUT} ]]; then # assumes we are on Google Clou
    echo "hadoop distcp ${HDFS_OUTPUT} ${GS_OUTPUT}"
    hadoop distcp ${HDFS_OUTPUT} ${GS_OUTPUT}
  fi
}
function germline() {
  preprocessIfNeeded $GERMLINE
  GERMLINE=$NEWINPUT

  CLASS=be.ugent.intec.halvade.job.GermlinePipeline
  configureSpark
  getHalvadeOpts
  CMD="${SPARK_COMMAND} ${HALVADE_JAR} --germline  ${GERMLINE} --samplename ${SM} --output ${OUTPUT} --reference ${REF} ${SNIP_DB} ${TMP_DIR} --bindir ${BIN_DIR} --partitions ${PARTITIONS} ${EXTRA_HALVADE_OPTS}"
  run $CMD
  cleanup
}

function somatic() {
  preprocessIfNeeded $TUMOR_IN
  TUMOR_IN=$NEWINPUT
  preprocessIfNeeded $NORMAL_IN
  NORMAL_IN=$NEWINPUT

  CLASS=be.ugent.intec.halvade.job.SomaticPipeline
  configureSpark
  getHalvadeOpts

  CMD="${SPARK_COMMAND} ${HALVADE_JAR} --normal ${NORMAL_IN} --normalsm ${NORMAL_SM} --tumor ${TUMOR_IN} --tumorsm ${TUMOR_SM} --output ${OUTPUT} --reference ${REF} ${SNIP_DB} ${TMP_DIR} --bindir ${BIN_DIR} --partitions ${PARTITIONS} ${EXTRA_HALVADE_OPTS}"
  run $CMD
  cleanup
}

function parseOptions() {
  # parse options
  while [[ $# -gt 0 ]]
  do
    key="$1"
    case $key in
        --exome)
          EXOME_PIPELINE="true"
          shift # past argument
          ;;
        --quiet)
          QUIET="true"
          shift # past argument
          ;;
        --germlineSM)
          SM="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_memory)
          EXECUTOR_MEMORY="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_cpus)
          TASK_CPUS="$2"
          shift # past argument
          shift # past value
          ;;
        --partitions)
          PARTITIONS="$2"
          shift # past argument
          shift # past value
          ;;
        --tmpdir)
          TMP_DIR="$2"
          shift # past argument
          shift # past value
          ;;
        --tumorSM)
          TUMOR_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --normalSM)
          NORMAL_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --memory)
          RESOURCE_MEMORY="$2"
          RESOURCE_MEMORY=$((RESOURCE_MEMORY*1024)) # in MB
          shift # past argument
          shift # past value
          ;;
        --nodes)
          NODES="$2"
          shift # past argument
          shift # past value
          ;;
        --cpus)
          RESOURCE_CPU="$2"
          shift # past argument
          shift # past value
          ;;
        --halvade_opts)
          if [[ -z $HALVADE_OPTS ]]; then HALVADE_OPTS="${2//;/ }"; else echp "HALVADE_OPTS already set, ignoring --halvade_opts";fi
          shift # past argument
          shift # past value
          ;;
        *)    # unknown option
          echo "unknown option $1"
          customexit 100;
          ;;
    esac
  done
}

# read arguments and process
MODE=$1
shift
if [[ "$MODE" = "germline" ]]; then
  if [[ "$#" -lt "4" ]]; then
    echo -e $help
    customexit 7;
  fi
  REF_DIR=$(readlink -f $1); shift;
  BIN_DIR=$(readlink -f $1); shift;
  GERMLINE=$(getInput $1); shift;
  getOutput $1; shift
  SM="SAMPLE"
  parseOptions "$@"
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $INPUT == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $INPUT == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  germline
elif [[ "$MODE" = "somatic" ]]; then
  if [[ "$#" -lt "5" ]]; then
    echo -e $help
    customexit 8;
  fi
  REF_DIR=$(readlink -f $1); shift;
  BIN_DIR=$(readlink -f $1); shift;
  TUMOR_IN=$(getInput $1); shift;
  NORMAL_IN=$(getInput $1); shift;
  getOutput $1; shift
  TUMOR_SM="TUMOR_SM"
  NORMAL_SM="NORMAL_SM"
  parseOptions "$@"
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  somatic
else
  echo -e $help
  customexit 9;
fi

customexit 0; # successfull completion!
