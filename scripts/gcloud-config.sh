#!/bin/bash

# REQUIRED USER INPUT
GCLOUD_LIB_DIR=gs://halvade/lib/
GCLOUD_REF_DIR=gs://halvade/grch38/
GCLOUD_BIN_DIR=gs://halvade/bin/

# OPTIONAL GOOGLE CLOUD CONFIGURATION IF DEFAULT NOT SET WITH GCLOUD CONFIG
# GCLOUD_REGION=eu-west-1
# GCLOUD_ZONE=eu-west-1-d
# GCLOUD_PROJECT=halvade-project

# OPTIONAL USER INPUT
# WORKER_INSTANCE_COUNT=1 # default number of worker nodes can be overridden by --nodes
# WORKER_INSTANCE_TYPE=n2-highmem-32
# MASTER_INSTANCE_TYPE=c2-standard-4
