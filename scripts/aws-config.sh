#!/bin/bash

# REQUIRED USER INPUT
EC2_KEY_PAIR=aws-key # name of the key
S3_LIB_DIR=s3://halvade/lib/
S3_REF_DIR=s3://halvade/grch38/
S3_BIN_DIR=s3://halvade/bin/

# OPTIONAL IF SET IN AWS CONFIGURATION
# AWS_REGION=eu-west-1

# OPTIONAL USER INPUT
# CORE_INSTANCE_COUNT=1 # default number of worker nodes can be overridden by --nodes
# CORE_INSTANCE_TYPE=r5d.8xlarge
# CORE_BID_PRICE=0.1 # max bid price for spot instances (core nodes)
# MASTER_INSTANCE_TYPE=r5d.xlarge
# MASTER_BID_PRICE=0.25 # max bid price for spot instances (master node)
# LOG_URI=s3://halvade-logs/
