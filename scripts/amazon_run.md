# amazon aws scripts

## setup AWS CLI
- Run these commands to configure AWS CLI or look [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) for the full description.

```bash
# configure aws
aws configure
# required:
# - access key id
# - secret access key
# - default region name
# - default output format [json]
aws emr create-default-roles
```

- Create an IAM role with S3 access. Not sure if needed
- get EC2 keypair?

## setup data
``` bash
# copy reference data to S3:
aws s3 sync referenceFolder s3://my-bucket/referenceFolder
# copy binaries + scripts
aws s3 sync binariesFolder s3://my-bucket/binariesFolder
# copy inputdata
aws s3 sync inputData s3://my-bucket/inputData
```

## run halvade
- start the cluster

```bash
# start emr cluster
name="Halvade Somatic EMR"
releaseLabel="emr-5.33.0"
ec2Attributes="EMRKeyPairName"
instanceCount=2
instanceType="r5d.8xlarge"
libDir="s3://"
HALVADE_JAR=${libDir}/halvade-assembly-2.0.4.jar
HALVADE_ARGS="arg1,arg2,arg3"

aws emr create-cluster \
--name $name \
--release-label $releaseLabel \
--applications Name=Spark \
--ec2-attributes KeyName=$ec2Attributes \
--instance-type $instanceType \
--instance-count $instanceCount \
--use-default-roles \
--bootstrap-actions Path="s3://bucket/amazon-bootstrap.sh", Args=[${S3_REF_FOLDER}, ${S3_BIN_FOLDER}] \
--steps Type=CUSTOM_JAR,Name=CustomJAR,ActionOnFailure=TERMINATE,Jar=${HALVADE_JAR},MainClass=${SOMATIC_CLASS}Args=${HALVADE_ARGS} \
        # Type=CUSTOM_JAR,Name=CustomJAR,ActionOnFailure=CONTINUE,Jar=s3://mybucket/mytest.jar,MainClass=mymainclass,Args=arg1,arg2,arg3

```
interesting:
https://towardsdatascience.com/how-to-create-and-run-an-emr-cluster-using-aws-cli-3a78977dc7f0

// multiple steps:
```bash
--steps Type=CUSTOM_JAR,Name=CustomJAR,ActionOnFailure=CONTINUE,Jar=s3://myBucket/mytest.jar,Args=arg1,arg2,arg3 Type=CUSTOM_JAR,Name=CustomJAR,ActionOnFailure=CONTINUE,Jar=s3://myBucket/mytest.jar,MainClass=mymainclass,Args=arg1,arg2,arg3
```
create json file for steps, like this `--steps file://./multiplefiles.json`:
```json
[
  {
    "Name": "string",
    "Args": ["string", ...],
    "Jar": "string",
    "ActionOnFailure": "TERMINATE_CLUSTER"|"CANCEL_AND_WAIT"|"CONTINUE",
    "MainClass": "string",
    "Type": "CUSTOM_JAR"|"STREAMING"|"HIVE"|"PIG"|"IMPALA",
    "Properties": "string"
  },
  {
  ...
  }
]
```

```bash
# other example with spark submit
aws emr add-steps --cluster-id j-2AXXXXXXGAPLF --steps Name="Command runner jar step",Jar=command-runner.jar,Args=[spark-submit,<additional-args>]
```

- copy reference/binaries from s3 to all nodes

```bash
# add step
clusterid=`aws emr list-clusters --cluster-states WAITING`
aws emr add-steps \
--cluster-id $clusterid \
--steps Type=Spark,Name="Halvade: Copy Reference",ActionOnFailure=TERMINATE_CLUSTER,Args=[s3://DOC-EXAMPLE-BUCKET/health_violations.py,--data_source,s3://DOC-EXAMPLE-BUCKET/food_establishment_data.csv,--output_uri,s3://DOC-EXAMPLE-BUCKET/MyOutputFolder]
```

- run halvade

```bash
# add a step
aws emr add-steps \
--cluster-id $clusterid \
--steps Type=CUSTOM_JAR,Name=CustomJAR,ActionOnFailure=CONTINUE,Jar=s3://mybucket/mytest.jar,Args=arg1,arg2,arg3 Type=CUSTOM_JAR,Name=CustomJAR,ActionOnFailure=CONTINUE,Jar=s3://mybucket/mytest.jar,MainClass=mymainclass,Args=arg1,arg2,arg3
```

- shut down cluster
automatically? # --auto-terminate in create step

```bash
# shut down cluster
aws emr terminate-clusters --cluster-ids $clusterid
# or use --auto-terminate in create step, but then need to provide all steps!
```


- create a script that runs this automaticall
  +  that detects fastq paired files and creates a manifest from it
     folder means preprocess -> detect files ,fq -> detect pairs, bam -> pairs in bam
     if input is bam -> use that as input

default 1800/1500 parts
if --exome 320/270 parts
args:
reference folder in f3
binaries.tar.gz // i provide this but they upload it (also explain how to make it + build on amazon ec2)
input bam/folder normal
input bam/folder tumor
samplename
output folder s3-> create somatic.vcf file there

before starting cluster -> print input fq pairs so user can check if correct
```bash
aws s3 ls s3://mybucket/input/samplename/normal/
# shows contents
```
needed to first put on hdfs?
-> check my code in merge vcf if i use mapreduce filesystem? probably yes


list of libraries needed
- halvade-assembly-*.jar
- hadoop-bam-*.jar
- htsjdk-*.jar
list of binaries needed
- bwa
- samtools
- gatk-package-\*local\*.jar
- strelka2/ (folder)



# steps
``` bash
aws configure
AWS Access Key ID [None]:
# AWS console -> IAM -> Users -> select user -> security credentials -> create access key  // or found in ~/.aws/credentials
AWS Secret Access Key [None]:
# same as before
Default region name [None]:
# see aws console -> global -> overview of all regions -> eu-west-1
Default output format [None]:
# empty?

# get it from another machine:
aws configure get aws_access_key_id # or other variable

aws ec2 describe-instance-types --filters "Name=instance-type,Values=r5d.8xlarge" --query "InstanceTypes[].InstanceStorageInfo"
[
    {
        "TotalSizeInGB": 1200,
        "Disks": [
            {
                "SizeInGB": 600,
                "Count": 2,
                "Type": "ssd"
            }
        ],
        "NvmeSupport": "required"
    }
]


# set root volume size: --ebs-root-volume-size 20
# use different master node
# use many worker nodes

# option for spot instances
```
